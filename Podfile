source 'https://github.com/CocoaPods/Specs.git'

platform :ios, '10.0'
use_frameworks!
inhibit_all_warnings!

def shared_pods

  # Crash Report
    pod 'Fabric'
    pod 'Crashlytics'

  # Code utilities
  pod 'SwiftyJSON', :git => 'https://github.com/SwiftyJSON/SwiftyJSON.git', :tag => '4.0.0-alpha.1'
  pod 'semver', :git => 'https://github.com/rafaelks/Semver.Swift.git', :branch => 'chore/swift4'

  # UI
  pod 'SideMenuController', :git => 'https://github.com/rafaelks/SideMenuController.git'
  pod 'SlackTextViewController', :git => 'https://github.com/rafaelks/SlackTextViewController.git', :branch => 'master'
  pod 'MobilePlayer'
  pod 'SimpleImageViewer', :git => 'https://github.com/cardoso/SimpleImageViewer.git'

  # Text Processing
  pod 'RCMarkdownParser'

  # Database
  pod 'RealmSwift'

  # Network
  pod 'SDWebImage', '~> 3'
  pod 'Starscream'
  pod 'ReachabilitySwift'

  # Authentication SDKs
  pod '1PasswordExtension'
  pod 'Google/SignIn'

  # APNS
 pod 'APNS'
 pod 'ProtocolBuffers-Swift'

 pod 'Alamofire'
 
 pod 'TagListView', '~> 1.0'
 pod "SearchTextField"

# Firebase
  pod 'Firebase/Core'
  pod 'Firebase/Messaging'

# PJSIP
  pod 'pjsip'
end

target 'Rocket.Chat' do
  # Shared pods
  shared_pods
end

target 'Rocket.ChatTests' do
  # Shared pods
  shared_pods
end

post_install do |installer|
    swift4Targets = ['TagListView', 'SearchTextField']
    installer.pods_project.targets.each do |target|
        target.build_configurations.each do |config|
            config.build_settings['SWIFT_VERSION'] = '3.1'
        end
        if swift4Targets.include? target.name
            target.build_configurations.each do |config|
                config.build_settings['SWIFT_VERSION'] = '4.0'
            end
        end
    end
end

