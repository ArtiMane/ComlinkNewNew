//
//  AppDelegate.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 7/5/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import UIKit
import RealmSwift
import UserNotifications
import JitsiMeet
import Firebase
import FirebaseMessaging
import FirebaseCore
import FirebaseInstanceID
import Fabric
import Crashlytics

// swiftlint:disable trailing_whitespace
// swiftlint:disable colon
// swiftlint:disable vertical_whitespace

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , UNUserNotificationCenterDelegate , FIRMessagingDelegate  {
    var window: UIWindow?
    var notificationInfo : NSDictionary = [:]

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Launcher().prepareToLaunch(with: launchOptions)
        
        application.isIdleTimerDisabled = true
        Fabric.with([Crashlytics.self])
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        } else {
            // Fallback on earlier versions
        }
        // iOS 10 support
        if #available(iOS 10, *) {
           //
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge ,.alert, .sound]){ (granted, error) in }
            UNUserNotificationCenter.current().delegate = self
            FIRMessaging.messaging().remoteMessageDelegate = self

            application.registerForRemoteNotifications()
        }
            // iOS 9 support
        else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert], categories: nil))
          //  FIRMessaging.messaging().remoteMessageDelegate = self
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 8 support
        else if #available(iOS 8, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 7 support
        else {
            application.registerForRemoteNotifications(matching: [.sound, .alert])
        }
        
        if (launchOptions != nil) {
            
            // For local Notification
            if let localNotificationInfo = launchOptions?[UIApplicationLaunchOptionsKey.localNotification] as? UILocalNotification {
                
                
                
            } else
                
                // For remote Notification
                if let remoteNotification = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable : Any] {
                    
                    notificationInfo = remoteNotification as NSDictionary
                    
                    
                    
                    if(notificationInfo["link"] as? String != nil){
                        let type : String = (notificationInfo["type"] as? String)!
                        if(type == "video"){
                        let link : String = (notificationInfo["link"] as? String)!
                        let defaults = UserDefaults.standard
                        defaults.set(link, forKey: "serverurl1")
                        defaults.set("1", forKey: "isTapped") //means user taps notification
                        let title : String = (((notificationInfo["aps"] as? NSDictionary)!.value(forKey: "alert") as? NSDictionary)!.value(forKey: "body") as? NSString)! as String
                        let arrayTitle : NSArray = title.components(separatedBy: " ") as NSArray
                        let bodyTitle : String = (arrayTitle[3] as? String)!
                        print(bodyTitle)
                        defaults.set(bodyTitle, forKey: "bodyTitle")
                        defaults.set("1", forKey: "isVideoGoing")
                        let storyboardChat = UIStoryboard(name: "Chat", bundle: Bundle.main)
                      //  let controller = storyboardChat.instantiateViewController(withIdentifier: "JitsiViewController")
                        let controller = storyboardChat.instantiateViewController(withIdentifier: "IncomingCallVC")
                        if let main = UIApplication.shared.delegate?.window??.rootViewController as? MainChatViewController {
                            MainChatViewController.closeSideMenu()
                            if let nav = main.centerViewController as? UINavigationController {
                                nav.pushViewController(controller, animated: true)
                            }
                        }
                        }
                        if(type == "peer_chat" || type == "group_chat"){
                            let title : String = (((notificationInfo["aps"] as? NSDictionary)!.value(forKey: "alert") as? NSDictionary)!.value(forKey: "body") as? NSString)! as String
                            let bodyTitle : String = (((notificationInfo["aps"] as? NSDictionary)!.value(forKey: "alert") as? NSDictionary)!.value(forKey: "title") as? NSString)! as String
                            print(bodyTitle)
                            let roomName = bodyTitle
                            UserDefaults.standard.setValue(roomName, forKey: "newRoom")
                            
                        }
                    }
                    else {
                        if((notificationInfo["aps"] as? NSDictionary)!.value(forKey: "alert") as? String != nil){
                            let body = ((notificationInfo["aps"] as? NSDictionary)!.value(forKey: "alert") as? String)!
                            print(body)
                            
                            let arrayTitle : NSArray = body.components(separatedBy: "\n") as NSArray
                            var bodyTitle : String = (arrayTitle[0] as? String)!
                            if(bodyTitle .contains("#")){
                                bodyTitle = bodyTitle.replacingOccurrences(of: "#", with: "")
                            }
                            let roomName = bodyTitle
                            UserDefaults.standard.setValue(roomName, forKey: "newRoom")
                            
                        }
                    }

                  
                    
            }
        }
        FIRApp.configure()
        NotificationCenter.default.addObserver(self, selector: #selector(tokenRefreshNotification), name:
            Notification.Name.firInstanceIDTokenRefresh, object: nil)
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            UserDefaults.standard.setValue(refreshedToken, forKey: "DeviceToken")
        }

        
        return true
    }

    // MARK: AppDelegate LifeCycle

    func applicationDidBecomeActive(_ application: UIApplication) {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
        } else {
            // Fallback on earlier versions
        }
      //  center.removeAllDeliveredNotifications()
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        if AuthManager.isAuthenticated() != nil {
//            UserManager.setUserPresence(status: .away) { (_) in
//                SocketManager.disconnect({ (_, _) in })
//            }
        }
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance().handle(
            url,
            sourceApplication: options[.sourceApplication] as? String,
            annotation: options[.annotation]
        )
    }
    // MARK: Remote Notification
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {

        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("APNs device token: \(deviceTokenString)")
        print(deviceToken)
        UserDefaults.standard.set(deviceToken.hexString, forKey: PushManager.kDeviceTokenKey)

    }
    

  
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        Log.debug("Notification: \(userInfo)")
//        print("Push notification received: \(data)")
        print("my user info \(userInfo)")
        notificationInfo = userInfo as NSDictionary
        
        let state = UIApplication.shared.applicationState
        var body : String = ""
        if((notificationInfo["aps"] as? NSDictionary)!.value(forKey: "alert") as? String != nil){
            body = ((notificationInfo["aps"] as? NSDictionary)!.value(forKey: "alert") as? String)!
            print(body)
        }
        if (state == .active){
            if(notificationInfo["link"] as? String != nil){
                
            }
            else if(body.contains("https://ctsivideo.mvoipctsi.com")){
                
            }
            else {
               completionHandler(.newData)
            }
        }
       
        else if(state == .background || state == .inactive){
            if(body.contains("https://ctsivideo.mvoipctsi.com")){
                
            }
            else {
              //  completionHandler(.newData)
            }
            
        }

        
    }


    
    @available(iOS 10.0, *)
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("notification received")
      //  print("\(response.notification.request.content.userInfo)")
        let dic : NSDictionary = response.notification.request.content.userInfo as NSDictionary
        print("dic = \(dic)")
        let state = UIApplication.shared.applicationState
        var body : String = ""
        if((dic["aps"] as? NSDictionary)!.value(forKey: "alert") as? String != nil){
            body = ((dic["aps"] as? NSDictionary)!.value(forKey: "alert") as? String)!
            print(body)
            
        }
        if state == .active {
            if(dic["link"] as? String != nil){
                
            }
            else if(body.contains("https://ctsivideo.mvoipctsi.com")){
                
            }
            else {
                completionHandler()
            }
        }
        else if(state == .background || state == .inactive){
            if(body.contains("https://ctsivideo.mvoipctsi.com")){
                
            }
            else {
                completionHandler()
            }
        
        }
        
        if(dic["link"] as? String != nil){
            let link : String = (dic["link"] as? String)!
            let type : String = (dic["type"] as? String)!
            if(type == "video"){
            let defaults = UserDefaults.standard
            defaults.set(link, forKey: "serverurl1")
            defaults.set("1", forKey: "isTapped") //means user taps notification
            let title : String = (((dic["aps"] as? NSDictionary)!.value(forKey: "alert") as? NSDictionary)!.value(forKey: "body") as? NSString)! as String
            let arrayTitle : NSArray = title.components(separatedBy: " ") as NSArray
            let bodyTitle : String = (arrayTitle[3] as? String)!
            defaults.set(bodyTitle, forKey: "bodyTitle")
            print(bodyTitle)
            defaults.set("1", forKey: "isVideoGoing")
            let storyboardChat = UIStoryboard(name: "Chat", bundle: Bundle.main)
         //   let controller = storyboardChat.instantiateViewController(withIdentifier: "JitsiViewController")
            let controller = storyboardChat.instantiateViewController(withIdentifier: "IncomingCallVC")
            if let main = UIApplication.shared.delegate?.window??.rootViewController as? MainChatViewController {
                MainChatViewController.closeSideMenu()
                if let nav = main.centerViewController as? UINavigationController {
                    nav.pushViewController(controller, animated: true)
                }
            }
            }
            if(type == "peer_chat" || type == "group_chat"){
                let title : String = (((dic["aps"] as? NSDictionary)!.value(forKey: "alert") as? NSDictionary)!.value(forKey: "body") as? NSString)! as String
                let arrayTitle : NSArray = title.components(separatedBy: " ") as NSArray
                let bodyTitle : String = (((dic["aps"] as? NSDictionary)!.value(forKey: "alert") as? NSDictionary)!.value(forKey: "title") as? NSString)! as String
                print(bodyTitle)
                let roomName = bodyTitle
                UserDefaults.standard.setValue(roomName, forKey: "newRoom")
                guard let auth = AuthManager.isAuthenticated() else { return }
                
                SubscriptionManager.updateSubscriptions(auth) { _ in
                    if let newRoom = Realm.shared?.objects(Subscription.self).filter("name == '\(roomName)'").first {
                        
                        let state = UIApplication.shared.applicationState
                        let roomName = bodyTitle
                        
                        let controller = ChatViewController.shared
                        
                        if(state == .background || state == .active ){
                            let controller = ChatViewController.shared
                            controller?.subscription = newRoom
                            
                        }
                        
                        
                    } else {
                        
                    }
                }
            }
        }
        else {
            body = ((dic["aps"] as? NSDictionary)!.value(forKey: "alert") as? String)!
            print(body)
            
            let arrayTitle : NSArray = body.components(separatedBy: "\n") as NSArray
            var bodyTitle : String = (arrayTitle[0] as? String)!
            if(bodyTitle .contains("#")){
                bodyTitle = bodyTitle.replacingOccurrences(of: "#", with: "")
            }
            let roomName = bodyTitle
            UserDefaults.standard.setValue(roomName, forKey: "newRoom")
            guard let auth = AuthManager.isAuthenticated() else { return }
            
            SubscriptionManager.updateSubscriptions(auth) { _ in
                if let newRoom = Realm.shared?.objects(Subscription.self).filter("name == '\(roomName)'").first {
                    
                    let state = UIApplication.shared.applicationState
                    let controller = ChatViewController.shared
                    
                    if(state == .background || state == .active ){
                        let controller = ChatViewController.shared
                        
                        controller?.subscription = newRoom
                        MainChatViewController.closeSideMenu()
                        
                    }
                    
                    
                } else {
                    
                }
            }
        }
      //  completionHandler()

    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("notification received in will present")
      //  completionHandler(.badge)
        let dic : NSDictionary = notification.request.content.userInfo as NSDictionary
        print("dic = \(dic)")
       
        let state = UIApplication.shared.applicationState
        var body : String = ""
        if((dic["aps"] as? NSDictionary)!.value(forKey: "alert") as? String != nil){
            body = ((dic["aps"] as? NSDictionary)!.value(forKey: "alert") as? String)!
            print(body)
        }
        if state == .active {
            if(dic["link"] as? String != nil){
              //   completionHandler([.sound])
                let type : String = (dic["type"] as? String)!
                if(type == "video"){
                let link : String = (dic["link"] as? String)!
                let defaults = UserDefaults.standard
                defaults.set(link, forKey: "serverurl1")
                defaults.set("1", forKey: "isTapped") //means user taps notification
                let title : String = (((dic["aps"] as? NSDictionary)!.value(forKey: "alert") as? NSDictionary)!.value(forKey: "body") as? NSString)! as String
                let arrayTitle : NSArray = title.components(separatedBy: " ") as NSArray
                let bodyTitle : String = (arrayTitle[3] as? String)!
                defaults.set(bodyTitle, forKey: "bodyTitle")
                print(bodyTitle)
                if(defaults.value(forKey: "isVideoGoing") != nil){
                    let isVideoGoing = (defaults.value(forKey: "isVideoGoing") as? String)!
                    if(isVideoGoing == "1"){
                        completionHandler([.alert , .badge , .sound])
                    }
                    else {
                        let storyboardChat = UIStoryboard(name: "Chat", bundle: Bundle.main)
                        //   let controller = storyboardChat.instantiateViewController(withIdentifier: "JitsiViewController")
                        let controller = storyboardChat.instantiateViewController(withIdentifier: "IncomingCallVC")
                        if let main = UIApplication.shared.delegate?.window??.rootViewController as? MainChatViewController {
                            MainChatViewController.closeSideMenu()
                            if let nav = main.centerViewController as? UINavigationController {
                                nav.pushViewController(controller, animated: true)
                            }
                        }
                    }
                    
                }
                else {
                defaults.set("1", forKey: "isVideoGoing")
                let storyboardChat = UIStoryboard(name: "Chat", bundle: Bundle.main)
                //   let controller = storyboardChat.instantiateViewController(withIdentifier: "JitsiViewController")
                let controller = storyboardChat.instantiateViewController(withIdentifier: "IncomingCallVC")
                if let main = UIApplication.shared.delegate?.window??.rootViewController as? MainChatViewController {
                    MainChatViewController.closeSideMenu()
                    if let nav = main.centerViewController as? UINavigationController {
                        nav.pushViewController(controller, animated: true)
                    }
                }
            }
            }
            }
            else if(body.contains("https://ctsivideo.mvoipctsi.com")){
                
            }
            
                
            else {
                 completionHandler([ .sound])
            }
        }
        else if(state == .background || state == .inactive){
            if(body.contains("https://ctsivideo.mvoipctsi.com")){
                
            }
            else {
                completionHandler([.sound])
            }
            
        }
        
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        Log.debug("Fail to register for notification: \(error)")
    }
    //MARK: Jitsi Deep link supporting methods (Optional methods)
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        return JitsiMeetView.application(application, continue: userActivity, restorationHandler: restorationHandler)
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return JitsiMeetView.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }

    @objc func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            UserDefaults.standard.setValue(refreshedToken, forKey: "DeviceToken")
        }
    }

    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        print("got message")
    }

}
