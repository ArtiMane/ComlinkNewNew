//
//  pjstring.h
//  Rocket.Chat
//
//  Created by Riyaz Lakhani on 21/03/18.
//  Copyright © 2018 Rocket.Chat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <pjsua.h>

@interface pjstring : NSObject

// Returns an NSString object created from a given PJSUA string.
+ (NSString *)stringWithPJString:(pj_str_t)pjString;

// Returns PJSUA string created from the receiver.
- (pj_str_t)pjString;

@end
