//
//  ConnectServerViewController.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 7/6/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import UIKit
import SwiftyJSON
import semver
import APNS

final class ConnectServerViewController: BaseViewController {

   // internal let defaultURL = "https://open.rocket.chat" //rocket url
    internal let defaultURL = "https://ctsichat.mvoipctsi.com" // QuicSolv/Comlink server
   // internal let defaultURL = "http://166.62.93.231"
    internal var connecting = false
    var url: URL? {
        guard var urlText = textFieldServerURL.text else { return nil }
        if urlText.isEmpty {
            urlText = defaultURL
        }
        return  URL(string: urlText, scheme: "https")
    }
    
    var serverPublicSettings: AuthSettings?

    @IBOutlet weak var buttonClose: UIBarButtonItem!

    @IBOutlet weak var visibleViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var textFieldServerURL: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    @IBOutlet weak var viewFields: UIView! {
        didSet {
            viewFields.layer.cornerRadius = 4
            viewFields.layer.borderColor = UIColor.RCLightGray().cgColor
            viewFields.layer.borderWidth = 0.5
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if DatabaseManager.servers?.count ?? 0 > 0 {
            title = localized("servers.add_new_team")
        } else {
            navigationItem.leftBarButtonItem = nil
        }

        textFieldServerURL.placeholder = defaultURL

        if let nav = navigationController as? BaseNavigationController {
            nav.setTransparentTheme()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        SocketManager.sharedInstance.socket?.disconnect()
        DatabaseManager.cleanInvalidDatabases()
        
        if let applicationServerURL = AppManager.applicationServerURL {
            textFieldServerURL.text = applicationServerURL.host
            print("textFieldServerURL is %@",textFieldServerURL.text! as Any)
            connect()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(_:)),
            name: NSNotification.Name.UIKeyboardWillShow,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: NSNotification.Name.UIKeyboardWillHide,
            object: nil
        )
        textFieldServerURL.becomeFirstResponder()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? AuthViewController, segue.identifier == "Auth" {
            controller.serverURL = url?.socketURL()
            controller.serverPublicSettings = self.serverPublicSettings
        }
    }

    // MARK: Keyboard Handlers
    override func keyboardWillShow(_ notification: Notification) {
        if let keyboardSize = ((notification as NSNotification).userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            visibleViewBottomConstraint.constant = keyboardSize.height
        }
    }

    override func keyboardWillHide(_ notification: Notification) {
        visibleViewBottomConstraint.constant = 0
    }

    // MARK: IBAction

    @IBAction func action_sendNotification(_ sender: Any) {
        
        
//        let aps = ["sound":"default", "alert":"testPush()"]
//        let payload = ["aps":aps]
//        _ = try! APNSNetwork().sendPush(topic: "chat.rocket.iostest", priority: 10, payload: payload, deviceToken: "3dd55a59056441ab275b8b679458388cae76be3a9a02a00234388e50fe91f2fe", certificatePath: Bundle.path(forResource: "Certificates", ofType: "p12", inDirectory: "<#T##String#>"), passphrase: "aarti123", sandbox: true, responseBlock: { (response) in
//            XCTAssertTrue(response.serviceStatus.0 == 200)
//            self.expectation.fulfill()
//        }, networkError: { (error) in
//
//        })
        //F465ACE01BD279C3D5E7464C30B6E4A279E1D5C405CAE6A3746CA02B14E69065
        //C8524552C7A28194780B4FA2F37C514D201BCC44A3E01BE9EE1A7F63B7A32ADE
        
        let aps : NSDictionary = ["sound":"default", "alert":"testPush", "badge" : "1"] as NSDictionary
        let payload  = ["aps" : aps] 
      //  let str = Bundle(for:UnitTest.self).pathForResource("Certificates", ofType: "p12")!
        let str = Bundle(for: ConnectServerViewController.self).path(forResource: "Certificates", ofType: "p12")!
        var mess = ApplePushMessage(topic: "chat.rocket.iostest",
                                    priority: 5,
                                    payload: payload,
                                    deviceToken: (UserDefaults.standard.value(forKey: "DeviceToken") as? String)!,
                                    certificatePath:str,
                                    passphrase: "aarti123",
                                    sandbox: true
                                    )
        
        

        mess.responseBlock = { response in
        }

        mess.networkError = { err in
            if (err != nil) {
                print(err)
            }
        }
        _ = try? mess.send() // OR try! mess.send(session:<URLSession>)
        
        
    }
    @IBAction func buttonCloseDidPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)

        let storyboardChat = UIStoryboard(name: "Main", bundle: Bundle.main)
        let controller = storyboardChat.instantiateInitialViewController()
        let application = UIApplication.shared

        if let window = application.windows.first {
            window.rootViewController = controller
        }
    }

    func alertInvalidURL() {
        let alert = UIAlertController(
            title: localized("alert.connection.invalid_url.title"),
            message: localized("alert.connection.invalid_url.message"),
            preferredStyle: .alert
        )

        alert.addAction(UIAlertAction(title: localized("global.ok"), style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }

    func connect() {
        textFieldServerURL.text = url?.absoluteString
        print(url?.absoluteString)
        guard let url = url else { return alertInvalidURL() }
        guard let socketURL = url.socketURL() else { return alertInvalidURL() }
        //print(/(socketURL) is socketURL)
        // Check if server already exists and connect to that instead
        if let servers = DatabaseManager.servers {
            let sameServerIndex = servers.index(where: {
                if let stringServerUrl = $0[ServerPersistKeys.serverURL],
                    let serverUrl = URL(string: stringServerUrl) {

                    return serverUrl == socketURL
                } else {
                    return false
                }
            })

            if let sameServerIndex = sameServerIndex {
                MainChatViewController.shared?.changeSelectedServer(index: sameServerIndex)
                textFieldServerURL.resignFirstResponder()
                return
            }
        }

        connecting = true
        textFieldServerURL.alpha = 0.5
        activityIndicator.startAnimating()
        textFieldServerURL.resignFirstResponder()
        //print(/(url) is url)
        API.shared.host = url
        validate { [weak self] (_, error) in
            guard !error else {
                DispatchQueue.main.async {
                    self?.connecting = false
                    self?.textFieldServerURL.alpha = 1
                    self?.activityIndicator.stopAnimating()
                    self?.alertInvalidURL()
                }

                return
            }

            let index = DatabaseManager.createNewDatabaseInstance(serverURL: socketURL.absoluteString)
            DatabaseManager.changeDatabaseInstance(index: index)

            SocketManager.connect(socketURL) { (_, connected) in
                AuthSettingsManager.updatePublicSettings(nil) { (settings) in
                    self?.serverPublicSettings = settings

                    if connected {
                        self?.performSegue(withIdentifier: "Auth", sender: nil)
                    }

                    self?.connecting = false
                    self?.textFieldServerURL.alpha = 1
                    self?.activityIndicator.stopAnimating()
                }
            }
        }
    }

    func validate(completion: @escaping RequestCompletion) {
        API.shared.fetch(InfoRequest()) {
            result in
            guard let version = result?.version else {
                return completion(nil, true)
            }

            if let minVersion = Bundle.main.object(forInfoDictionaryKey: "RC_MIN_SERVER_VERSION") as? String {
                if Semver.lt(version, minVersion) {
                    let alert = UIAlertController(
                        title: localized("alert.connection.invalid_version.title"),
                        message: String(format: localized("alert.connection.invalid_version.message"), version, minVersion),
                        preferredStyle: .alert
                    )

                    alert.addAction(UIAlertAction(title: localized("global.ok"), style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }

            completion(result?.raw, false)
        }
    }
}

extension ConnectServerViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return !connecting
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        connect()
        return true
    }
}
