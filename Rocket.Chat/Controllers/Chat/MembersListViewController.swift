//
//  MembersListViewController.swift
//  Rocket.Chat
//
//  Created by Matheus Cardoso on 9/19/17.
//  Copyright © 2017 Rocket.Chat. All rights reserved.
//

import UIKit
import Alamofire

// swiftlint:disable Trailing Whitespace
// swiftlint:disable trailing_whitespace
// swiftlint:disable colon
// swiftlint:disable control_statement
// swiftlint:disable vertical_whitespace
// swiftlint:disable opening_brace
// swiftlint:disable statement_position
// swiftlint:disable force_unwrapping


class MembersListViewData {
    var subscription: Subscription?
    
    let pageSize = 50
    var currentPage = 0
    
    var showing: Int = 0
    var total: Int = 0
    
    var title: String {
        return String(format: localized("chat.members.list.title"), total)
    }
    
    var isShowingAllMembers: Bool {
        return showing >= total
    }
    
    var membersPages: [[User]] = []
    var members: FlattenCollection<[[User]]> {
        return membersPages.joined()
    }
    
    func member(at index: Int) -> User {
        return members[members.index(members.startIndex, offsetBy: index)]
    }
    
    private var isLoadingMoreMembers = false
    func loadMoreMembers(completion: (() -> Void)? = nil) {
        if isLoadingMoreMembers { return }
        
        if let subscription = subscription {
            isLoadingMoreMembers = true
            API.shared.fetch(SubscriptionMembersRequest(roomId: subscription.rid, type: subscription.type), options: .paginated(count: pageSize, offset: currentPage*pageSize)) { result in
                self.showing += result?.count ?? 0
                self.total = result?.total ?? 0
                if let members = result?.members {
                    self.membersPages.append(members.flatMap { $0 })
                }
                
                self.currentPage += 1
                
                self.isLoadingMoreMembers = false
                completion?()
            }
        }
    }
}

class MembersListViewController: BaseViewController {
    @IBOutlet weak var membersTableView: UITableView!

    var loaderCell: LoaderTableViewCell!
    var activityView : UIActivityIndicatorView!
    var owner :  String = ""
    var usernames : NSMutableArray  = []
    var isOwner : Bool = false

    var data = MembersListViewData()
    
    @objc func refreshControlDidPull(_ sender: UIRefreshControl) {
        let data = MembersListViewData()
        data.subscription = self.data.subscription
        data.loadMoreMembers { [weak self] in
            self?.data = data
            
            DispatchQueue.main.async {
                if self?.membersTableView?.refreshControl?.isRefreshing ?? false {
                    self?.membersTableView?.refreshControl?.endRefreshing()
                }
                
                UIView.performWithoutAnimation {
                    self?.membersTableView?.reloadData()
                }
            }
        }
    }
    
    
    @IBOutlet weak var exitgrupimageimage: UIImageView!
    @IBOutlet weak var exitgroupimage: UIButton!
    @IBOutlet weak var deleteGroupButton: UIButton!
    @IBAction func action_exitGroup(_ sender: Any) {
    activityView.startAnimating()
        let headers: HTTPHeaders = [
            "X-Auth-Token": API.shared.authToken!,
            "X-User-Id": API.shared.userId!
        ]
        let type : String = (self.data.subscription?.type.rawValue)!
        var finalURL : String = ""
        let urlGroup = "https://ctsichat.mvoipctsi.com/api/v1/groups.leave"
        let urlChannel = "https://ctsichat.mvoipctsi.com/api/v1/channels.leave"
        print(self.data.subscription?.name)
        if(type == "p"){
            finalURL = urlGroup
        }
        else if(type == "c"){
            finalURL = urlChannel
        }
        
        Alamofire.request(finalURL ,method: .post, parameters: ["roomId" : (self.data.subscription?.rid)! as AnyObject], encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            print("log")
            print(response.request as Any)  // original URL request
            print(response.response as Any) // URL response
            print(response.result.value as Any)
            
            if(response.result.isSuccess)
            {   self.activityView.stopAnimating()
                let resultDic : NSDictionary = (response.result.value as? NSDictionary)!
                let success : Bool = (resultDic.value(forKey: "success") as? Bool)!
                if(success == false){
                let alertcontroller = UIAlertController(title: "Error", message: (resultDic.value(forKey: "error") as? String)!, preferredStyle: .actionSheet)
                let alertAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                    alertcontroller.removeFromParentViewController()
                })
                alertcontroller.addAction(alertAction)
                self.present(alertcontroller, animated: true, completion: nil)
                
                }
                else 
                {
                    print("removed successfully")
                    self.membersTableView.reloadData()
                    self.loadMoreMembers()
//                    let data = MembersListViewData()
//                    data.subscription = self.data.subscription
//                    data.loadMoreMembers { [weak self] in
//                        self?.data = data
//
//                        DispatchQueue.main.async {
//                            if self?.membersTableView?.refreshControl?.isRefreshing ?? false {
//                                self?.membersTableView?.refreshControl?.endRefreshing()
//                            }
//
//                            UIView.performWithoutAnimation {
//                                self?.membersTableView?.reloadData()
//                            }
//                        }
//                    }
                }
                
            }
            if(response.result.isFailure)
            {
                self.activityView.stopAnimating()
                print("error")
            }
            
        }
        
    }

    func callApiForGroup()
    {
        self.activityView.startAnimating()
        let headers: HTTPHeaders = [
            "X-Auth-Token": API.shared.authToken!,
            "X-User-Id": API.shared.userId!
        ]
        let type : String = (self.data.subscription?.type.rawValue)!
        var finalURL : String = ""
        let urlGroup = "https://ctsichat.mvoipctsi.com/api/v1/groups.info"
        let urlChannel = "https://ctsichat.mvoipctsi.com/api/v1/channels.info"
        print(self.data.subscription?.name)
        if(type == "p"){
            finalURL = urlGroup
            Alamofire.request(finalURL, method: .get, parameters: ["roomId":(self.data.subscription?.rid)! as AnyObject], encoding: URLEncoding.default, headers:headers)
                .responseJSON { response in
                    print(response.request as Any)  // original URL request
                    print(response.response as Any) // URL response
                    print(response.result.value as Any)
                    // result of response serialization
                    if(response.result.value == nil)
                    {
                        self.activityView.stopAnimating()
                    }
                    else
                    {
                        self.activityView.stopAnimating()
                        let responseDic : NSDictionary = (response.result.value as? NSDictionary)!
                        let status : Bool = (responseDic.value(forKey: "success") as? Bool)!
                        if(status ==  true)
                        {
                            let groupDic = (responseDic.value(forKey: "group") as? NSDictionary)!
                            let array_users = (groupDic.value(forKey: "usernames") as? NSArray)!
                            self.owner = ((groupDic.value(forKey: "u") as?NSDictionary)?.value(forKey: "username") as? String)!
                            if((AuthManager.currentUser()?.username) == self.owner )
                            {
                                let button1 = UIBarButtonItem(title: "Add", style: .done, target: self, action: #selector(MembersListViewController.addMembers))
                                self.navigationItem.rightBarButtonItem = button1
                                self.isOwner = true
                            }

                            for var i in 0..<array_users.count
                            {
                                self.usernames.add(i)
                                if((AuthManager.currentUser()?.username) == (array_users[i] as? String) )
                                {

                                }
                                else
                                {

                                }
                            }

                        }
                        self.membersTableView.reloadData()
                    }
            }
        }
        else if(type == "c"){
            finalURL = urlChannel

        Alamofire.request(finalURL, method: .get, parameters: ["roomId":(self.data.subscription?.rid)! as AnyObject], encoding: URLEncoding.default, headers:headers)
            .responseJSON { response in
                print(response.request as Any)  // original URL request
                print(response.response as Any) // URL response
                print(response.result.value as Any)
                // result of response serialization
                if(response.result.value == nil)
                {
                    self.activityView.stopAnimating()
                }
                else
                {
                    self.activityView.stopAnimating()
                    let responseDic : NSDictionary = (response.result.value as? NSDictionary)!
                    let status : Bool = (responseDic.value(forKey: "success") as? Bool)!
                    if(status ==  true)
                    {
                        let groupDic = (responseDic.value(forKey: "channel") as? NSDictionary)!
                        let array_users = (groupDic.value(forKey: "usernames") as? NSArray)!
                        if(groupDic.value(forKey: "u") as?NSDictionary != nil){
                        self.owner = ((groupDic.value(forKey: "u") as?NSDictionary)?.value(forKey: "username") as? String)!
                        if((AuthManager.currentUser()?.username) == self.owner )
                        {
                            let button1 = UIBarButtonItem(title: "Add", style: .done, target: self, action: #selector(MembersListViewController.addMembers))
                            self.navigationItem.rightBarButtonItem = button1
                            self.isOwner = true
                        }
                        }
                        for var i in 0..<array_users.count
                        {
                            self.usernames.add(i)
                            if((AuthManager.currentUser()?.username) == (array_users[i] as? String) )
                            {

                            }
                            else
                            {

                            }
                        }

                    }
                }
               self.membersTableView.reloadData()
        }
        }
    }
    
    func loadMoreMembers() {
        data.loadMoreMembers { [weak self] in
            DispatchQueue.main.async {
                self?.title = self?.data.title
                
                if #available(iOS 10.0, *) {
                    if self?.membersTableView?.refreshControl?.isRefreshing ?? false {
                        self?.membersTableView?.refreshControl?.endRefreshing()
                    }
                } else {
                    // Fallback on earlier versions
                }
                
                UIView.performWithoutAnimation {
                    self?.membersTableView?.reloadData()
                }
            }
        }
    }
    
    @objc func deleteUser(sender : UIButton)
    {
        let buttonPosition:CGPoint = sender.convert(.zero, to:membersTableView)
        let indexPath = membersTableView.indexPathForRow(at: buttonPosition)
        print(indexPath?.row)
        let name : String = data.member(at: (indexPath?.row)!).name!
        print(name)
        print("hi")
        
    }
    
    func callDeleteMemberAPI(name : String)
    {
        let headers: HTTPHeaders = [
            "X-Auth-Token": API.shared.authToken!,
            "X-User-Id": API.shared.userId!
        ]
        let type : String = (self.data.subscription?.type.rawValue)!
        var finalURL : String = ""
        let urlGroup = "https://ctsichat.mvoipctsi.com/api/v1/groups.kick"
        let urlChannel = "https://ctsichat.mvoipctsi.com/api/v1/channels.kick"
        print(self.data.subscription?.name)
        if(type == "p"){
            finalURL = urlGroup
        }
        else if(type == "c"){
            finalURL = urlChannel
        }
        print(self.data.subscription?.displayName())
        print(name)
        
        Alamofire.request(finalURL ,method: .post, parameters: ["roomName" : (self.data.subscription?.name)! as AnyObject , "username" : name], encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            print("log")
            print(response.request as Any)  // original URL request
            print(response.response as Any) // URL response
            print(response.result.value as Any)
            
            if(response.result.isSuccess)
            {
                self.activityView.stopAnimating()
                let resultDic : NSDictionary = (response.result.value as? NSDictionary)!
                let success : Bool = (resultDic.value(forKey: "success") as? Bool)!
                if(success == false){
                    let alertcontroller = UIAlertController(title: "Error", message: (resultDic.value(forKey: "error") as? String)!, preferredStyle: .actionSheet)
                    let alertAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alertcontroller.removeFromParentViewController()
                    })
                    alertcontroller.addAction(alertAction)
                    self.present(alertcontroller, animated: true, completion: nil)
                    
                }
                else 
                {
                    print("removed successfully")
                    let alertcontroller = UIAlertController(title: "Message", message: "Removed successfully.", preferredStyle: .actionSheet)
                    let alertAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alertcontroller.removeFromParentViewController()
                    })
                    alertcontroller.addAction(alertAction)
                    self.present(alertcontroller, animated: true, completion: nil)
                    self.membersTableView.reloadData()
                    self.loadMoreMembers()
                    let data = MembersListViewData()
                    data.subscription = self.data.subscription
                    data.loadMoreMembers { [weak self] in
                        self?.data = data

                        DispatchQueue.main.async {
                            if self?.membersTableView?.refreshControl?.isRefreshing ?? false {
                                self?.membersTableView?.refreshControl?.endRefreshing()
                            }

                            UIView.performWithoutAnimation {
                                self?.membersTableView?.reloadData()
                            }
                        }
                    }
                }
                
            }
            if(response.result.isFailure)
            {
                self.activityView.stopAnimating()
                print("error")
            }
            
        }
    }
    
    
}

// MARK: ViewController
extension MembersListViewController {
    
    
    override func viewDidLoad() {
        activityView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityView.center = self.view.center
        self.view.addSubview(activityView)
        let type : String = (self.data.subscription?.type.rawValue)!
        if(type == "d"){
           deleteGroupButton.isHidden = true
            exitgroupimage.isHidden = true
            exitgrupimageimage.isHidden = true
        }
        else {
        deleteGroupButton.isHidden = false
        exitgroupimage.isHidden = false
        exitgrupimageimage.isHidden = false
        self.callApiForGroup()
        }
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshControlDidPull), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            membersTableView.refreshControl = refreshControl
        } else {
            // Fallback on earlier versions
        }
        
        registerCells()
        
        if let cell = membersTableView.dequeueReusableCell(withIdentifier: LoaderTableViewCell.identifier) as? LoaderTableViewCell {
            self.loaderCell = cell
        }
        
//        let button1 = UIBarButtonItem(title: "Add", style: .done, target: self, action: #selector(MembersListViewController.addMembers))
//        self.navigationItem.rightBarButtonItem = button1


    }
    
    func registerCells() {
        membersTableView.register(UINib(
            nibName: "MemberCell",
            bundle: Bundle.main
        ), forCellReuseIdentifier: MemberCell.identifier)
        
        membersTableView.register(UINib(
            nibName: "LoaderTableViewCell",
            bundle: Bundle.main
        ), forCellReuseIdentifier: LoaderTableViewCell.identifier)
    }
    
    @objc func addMembers()
    {
        print("method called")
        let addMembers : AddMembers = (self.storyboard?.instantiateViewController(withIdentifier: "AddMembers") as? AddMembers)!
        addMembers.subscription = self.data.subscription
        self.navigationController?.pushViewController(addMembers, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadMoreMembers()
        
        if #available(iOS 10.0, *) {
            guard let refreshControl = membersTableView.refreshControl else { return }
            membersTableView.refreshControl?.beginRefreshing()
            membersTableView.contentOffset = CGPoint(x: 0, y: -refreshControl.frame.size.height)
        } else {
            // Fallback on earlier versions
        }
        
        let data = MembersListViewData()
        data.subscription = self.data.subscription
        data.loadMoreMembers { [weak self] in
            self?.data = data

            DispatchQueue.main.async {
                if self?.membersTableView?.refreshControl?.isRefreshing ?? false {
                    self?.membersTableView?.refreshControl?.endRefreshing()
                }

                UIView.performWithoutAnimation {
                    self?.membersTableView?.reloadData()
                }
            }
        }

    }
}

// MARK: TableView

extension MembersListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.members.count + (data.isShowingAllMembers ? 0 : 1)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == data.members.count {
            return self.loaderCell
        }
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: MemberCell.identifier) as? MemberCell {
            cell.data = MemberCellData(member: data.member(at: indexPath.row))
//            cell.deleteUserButton.addTarget(self, action: #selector(MembersListViewController.deleteUser(sender:)), for: .touchUpInside)
            let name : String = data.member(at: (indexPath.row)).displayName()
            if(name == owner){
                cell.nameLabel.text = name + " (admin)"
            }

            if(isOwner == true){
            if(name == owner){

                cell.deleteUserButton.isHidden = true

            }
            else {
                cell.deleteUserButton.isHidden = false
            }
            }
            else {
                cell.deleteUserButton.isHidden = true
            }
            return cell
        }

        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

extension MembersListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // tableView.deselectRow(at: indexPath, animated: false)
      //  print("indexpath.row = \(indexPath.row)")
       // deleteUser()
        let name : String = data.member(at: (indexPath.row)).displayName()
        print(name)
        if(isOwner == true){
            if(name == owner){
              //  cell.deleteUserButton.isHidden = true

            }
            else {
              //  cell.deleteUserButton.isHidden = false
                let alertcontroller = UIAlertController(title: "Message", message: "Are you sure you want to remove "+name, preferredStyle: .actionSheet)
                let alertAction : UIAlertAction = UIAlertAction(title: "No", style: .cancel, handler: { (UIAlertAction) in
                    alertcontroller.removeFromParentViewController()
                })
                let alertAction1 : UIAlertAction = UIAlertAction(title: "Yes", style: .destructive, handler: { (UIAlertAction) in
                    self.activityView.startAnimating()
                    self.callDeleteMemberAPI(name: name)


                })
                alertcontroller.addAction(alertAction)
                alertcontroller.addAction(alertAction1)
                self.present(alertcontroller, animated: true, completion: nil)
            }
        }
        else {
          //  cell.deleteUserButton.isHidden = true
        }
//        let alertcontroller = UIAlertController(title: "Message", message: "Are you sure you want to remove "+name, preferredStyle: .actionSheet)
//        let alertAction : UIAlertAction = UIAlertAction(title: "No", style: .cancel, handler: { (UIAlertAction) in
//            alertcontroller.removeFromParentViewController()
//        })
//        let alertAction1 : UIAlertAction = UIAlertAction(title: "Yes", style: .destructive, handler: { (UIAlertAction) in
//            self.activityView.startAnimating()
//            self.callDeleteMemberAPI(name: name)
//
//
//        })
//        alertcontroller.addAction(alertAction)
//        alertcontroller.addAction(alertAction1)
//        self.present(alertcontroller, animated: true, completion: nil)

        
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == data.members.count - data.pageSize/2 {
            loadMoreMembers()
        }
    }
}
