//
//  ChatViewController.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 7/21/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import RealmSwift
import SlackTextViewController
import SimpleImageViewer
import JitsiMeet
import UIKit
import Darwin
import Alamofire
import APNS
import UserNotifications
import UserNotificationsUI
import Firebase
import FirebaseInstanceID
import FirebaseMessaging

// swiftlint:disable Trailing Whitespace
// swiftlint:disable trailing_whitespace
// swiftlint:disable colon
// swiftlint:disable control_statement
// swiftlint:disable vertical_whitespace
// swiftlint:disable opening_brace
// swiftlint:disable statement_position
// swiftlint:disable force_unwrapping


// swiftlint:disable file_length type_body_length
// swiftlint:disable 
final class ChatViewController: SLKTextViewController , UITextFieldDelegate  {
    
    var deviceToken : String = ""
    var activityIndicator: LoaderView!
    @IBOutlet weak var activityIndicatorContainer: UIView! {
        didSet {
            let width = activityIndicatorContainer.bounds.width
            let height = activityIndicatorContainer.bounds.height
            let frame = CGRect(x: 0, y: 0, width: width, height: height)
            let activityIndicator = LoaderView(frame: frame)
            activityIndicatorContainer.addSubview(activityIndicator)
            self.activityIndicator = activityIndicator
        }
    }

    @IBOutlet weak var buttonScrollToBottom: UIButton!
    var buttonScrollToBottomMarginConstraint: NSLayoutConstraint?

    var showButtonScrollToBottom: Bool = false {
        didSet {
            self.buttonScrollToBottom.superview?.layoutIfNeeded()

            if self.showButtonScrollToBottom {
                self.buttonScrollToBottomMarginConstraint?.constant = -64
            } else {
                self.buttonScrollToBottomMarginConstraint?.constant = 50
            }
            if showButtonScrollToBottom != oldValue {
                UIView.animate(withDuration: 0.5) {
                    self.buttonScrollToBottom.superview?.layoutIfNeeded()
                }
            }
        }
    }

    weak var chatTitleView: ChatTitleView?
    weak var chatPreviewModeView: ChatPreviewModeView?
    weak var chatHeaderViewStatus: ChatHeaderViewStatus?
    var documentController: UIDocumentInteractionController?

    var replyView: ReplyView!
    var replyString: String = ""

    var dataController = ChatDataController()

    var searchResult: [String: Any] = [:]
    var isGroupCall : Bool = false
    var isGroupMessage : Bool = false
    var closeSidebarAfterSubscriptionUpdate = false

    @IBOutlet var incomingView: UIView!
    var isRequestingHistory = false
    var isAppendingMessages = false

    var hud  = MBProgressHUD()
    
    let socketHandlerToken = String.random(5)
    var messagesToken: NotificationToken!
    var messagesQuery: Results<Message>!
    var messages: [Message] = []
    var subscription: Subscription! {
        didSet {
            guard !subscription.isInvalidated else { return }

            updateSubscriptionInfo()
            markAsRead()
            typingIndicatorView?.dismissIndicator()

            if let oldValue = oldValue, oldValue.identifier != subscription.identifier {
                unsubscribe(for: oldValue)
            }
        }
    
    }

    // MARK: View Life Cycle

    static var shared: ChatViewController? {
        if let main = UIApplication.shared.delegate?.window??.rootViewController as? MainChatViewController {
            if let nav = main.centerViewController as? UINavigationController {
                let chat  = ChatViewController()
               
                let myTitle = chat.subscription?.displayName()
                print("myTitle = \(myTitle)")
                if(myTitle == "general"){
                    nav.navigationItem.rightBarButtonItem?.isEnabled = false
                }
                else {
                    nav.navigationItem.rightBarButtonItem?.isEnabled = true
                }
                return nav.viewControllers.first as? ChatViewController
        }
        }

        return nil
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        SocketManager.removeConnectionHandler(token: socketHandlerToken)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        registerCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.tintColor = UIColor(rgb: 0x5B5B5B, alphaVal: 1)
        
        hud.mode = .determinate
        hud.label.text = "Loading"
        if #available(iOS 10.0, *) {
            collectionView?.isPrefetchingEnabled = true
            
        } else {
            // Fallback on earlier versions
        }

        isInverted = false
        bounces = true
        shakeToClearEnabled = true
        isKeyboardPanningEnabled = true
        shouldScrollToBottomAfterKeyboardShows = false

        //
        let button1 = UIBarButtonItem(image: UIImage(named: "ic_video_call"), style: .plain, target: self, action:#selector(rightMenuBar)) // action:#selector(Class.MethodName) for swift 3
         self.navigationItem.rightBarButtonItem  = button1
        
        leftButton.setImage(UIImage(named: "Upload"), for: .normal)
        rightButton.isEnabled = false

        errorLogFile(text: "dinesh", to: "to123")
        setupTitleView()
        setupTextViewSettings()
        setupScrollToBottomButton()

        NotificationCenter.default.addObserver(self, selector: #selector(reconnect), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        SocketManager.addConnectionHandler(token: socketHandlerToken, handler: self)

        if !SocketManager.isConnected() {
            socketDidDisconnect(socket: SocketManager.sharedInstance)
        }

        guard let auth = AuthManager.isAuthenticated() else { return }
        if(UserDefaults.standard.value(forKey: "newRoom") != nil){
            SubscriptionManager.updateSubscriptions(auth) { _ in
                let roomName  : String = (UserDefaults.standard.value(forKey: "newRoom") as? String)!
                print("room name = \(String(describing: roomName))")
                //   let roomName  : String = (UserDefaults.standard.value(forKey: "newRoom") as? String)!
                if let newRoom = Realm.shared?.objects(Subscription.self).filter("name == '\(roomName)'").first {
                    self.subscription = newRoom
                }
                UserDefaults.standard.removeObject(forKey: "newRoom")
            }
            
        }
        else {
            let subscriptions = auth.subscriptions.sorted(byKeyPath: "lastSeen", ascending: false)
            if let subscription = subscriptions.first {
                self.subscription = subscription
            }
        }
        
//        if let subscription = subscriptions.first {
//            self.subscription = subscription
//        }

        view.bringSubview(toFront: activityIndicatorContainer)
        view.bringSubview(toFront: buttonScrollToBottom)
        view.bringSubview(toFront: textInputbar)

        if buttonScrollToBottomMarginConstraint == nil {
            buttonScrollToBottomMarginConstraint = buttonScrollToBottom.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 50)
            buttonScrollToBottomMarginConstraint?.isActive = true
        }
        setupReplyView()
        
        if(UserDefaults.standard.value(forKey: "serverurl1") != nil)
        {
            if(UserDefaults.standard.value(forKey: "isTapped") != nil){
//                self.incomingView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
//                self.view.addSubview(self.incomingView)
            let storyboardChat = UIStoryboard(name: "Chat", bundle: Bundle.main)
          //  let controller = storyboardChat.instantiateViewController(withIdentifier: "JitsiViewController")
            MainChatViewController.closeSideMenu()
            let controller = storyboardChat.instantiateViewController(withIdentifier: "IncomingCallVC")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        }
    }
    
    
    
//    override func viewDidAppear(_ animated: Bool) {
//        
//        if(UserDefaults.standard.value(forKey: "ejson") != nil)
//        {
//         MainChatViewController.openSideMenu()
//            UserDefaults.standard.removeObject(forKey: "ejson")
//        }
//    }

    @objc func errorLogFile(text: String, to fileNamed: String, folder: String = "SavedFiles") {
        guard let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else { return }
        guard let writePath = NSURL(fileURLWithPath: path).appendingPathComponent(folder) else { return }
        try? FileManager.default.createDirectory(atPath: writePath.path, withIntermediateDirectories: true)
        let file = writePath.appendingPathComponent(fileNamed + ".txt")
        try? text.write(to: file, atomically: false, encoding: String.Encoding.utf8)
    }
    
    @objc func rightMenuBar() { //By Amol
      //  self.callApi()
        
        let id = subscription.rid
        print("subscription type = \(subscription.type.rawValue)")
        print("id = \(id)")
        if(subscription.type.rawValue == "p")
        {
            hud.show(animated: true)
             self.getGroupCount()
           
         //   self.callApiForGroup()
            
        }
        else if(subscription.type.rawValue == "c")
        {
            hud.show(animated: true)
           self.getChannelCount()
            
            ///   self.callApiForChannel()
            
            
        }
        else if(subscription.type.rawValue == "d")
        {
            self.callApi()
            let serverUrlChatVC = "org.jitsi.meet:https://ctsivideo.mvoipctsi.com/\(API.shared.userId!)\(subscription.rid)"
            let serverUrlChatVC1 = "https://ctsivideo.mvoipctsi.com/\(API.shared.userId!)\(subscription.rid)"
            //  let serverUrlChatVC = "https://meet.jit.si/\(API.shared.userId!)\(subscription.rid)"
            let defaults = UserDefaults.standard
            defaults.set(serverUrlChatVC, forKey: "serverurl")
            defaults.set(serverUrlChatVC1, forKey: "serverurl2")
            textView.text = "Hi you have received a video call , *if you are web user* then please click on *[Join Conference](\(serverUrlChatVC1))*"
            print("URL is :\(textView.text)")
            textView.delegate = self
            textView.isSelectable = true
           
            textView.isUserInteractionEnabled = true
            textView.dataDetectorTypes = .all
            sendMessage()
            let storyboardChat = UIStoryboard(name: "Chat", bundle: Bundle.main)
            let controller = storyboardChat.instantiateViewController(withIdentifier: "JitsiViewController")
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
        /*
        let serverUrlChatVC = "org.jitsi.meet:https://ctsivideo.mvoipctsi.com/\(API.shared.userId!)\(subscription.rid)"
        let serverUrlChatVC1 = "https://ctsivideo.mvoipctsi.com/\(API.shared.userId!)\(subscription.rid)"
      //  let serverUrlChatVC = "https://meet.jit.si/\(API.shared.userId!)\(subscription.rid)"
        let defaults = UserDefaults.standard
        defaults.set(serverUrlChatVC, forKey: "serverurl")
        defaults.set(serverUrlChatVC1, forKey: "serverurl2")
        textView.text = "Hi you have received a video call , *if you are web user* then please click on *[Join Conference](\(serverUrlChatVC1))*"
        print("URL is :\(textView.text)")
        textView.delegate = self
        textView.isSelectable = true
        textView.isUserInteractionEnabled = true
        textView.dataDetectorTypes = .all
        sendMessage()
        let storyboardChat = UIStoryboard(name: "Chat", bundle: Bundle.main)
        let controller = storyboardChat.instantiateViewController(withIdentifier: "JitsiViewController")
        self.navigationController?.pushViewController(controller, animated: true)
        */
        /*
        var isChanged : Bool
        isChanged = true
        let id = subscription.rid
        print("id = \(id)")
        let lastChatObj = String(describing: dataController.data.last?.message?.text)
        
        print("lastChatObj is:\(lastChatObj)")
        
        let input = lastChatObj
        print("input is : \(String(describing: input))")
        let detector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector?.matches(in: input, options: [], range: NSRange(location: 0, length: (input.utf16.count)))
        
        for match in matches! {
            isChanged = false
            if let range = Range(match.range, in: input)
            {
                let url = input[range]
                print("URL in string is :\(url)")
                
               // if (url.range(of: "https://meet.jit.si") != nil){
                    if (url.range(of: "https://ctsivideo.mvoipctsi.com") != nil){
                    print("Jitsi Call Receiver")
                    let serverUrlChatVC = url
                    print(serverUrlChatVC)
                    let defaults = UserDefaults.standard
                    defaults.set(serverUrlChatVC, forKey: "serverurl")
                    textView.text = "_Thanks for conference call_"
    print("Thanks Message is  :\(textView.text)")
                    textView.delegate = self
                    textView.isSelectable = true
                    textView.isUserInteractionEnabled = true
                    textView.dataDetectorTypes = .all
                    sendMessage()
                    //Now we are landing to video screen with below lines
                    let storyboardChat = UIStoryboard(name: "Chat", bundle: Bundle.main)
                    let controller = storyboardChat.instantiateViewController(withIdentifier: "JitsiViewController")
                    self.navigationController?.pushViewController(controller, animated: true)
                }
                else {
                    print("Jitsi Call initiater")
                    
                    let diceRoll = generateRandomStringWithLength(length: 5)
                    print("Random Room ID is : \(diceRoll)")
                    
                   // let serverUrlChatVC = "https://meet.jit.si/\(diceRoll)"
                    let serverUrlChatVC = "https://ctsivideo.mvoipctsi.com/\(API.shared.userId!)\(subscription.rid)"
                    print("serverUrlChatVC: \(serverUrlChatVC)")
                    let defaults = UserDefaults.standard
                    defaults.set(serverUrlChatVC, forKey: "serverurl")
                    /*to assign a text to text view*/
                    textView.text = "Hi you have recieved a video call, to accept please tap on *video icon* and *if you are web user* then please click on *[Join Conference](\(serverUrlChatVC))*"
                    print("URL is :\(textView.text)")
                    textView.delegate = self
                    textView.isSelectable = true
                    textView.isUserInteractionEnabled = true
                    textView.dataDetectorTypes = .all
                    sendMessage()
                    let storyboardChat = UIStoryboard(name: "Chat", bundle: Bundle.main)
                    let controller = storyboardChat.instantiateViewController(withIdentifier: "JitsiViewController")
                    self.navigationController?.pushViewController(controller, animated: true)
                }
        }
            else {
                print("Jitsi Call initiater")
                let diceRoll = generateRandomStringWithLength(length: 5)
                print("Random Room ID is : \(diceRoll)")
               // let serverUrlChatVC = "https://meet.jit.si/\(diceRoll)"
                 let serverUrlChatVC = "https://ctsivideo.mvoipctsi.com/\(API.shared.userId!)\(subscription.rid)"
                print("serverUrlChatVC: \(serverUrlChatVC)")
                let defaults = UserDefaults.standard
                defaults.set(serverUrlChatVC, forKey: "serverurl")
                textView.text = "Hi you have recieved a video call, to accept please tap on *video icon* and *if you are web user* then please click on *[Join Conference](\(serverUrlChatVC))*"
                print("URL is :\(textView.text)")
                textView.delegate = self
                textView.isSelectable = true
                textView.isUserInteractionEnabled = true
                textView.dataDetectorTypes = .all
                sendMessage()
                let storyboardChat = UIStoryboard(name: "Chat", bundle: Bundle.main)
                let controller = storyboardChat.instantiateViewController(withIdentifier: "JitsiViewController")
                self.navigationController?.pushViewController(controller, animated: true)
            }
            }
        if isChanged {
        print("Jitsi Call initiater")
         let diceRoll = generateRandomStringWithLength(length: 5)
        print("Random Room ID is : \(diceRoll)")
     //   let serverUrlChatVC = "https://meet.jit.si/\(diceRoll)"
        let serverUrlChatVC = "https://ctsivideo.mvoipctsi.com/\(API.shared.userId!)\(subscription.rid)"
            print("serverUrlChatVC: \(serverUrlChatVC)")
        let defaults = UserDefaults.standard
        defaults.set(serverUrlChatVC, forKey: "serverurl")
         textView.text = "Hi you have recieved a video call, to accept please tap on *video icon* and *if you are web user* then please click on *[Join Conference](\(serverUrlChatVC))*"
            print("URL is :\(textView.text)")
        textView.delegate = self
        textView.isSelectable = true
        textView.isUserInteractionEnabled = true
        textView.dataDetectorTypes = .all
        sendMessage()
        let storyboardChat = UIStoryboard(name: "Chat", bundle: Bundle.main)
        let controller = storyboardChat.instantiateViewController(withIdentifier: "JitsiViewController")
        self.navigationController?.pushViewController(controller, animated: true)
        }
 */
    }
    
    func callApi()
    {
        //https://ctsichat.mvoipctsi.com/api/v1/users.info
        title = subscription?.displayName()
        UserDefaults.standard.set(title, forKey: "title")
        print("title = \(String(describing: title))")
        let headers: HTTPHeaders = [
            "X-Auth-Token": API.shared.authToken!,
            "X-User-Id": API.shared.userId!
        ]
        Alamofire.request("https://ctsichat.mvoipctsi.com/api/v1/users.info", method: .get, parameters: ["username":title!], encoding: URLEncoding.default, headers:headers)
            .responseJSON { response in
                print(response.request as Any)  // original URL request
                print(response.response as Any) // URL response
                print(response.result.value as Any)
                // result of response serialization
                if(response.result.value == nil)
                {
                    
                }
                else
                {
                let responseDic : NSDictionary = (response.result.value as? NSDictionary)!
                let status : Bool = (responseDic.value(forKey: "success") as? Bool)!
                if(status ==  true)
                {
                    let usersDic = responseDic.value(forKey: "user") as? NSDictionary
                    let tokenDic = usersDic?.value(forKey: "customFields") as? NSDictionary
                    
                    
                      if let devicetoken = tokenDic?.value(forKey: "devicetoken") as? String 
                      {
                        print("deviceToken = \(String(describing: devicetoken))")
                        self.deviceToken = devicetoken
                        let defaults = UserDefaults.standard
                        let serverUrlVideoVC : String = (defaults.object(forKey:"serverurl") as? String)!
                        print("ServerUrlVideoVC is : \(String(describing: serverUrlVideoVC))")
                        self.isGroupCall = false
                        self.sendVideoNotification(deviceToken: devicetoken)

                        /*
                        let alert =  "Video call from " + (AuthManager.currentUser()?.username)! 
                        let aps : NSDictionary = ["sound":"0063.aiff", "alert": alert , "badge" : "1" , "content-available" : "1"] as NSDictionary
                        let payload  = ["aps" : aps ,"link" : serverUrlVideoVC] as [String : Any]
                        //  let str = Bundle(for:UnitTest.self).pathForResource("Certificates", ofType: "p12")!
                        let str = Bundle(for: ChatViewController.self).path(forResource: "Certificates", ofType: "p12")!
                        var mess = ApplePushMessage(topic: "chat.rocket.iostest",
                                                    priority: 5,
                                                    payload: payload,
                                                    deviceToken: self.deviceToken,
                                                    certificatePath:str,
                                                    passphrase: "aarti123",
                                                    sandbox: true
                        )
                        mess.responseBlock = { response in
                        }
                        mess.networkError = { err in
                            if (err != nil) {
                                print(err)
                            }
                        }
                        _ = try? mess.send()
                        print("sent")

                        */
                    
                    }
                    
                }
                }
        }
    }
    
    func getGroupCount()  {
        var count : Int? = nil
        title = subscription?.displayName()
        print("title = \(String(describing: title))")
        let headers: HTTPHeaders = [
            "X-Auth-Token": API.shared.authToken!,
            "X-User-Id": API.shared.userId!
        ]
        Alamofire.request("https://ctsichat.mvoipctsi.com/api/v1/groups.info", method: .get, parameters: ["roomName":title!], encoding: URLEncoding.default, headers:headers)
            .responseJSON { response in
                print(response.request as Any)  // original URL request
                print(response.response as Any) // URL response
                print(response.result.value as Any)
                // result of response serialization
                if(response.result.value == nil)
                {
                    count = 0
                    DispatchQueue.main.async {
                    self.hud.hide(animated: true)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                    self.hud.hide(animated: true)
                    }
                    let responseDic : NSDictionary = (response.result.value as? NSDictionary)!
                    let status : Bool = (responseDic.value(forKey: "success") as? Bool)!
                    if(status ==  true)
                    {
                        let groupDic = (responseDic.value(forKey: "group") as? NSDictionary)!
                        let array_users = (groupDic.value(forKey: "usernames") as? NSArray)!
                        let username : String = (groupDic.value(forKey: "username") as?String)!
                        if(array_users.count == 1){
                            let alertController = UIAlertController(title: "Error", message: "Please add the members in group.", preferredStyle: .actionSheet)
                            let alertAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                                alertController.dismiss(animated: true, completion: nil)
                            })
                            alertController.addAction(alertAction)
                            self.present(alertController, animated: true, completion: nil)
                            count = array_users.count
                        }
                        else {
                            let id = self.subscription.rid
                            print("subscription type = \(self.subscription.type.rawValue)")
                            print("id = \(id)")
                            self.callApiForGroup()
                            let serverUrlChatVC = "org.jitsi.meet:https://ctsivideo.mvoipctsi.com/\(API.shared.userId!)\(self.subscription.rid)"
                            let serverUrlChatVC1 = "https://ctsivideo.mvoipctsi.com/\(API.shared.userId!)\(self.subscription.rid)"
                            //  let serverUrlChatVC = "https://meet.jit.si/\(API.shared.userId!)\(subscription.rid)"
                            let defaults = UserDefaults.standard
                            defaults.set(serverUrlChatVC, forKey: "serverurl")
                            defaults.set(serverUrlChatVC1, forKey: "serverurl2")
                            self.textView.text = "Hi you have received a video call , *if you are web user* then please click on *[Join Conference](\(serverUrlChatVC1))*"
                            print("URL is :\(self.textView.text)")
                            self.textView.delegate = self
                            self.textView.isSelectable = true
                            self.textView.isUserInteractionEnabled = true
                            self.textView.dataDetectorTypes = .all
                            self.sendMessage()
                            let storyboardChat = UIStoryboard(name: "Chat", bundle: Bundle.main)
                            let controller = storyboardChat.instantiateViewController(withIdentifier: "JitsiViewController")
                            self.navigationController?.pushViewController(controller, animated: true)
                        }
                        
                        
                    }
                }
        }
       
    }
    
    func getChannelCount()  {
         var count : Int? = nil
        title = subscription?.displayName()
        print("title = \(String(describing: title))")
        let headers: HTTPHeaders = [
            "X-Auth-Token": API.shared.authToken!,
            "X-User-Id": API.shared.userId!
        ]
        Alamofire.request("https://ctsichat.mvoipctsi.com/api/v1/channels.info", method: .get, parameters: ["roomName":title!], encoding: URLEncoding.default, headers:headers)
            .responseJSON { response in
                print(response.request as Any)  // original URL request
                print(response.response as Any) // URL response
                print(response.result.value as Any)
                // result of response serialization
                if(response.result.value == nil)
                {
                    DispatchQueue.main.async {
                    self.hud.hide(animated: true)
                    }
                   count = 0
                }
                else
                {
                    DispatchQueue.main.async {
                    self.hud.hide(animated: true)
                    }
                    let responseDic : NSDictionary = (response.result.value as? NSDictionary)!
                    let status : Bool = (responseDic.value(forKey: "success") as? Bool)!
                    if(status ==  true)
                    {
                        let groupDic = (responseDic.value(forKey: "channel") as? NSDictionary)!
                        let array_users = (groupDic.value(forKey: "usernames") as? NSArray)!
                        let username : String = (groupDic.value(forKey: "username") as?String)!
                        if(array_users.count == 1){
                            let alertController = UIAlertController(title: "Error", message: "Please add the members in channel.", preferredStyle: .actionSheet)
                            let alertAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                                alertController.dismiss(animated: true, completion: nil)
                            })
                            alertController.addAction(alertAction)
                            self.present(alertController, animated: true, completion: nil)
                        }
                        else {
                            let id = self.subscription.rid
                            print("subscription type = \(self.subscription.type.rawValue)")
                            print("id = \(id)")
                            self.callApiForChannel()
                            let serverUrlChatVC = "org.jitsi.meet:https://ctsivideo.mvoipctsi.com/\(API.shared.userId!)\(self.subscription.rid)"
                            let serverUrlChatVC1 = "https://ctsivideo.mvoipctsi.com/\(API.shared.userId!)\(self.subscription.rid)"
                            //  let serverUrlChatVC = "https://meet.jit.si/\(API.shared.userId!)\(subscription.rid)"
                            let defaults = UserDefaults.standard
                            defaults.set(serverUrlChatVC, forKey: "serverurl")
                            defaults.set(serverUrlChatVC1, forKey: "serverurl2")
                            self.textView.text = "Hi you have received a video call , *if you are web user* then please click on *[Join Conference](\(serverUrlChatVC1))*"
                            print("URL is :\(self.textView.text)")
                            self.textView.delegate = self
                            self.textView.isSelectable = true
                            self.textView.isUserInteractionEnabled = true
                            self.textView.dataDetectorTypes = .all
                            self.sendMessage()
                            let storyboardChat = UIStoryboard(name: "Chat", bundle: Bundle.main)
                            let controller = storyboardChat.instantiateViewController(withIdentifier: "JitsiViewController")
                            self.navigationController?.pushViewController(controller, animated: true)
                            
                        }
                        
                        count = array_users.count
                    }
                }
        }
       
    }
    
    func callApiForGroup()
    {
        title = subscription?.displayName()
        UserDefaults.standard.set(title, forKey: "title")
        print("title = \(String(describing: title))")
        let headers: HTTPHeaders = [
            "X-Auth-Token": API.shared.authToken!,
            "X-User-Id": API.shared.userId!
        ]
        Alamofire.request("https://ctsichat.mvoipctsi.com/api/v1/groups.info", method: .get, parameters: ["roomName":title!], encoding: URLEncoding.default, headers:headers)
            .responseJSON { response in
                print(response.request as Any)  // original URL request
                print(response.response as Any) // URL response
                print(response.result.value as Any)
                // result of response serialization
                if(response.result.value == nil)
                {
                    
                }
                else
                {
                    let responseDic : NSDictionary = (response.result.value as? NSDictionary)!
                    let status : Bool = (responseDic.value(forKey: "success") as? Bool)!
                    if(status ==  true)
                    {
                        let groupDic = (responseDic.value(forKey: "group") as? NSDictionary)!
                        let array_users = (groupDic.value(forKey: "usernames") as? NSArray)!
                        let username : String = (groupDic.value(forKey: "username") as?String)!
                        if(array_users.count == 1){
                            let alertController = UIAlertController(title: "Error", message: "Please add the members in group.", preferredStyle: .actionSheet)
                            let alertAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                                alertController.dismiss(animated: true, completion: nil)
                            })
                            alertController.addAction(alertAction)
                            self.present(alertController, animated: true, completion: nil)
                        }
                        else {
                        for var i in 0..<array_users.count
                        {
                            if((AuthManager.currentUser()?.username) == (array_users[i] as? String) )
                            {
                                
                            }
                            else
                            {
                            self.isGroupCall = true
                            self.callgroupApiForname(name: (array_users[i] as? String)!)
                            }
                        }
                        }
                        
                    }
                }
        }
    }
    
    func callApiForChannel()
    {
        title = subscription?.displayName()
        UserDefaults.standard.set(title, forKey: "title")
        print("title = \(String(describing: title))")
        let headers: HTTPHeaders = [
            "X-Auth-Token": API.shared.authToken!,
            "X-User-Id": API.shared.userId!
        ]
        Alamofire.request("https://ctsichat.mvoipctsi.com/api/v1/channels.info", method: .get, parameters: ["roomName":title!], encoding: URLEncoding.default, headers:headers)
            .responseJSON { response in
                print(response.request as Any)  // original URL request
                print(response.response as Any) // URL response
                print(response.result.value as Any)
                // result of response serialization
                if(response.result.value == nil)
                {
                    
                }
                else
                {
                    let responseDic : NSDictionary = (response.result.value as? NSDictionary)!
                    let status : Bool = (responseDic.value(forKey: "success") as? Bool)!
                    if(status ==  true)
                    {
                        let groupDic = (responseDic.value(forKey: "channel") as? NSDictionary)!
                        let array_users = (groupDic.value(forKey: "usernames") as? NSArray)!
                        let username : String = (groupDic.value(forKey: "username") as?String)!
                        if(array_users.count == 1){
                            let alertController = UIAlertController(title: "Error", message: "Please add the members in channel.", preferredStyle: .actionSheet)
                            let alertAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                                alertController.dismiss(animated: true, completion: nil)
                            })
                            alertController.addAction(alertAction)
                            self.present(alertController, animated: true, completion: nil)
                        }
                        else {
                        for var i in 0..<array_users.count
                        {
                            if((AuthManager.currentUser()?.username) == (array_users[i] as? String) )
                            {
                                
                            }
                            else
                            {   self.isGroupCall = true
                                self.callgroupApiForname(name: (array_users[i] as? String)!)
                            }
                        }
                        }
                        
                    }
                }
        }
    }
    
    func callgroupApiForname(name : String)
    {
        //https://ctsichat.mvoipctsi.com/api/v1/users.info
        title = subscription?.displayName()
        print("title = \(String(describing: title))")
        let headers: HTTPHeaders = [
            "X-Auth-Token": API.shared.authToken!,
            "X-User-Id": API.shared.userId!
        ]
        Alamofire.request("https://ctsichat.mvoipctsi.com/api/v1/users.info", method: .get, parameters: ["username":name], encoding: URLEncoding.default, headers:headers)
            .responseJSON { response in
                print(response.request as Any)  // original URL request
                print(response.response as Any) // URL response
                print(response.result.value as Any)
                // result of response serialization
                if(response.result.value == nil)
                {
                    
                }
                else
                {
                    let responseDic : NSDictionary = (response.result.value as? NSDictionary)!
                    let status : Bool = (responseDic.value(forKey: "success") as? Bool)!
                    if(status ==  true)
                    {
                        let usersDic = responseDic.value(forKey: "user") as? NSDictionary
                        let tokenDic = usersDic?.value(forKey: "customFields") as? NSDictionary
                        
                        
                        if let devicetoken = tokenDic?.value(forKey: "devicetoken") as? String 
                        {
                            print("deviceToken 1 = \(String(describing: devicetoken))")
                            self.deviceToken = devicetoken
                            let defaults = UserDefaults.standard
                            let serverUrlVideoVC : String = (defaults.object(forKey:"serverurl") as? String)!
                            print("ServerUrlVideoVC is : \(String(describing: serverUrlVideoVC))")
                            self.sendVideoNotification(deviceToken: devicetoken)
                            /*
                            let alert = self.title! + " is calling you"
                            let aps : NSDictionary = ["sound":"0063.aiff", "alert": alert , "badge" : "1" , "content-available" : "1"] as NSDictionary
                            let payload  = ["aps" : aps ,"link" : serverUrlVideoVC] as [String : Any]
                            //  let str = Bundle(for:UnitTest.self).pathForResource("Certificates", ofType: "p12")!
                            let str = Bundle(for: ChatViewController.self).path(forResource: "Certificates", ofType: "p12")!
                            var mess = ApplePushMessage(topic: "chat.rocket.iostest",
                                                        priority: 5,
                                                        payload: payload,
                                                        deviceToken: self.deviceToken,
                                                        certificatePath:str,
                                                        passphrase: "aarti123",
                                                        sandbox: true
                            )
                            mess.responseBlock = { response in
                            }
                            mess.networkError = { err in
                                if (err != nil) {
                                    print(err)
                                }
                            }
                            _ = try? mess.send()
                            print("sent")
                            */
                        }
                        
                    }
                }
        }
    }

    //"key=AAAArhocp9M:APA91bEYUeMsaiqS5L8kHSU1hjbHJn0mImrUy3JblqLuM91jjln7QKGGlSvzCZl8wsVstVgZwSoV-meV0mZnoEIeXwKB-_deJ_HVag09e1UNGGJIJm9TQd2Y5Ggsj-_uvJYK5ATAlilt"
   
    
    //AAAAdhE2N2Y:APA91bGpJRtkifNG8G_vrEGtLi2hrYAtx4HYlqqhTwjC93TaAkpB-KfZCa5wO8mVDKFYMadxJ0YA_1tTtMUApkkueRBWfcFssO33s1UM8C3dmgNYwKn-zVQxaBWnRZVt5gvmt52qKlO6

    
    //AAAABB9cgnQ:APA91bFX9AOpLr-O75_XPhmVDLemTjfgeN7AhW9MUg5bCEtwylSk7GQ56J3h-xAPUlPTratAlsqXmhzf9P8qzFdRjf6Ry5w-H6ZiBVVVEHNdgMf-RpWaYsVnmeAMRXa7fGQWJaYh2yWj
    func sendVideoNotification(deviceToken:String)
    {
        var titleOfAlert = ""
        if(isGroupCall == true){titleOfAlert = (subscription?.displayName())! }
        else {titleOfAlert = (AuthManager.currentUser()?.displayName())! }
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "key=AAAABB9cgnQ:APA91bFX9AOpLr-O75_XPhmVDLemTjfgeN7AhW9MUg5bCEtwylSk7GQ56J3h-xAPUlPTratAlsqXmhzf9P8qzFdRjf6Ry5w-H6ZiBVVVEHNdgMf-RpWaYsVnmeAMRXa7fGQWJaYh2yWj"
        ]
        // deviceToken
        // (AuthManager.currentUser()?.displayName())!
        let parameters : [String:AnyObject] = ["to":deviceToken, "priority" : "high" ,"notification" :["body" : "Video Call from " + titleOfAlert ,"title" : titleOfAlert , "sound" : "tring_tring_tring.mp3","click_action" : "com.comlinkinc.MainActivity"], "data" : ["link" : (UserDefaults.standard.object(forKey:"serverurl2") as? String)!, "type":"video" ]] as [String : AnyObject]
        print("parameters = \(parameters)")
        let jsonObject = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        let jsonString = String(data: jsonObject! , encoding: .utf8)
        print("json string = \(jsonString)")
        Alamofire.request("https://fcm.googleapis.com/fcm/send", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers:headers)
            .responseJSON { response in

                print("response.request : \(response.request as Any)")  // original URL request
                print("response.response : \(response.response as Any)") // URL response
                print("result : \(response.result.value as Any)")

        }
    }
    
    func callApiForMessage(message : String)
    {
        //https://ctsichat.mvoipctsi.com/api/v1/users.info
        title = subscription?.displayName()
        UserDefaults.standard.set(title, forKey: "title")
        print("title = \(String(describing: title))")
        let headers: HTTPHeaders = [
            "X-Auth-Token": API.shared.authToken!,
            "X-User-Id": API.shared.userId!
        ]
        Alamofire.request("https://ctsichat.mvoipctsi.com/api/v1/users.info", method: .get, parameters: ["username":title!], encoding: URLEncoding.default, headers:headers)
            .responseJSON { response in
                print(response.request as Any)  // original URL request
                print(response.response as Any) // URL response
                print(response.result.value as Any)
                // result of response serialization
                if(response.result.value == nil)
                {
                    
                }
                else
                {
                    let responseDic : NSDictionary = (response.result.value as? NSDictionary)!
                    let status : Bool = (responseDic.value(forKey: "success") as? Bool)!
                    if(status ==  true)
                    {
                        let usersDic = responseDic.value(forKey: "user") as? NSDictionary
                        let tokenDic = usersDic?.value(forKey: "customFields") as? NSDictionary
                        
                        
                        if let devicetoken = tokenDic?.value(forKey: "devicetoken") as? String
                        {
                            print("deviceToken = \(String(describing: devicetoken))")
                            self.deviceToken = devicetoken
                            let defaults = UserDefaults.standard
                          
                            self.isGroupMessage = false
                            self.sendMessageNotification(deviceToken: devicetoken, message: message)
                        }
                        
                    }
                }
        }
    }
    
    func callApiForGroupMessage(message: String)
    {
        title = subscription?.displayName()
        UserDefaults.standard.set(title, forKey: "title")
        print("title = \(String(describing: title))")
        let headers: HTTPHeaders = [
            "X-Auth-Token": API.shared.authToken!,
            "X-User-Id": API.shared.userId!
        ]
        Alamofire.request("https://ctsichat.mvoipctsi.com/api/v1/groups.info", method: .get, parameters: ["roomName":title!], encoding: URLEncoding.default, headers:headers)
            .responseJSON { response in
                print(response.request as Any)  // original URL request
                print(response.response as Any) // URL response
                print(response.result.value as Any)
                // result of response serialization
                if(response.result.value == nil)
                {
                    
                }
                else
                {
                    let responseDic : NSDictionary = (response.result.value as? NSDictionary)!
                    let status : Bool = (responseDic.value(forKey: "success") as? Bool)!
                    if(status ==  true)
                    {
                        let groupDic = (responseDic.value(forKey: "group") as? NSDictionary)!
                        let array_users = (groupDic.value(forKey: "usernames") as? NSArray)!
                        let username : String = (groupDic.value(forKey: "username") as?String)!

                            for var i in 0..<array_users.count
                            {
                                if((AuthManager.currentUser()?.username) == (array_users[i] as? String) )
                                {
                                    
                                }
                                else
                                {
                                    
                                 
                                    self.isGroupMessage = true
                                    self.callgroupApiFornameMessage(name: (array_users[i] as? String)!, message: message)
                                }
                            }
                     
                        
                    }
                }
        }
    }
    
    func callApiForChannelMessage( message: String)
    {
        title = subscription?.displayName()
        UserDefaults.standard.set(title, forKey: "title")
        print("title = \(String(describing: title))")
        let headers: HTTPHeaders = [
            "X-Auth-Token": API.shared.authToken!,
            "X-User-Id": API.shared.userId!
        ]
        Alamofire.request("https://ctsichat.mvoipctsi.com/api/v1/channels.info", method: .get, parameters: ["roomName":title!], encoding: URLEncoding.default, headers:headers)
            .responseJSON { response in
                print(response.request as Any)  // original URL request
                print(response.response as Any) // URL response
                print(response.result.value as Any)
                // result of response serialization
                if(response.result.value == nil)
                {
                    
                }
                else
                {
                    let responseDic : NSDictionary = (response.result.value as? NSDictionary)!
                    let status : Bool = (responseDic.value(forKey: "success") as? Bool)!
                    if(status ==  true)
                    {
                        let groupDic = (responseDic.value(forKey: "channel") as? NSDictionary)!
                        let array_users = (groupDic.value(forKey: "usernames") as? NSArray)!
                        let username : String = (groupDic.value(forKey: "username") as?String)!

                            for var i in 0..<array_users.count
                            {
                                if((AuthManager.currentUser()?.username) == (array_users[i] as? String) )
                                {
                                    
                                }
                                else
                                {
                                  
                                    self.isGroupMessage = true
                                    self.callgroupApiFornameMessage(name: (array_users[i] as? String)!, message: message)
                                }
                            }
                    
                        
                    }
                }
        }
    }
    
    func callgroupApiFornameMessage(name : String, message: String)
    {
        //https://ctsichat.mvoipctsi.com/api/v1/users.info
        title = subscription?.displayName()
        print("title = \(String(describing: title))")
        let headers: HTTPHeaders = [
            "X-Auth-Token": API.shared.authToken!,
            "X-User-Id": API.shared.userId!
        ]
        Alamofire.request("https://ctsichat.mvoipctsi.com/api/v1/users.info", method: .get, parameters: ["username":name], encoding: URLEncoding.default, headers:headers)
            .responseJSON { response in
                print(response.request as Any)  // original URL request
                print(response.response as Any) // URL response
                print(response.result.value as Any)
                // result of response serialization
                if(response.result.value == nil)
                {
                    
                }
                else
                {
                    let responseDic : NSDictionary = (response.result.value as? NSDictionary)!
                    let status : Bool = (responseDic.value(forKey: "success") as? Bool)!
                    if(status ==  true)
                    {
                        let usersDic = responseDic.value(forKey: "user") as? NSDictionary
                        let tokenDic = usersDic?.value(forKey: "customFields") as? NSDictionary
                        
                        
                        if let devicetoken = tokenDic?.value(forKey: "devicetoken") as? String
                        {
                            print("deviceToken 1 = \(String(describing: devicetoken))")
                            self.deviceToken = devicetoken
                            let defaults = UserDefaults.standard
                        
                            self.sendMessageNotification(deviceToken: devicetoken, message: message)
                            
                        }
                        
                    }
                }
        }
    }
    
    //"key=AAAArhocp9M:APA91bEYUeMsaiqS5L8kHSU1hjbHJn0mImrUy3JblqLuM91jjln7QKGGlSvzCZl8wsVstVgZwSoV-meV0mZnoEIeXwKB-_deJ_HVag09e1UNGGJIJm9TQd2Y5Ggsj-_uvJYK5ATAlilt"
    
    
    //AAAAdhE2N2Y:APA91bGpJRtkifNG8G_vrEGtLi2hrYAtx4HYlqqhTwjC93TaAkpB-KfZCa5wO8mVDKFYMadxJ0YA_1tTtMUApkkueRBWfcFssO33s1UM8C3dmgNYwKn-zVQxaBWnRZVt5gvmt52qKlO6
    
    
    //AAAABB9cgnQ:APA91bFX9AOpLr-O75_XPhmVDLemTjfgeN7AhW9MUg5bCEtwylSk7GQ56J3h-xAPUlPTratAlsqXmhzf9P8qzFdRjf6Ry5w-H6ZiBVVVEHNdgMf-RpWaYsVnmeAMRXa7fGQWJaYh2yWj
    func sendMessageNotification(deviceToken:String , message: String)
    {
        var titleOfAlert = ""
        if(isGroupMessage == true){titleOfAlert = (subscription?.displayName())! }
        else {titleOfAlert = (AuthManager.currentUser()?.displayName())! }
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "key=AAAABB9cgnQ:APA91bFX9AOpLr-O75_XPhmVDLemTjfgeN7AhW9MUg5bCEtwylSk7GQ56J3h-xAPUlPTratAlsqXmhzf9P8qzFdRjf6Ry5w-H6ZiBVVVEHNdgMf-RpWaYsVnmeAMRXa7fGQWJaYh2yWj"
        ]
        // deviceToken
        // (AuthManager.currentUser()?.displayName())!
        var link = ""
        var type = ""
        if(isGroupMessage){
            link = subscription.rid + "," + titleOfAlert
            type = "group_chat"
        }
        else {
            link = subscription.rid + "," + (AuthManager.currentUser()?.displayName())!
            type = "peer_chat"
        }
       
        let parameters : [String:AnyObject] = ["to":deviceToken, "priority" : "high" ,"notification" :["body" : message ,"title" : titleOfAlert  ,"click_action" : "org.jitsi.meet.activities.LoginActivity"
            ,"sound" : "message_beep_tone.mp3"], "data" : ["link" : link, "type":type ]] as [String : AnyObject]
        print("parameters = \(parameters)")
        let jsonObject = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        let jsonString = String(data: jsonObject! , encoding: .utf8)
        print("json string = \(jsonString)")
        Alamofire.request("https://fcm.googleapis.com/fcm/send", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers:headers)
            .responseJSON { response in
                
                print("response.request : \(response.request as Any)")  // original URL request
                print("response.response : \(response.response as Any)") // URL response
                print("result : \(response.result.value as Any)")
                
        }
    }
    
    
    @IBAction func action_receiveCall(_ sender: Any) {
        
//        self.incomingView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
//        self.view.addSubview(self.incomingView)
        
        let storyboardChat = UIStoryboard(name: "Chat", bundle: Bundle.main)
        let controller = storyboardChat.instantiateViewController(withIdentifier: "JitsiViewController")
        
        self.navigationController?.pushViewController(controller, animated: true)
         self.incomingView.removeFromSuperview()
    }
    
    
    @IBAction func action_declineCall(_ sender: Any) {
        self.incomingView.removeFromSuperview()
    }
    
    
    /*
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("notification received")
        //  print("\(response.notification.request.content.userInfo)")
        let dic : NSDictionary = response.notification.request.content.userInfo as NSDictionary
        print("dic = \(dic)")
        
        let aps : NSDictionary = (dic[AnyHashable("aps")] as? NSDictionary)!
        let alert = aps.value(forKey: "alert") as? String
        print(alert)
        if(dic["link"] as? String != nil)
        {
            let link : String = (dic["link"] as? String)!
            print(link)
            let defaults = UserDefaults.standard
            defaults.set(link, forKey: "serverurl1")
            defaults.set("1", forKey: "isTapped") //means user taps notification
            print("server url = \(alert)")
            let array : [String] = (alert?.components(separatedBy: " "))!
            let nameOfCaller : String = array[3] as String
            print("nameOfCaller = \(nameOfCaller)")
            defaults.set(nameOfCaller, forKey: "nameOfCaller")
            let storyboardChat = UIStoryboard(name: "Chat", bundle: Bundle.main)
            let controller = storyboardChat.instantiateViewController(withIdentifier: "JitsiViewController")
            // var rootViewController = (self.window!.rootViewController as? UINavigationController)!
            //  window?.rootViewController = controller
            if let main = UIApplication.shared.delegate?.window??.rootViewController as? MainChatViewController {
                if let nav = main.centerViewController as? UINavigationController {
                    nav.pushViewController(controller, animated: true)
                }
            }
        }
        else if(dic["ejson"] != nil)
        {
            let defaults = UserDefaults.standard
            defaults.set("1", forKey: "ejson")
            let storyboardChat = UIStoryboard(name: "Chat", bundle: Bundle.main)
            let controller = storyboardChat.instantiateViewController(withIdentifier: "ChatViewController")
            if(UserDefaults.standard.value(forKey: "ejson") != nil)
            {
                //MainChatViewController.openSideMenu()
                UserDefaults.standard.removeObject(forKey: "ejson")
            }
        }
        
        completionHandler()
        //  window?.rootViewController?.present(controller, animated: true, completion: nil)
    }

    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("notification received in will present")
        //  completionHandler(.badge)
        let dic : NSDictionary = notification.request.content.userInfo as NSDictionary
        print("dic = \(dic)")
        completionHandler([.alert , .badge , .sound])
        
    }
 */
    func generateRandomStringWithLength(length: Int) -> String {
        var randomString = ""
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        
        for _ in 1...length {
            let randomIndex  = Int(arc4random_uniform(UInt32(letters.characters.count)))
            let a = letters.index(letters.startIndex, offsetBy: randomIndex)
            randomString +=  String(letters[a])
        }
        
        return randomString
    }
    
    func textView(textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange) -> Bool {
        return true
    }

    @objc internal func reconnect() {
        chatHeaderViewStatus?.activityIndicator.startAnimating()
        chatHeaderViewStatus?.buttonRefresh.isHidden = true

        if !SocketManager.isConnected() {
            SocketManager.reconnect()
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            if !SocketManager.isConnected() {
                self.chatHeaderViewStatus?.activityIndicator.stopAnimating()
                self.chatHeaderViewStatus?.buttonRefresh.isHidden = false
            }
        }
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        let insets = UIEdgeInsets(
            top: 0,
            left: 0,
            bottom: chatPreviewModeView?.frame.height ?? 0,
            right: 0
        )

        collectionView?.contentInset = insets
        collectionView?.scrollIndicatorInsets = insets
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        coordinator.animate(alongsideTransition: nil, completion: { _ in
            self.collectionView?.collectionViewLayout.invalidateLayout()
        })
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nav = segue.destination as? UINavigationController, segue.identifier == "Channel Info" {
            if let controller = nav.viewControllers.first as? ChannelInfoViewController {
                if let subscription = self.subscription {
                    controller.subscription = subscription
                }
            }
        }
    }

    fileprivate func setupTextViewSettings() {
        textInputbar.autoHideRightButton = true

        textView.registerMarkdownFormattingSymbol("*", withTitle: "Bold")
        textView.registerMarkdownFormattingSymbol("_", withTitle: "Italic")
        textView.registerMarkdownFormattingSymbol("~", withTitle: "Strike")
        textView.registerMarkdownFormattingSymbol("`", withTitle: "Code")
        textView.registerMarkdownFormattingSymbol("```", withTitle: "Preformatted")
        textView.registerMarkdownFormattingSymbol(">", withTitle: "Quote")

        
        registerPrefixes(forAutoCompletion: ["@", "#"])
    }

    fileprivate func setupTitleView() {
        let view = ChatTitleView.instantiateFromNib()
        self.navigationItem.titleView = view
        chatTitleView = view

        let gesture = UITapGestureRecognizer(target: self, action: #selector(chatTitleViewDidPressed))
        chatTitleView?.addGestureRecognizer(gesture)
    }

    fileprivate func setupScrollToBottomButton() {
        buttonScrollToBottom.layer.cornerRadius = 25
        buttonScrollToBottom.layer.borderColor = UIColor.lightGray.cgColor
        buttonScrollToBottom.layer.borderWidth = 1
    }

    override class func collectionViewLayout(for decoder: NSCoder) -> UICollectionViewLayout {
        return ChatCollectionViewFlowLayout()
    }

    fileprivate func registerCells() {
        collectionView?.register(UINib(
            nibName: "ChatLoaderCell",
            bundle: Bundle.main
        ), forCellWithReuseIdentifier: ChatLoaderCell.identifier)

        collectionView?.register(UINib(
            nibName: "ChatMessageCell",
            bundle: Bundle.main
        ), forCellWithReuseIdentifier: ChatMessageCell.identifier)

        collectionView?.register(UINib(
            nibName: "ChatMessageDaySeparator",
            bundle: Bundle.main
        ), forCellWithReuseIdentifier: ChatMessageDaySeparator.identifier)

        collectionView?.register(UINib(
            nibName: "ChatChannelHeaderCell",
            bundle: Bundle.main
        ), forCellWithReuseIdentifier: ChatChannelHeaderCell.identifier)

        collectionView?.register(UINib(
            nibName: "ChatDirectMessageHeaderCell",
            bundle: Bundle.main
        ), forCellWithReuseIdentifier: ChatDirectMessageHeaderCell.identifier)

        autoCompletionView.register(UINib(
            nibName: "AutocompleteCell",
            bundle: Bundle.main
        ), forCellReuseIdentifier: AutocompleteCell.identifier)
    }

    internal func scrollToBottom(_ animated: Bool = false) {
        let boundsHeight = collectionView?.bounds.size.height ?? 0
        let sizeHeight = collectionView?.contentSize.height ?? 0
        let offset = CGPoint(x: 0, y: max(sizeHeight - boundsHeight, 0))
        collectionView?.setContentOffset(offset, animated: animated)
        showButtonScrollToBottom = false
    }

    // MARK: SlackTextViewController

    override func canPressRightButton() -> Bool {
        return SocketManager.isConnected()
    }

    override func didPressRightButton(_ sender: Any?) {
        sendMessage()
    }

    override func didPressLeftButton(_ sender: Any?) {
        buttonUploadDidPressed()
    }

    override func didPressReturnKey(_ keyCommand: UIKeyCommand?) {
        sendMessage()
    }

    override func textViewDidBeginEditing(_ textView: UITextView) {
        scrollToBottom(true)
    }

    override func textViewDidChange(_ textView: UITextView) {
        if textView.text.isEmpty {
            SubscriptionManager.sendTypingStatus(subscription, isTyping: false)
        } else {
            SubscriptionManager.sendTypingStatus(subscription, isTyping: true)
        }
    }

    // MARK: Message
    fileprivate func sendMessage() {
        print("send message is called")
        print(textView.text)
        guard let messageText = textView.text, messageText.characters.count > 0 else { return }

        let replyString = self.replyString
        stopReplying()

        self.scrollToBottom()
        rightButton.isEnabled = false

        var message: Message?
        Realm.executeOnMainThread({ (realm) in
            message = Message()
            message?.internalType = ""
            message?.createdAt = Date.serverDate
            message?.text = "\(messageText)\(replyString)"
            message?.subscription = self.subscription
            message?.identifier = String.random(18)
            message?.temporary = true
            message?.user = AuthManager.currentUser()

            if let message = message {
                realm.add(message)
            }
        })

        if let message = message {
            textView.text = ""
            rightButton.isEnabled = true
            SubscriptionManager.sendTypingStatus(subscription, isTyping: false)

            SubscriptionManager.sendTextMessage(message) { response in
                Realm.executeOnMainThread({ (realm) in
                    message.temporary = false
                    message.map(response.result["result"], realm: realm)
                    realm.add(message, update: true)

                    MessageTextCacheManager.shared.update(for: message)
                })
            }
        }
        
        if(subscription.type.rawValue == "p")
        {
            self.callApiForGroupMessage(message: messageText)
        }
        else if(subscription.type.rawValue == "c")
        {
            self.callApiForChannelMessage(message: messageText)
        }
        else if(subscription.type.rawValue == "d")
        {
            self.callApiForMessage(message: messageText)
            
        }
    }

    fileprivate func chatLogIsAtBottom() -> Bool {
        guard let collectionView = collectionView else { return false }

        let height = collectionView.bounds.height
        let bottomInset = collectionView.contentInset.bottom
        let scrollContentSizeHeight = collectionView.contentSize.height
        let verticalOffsetForBottom = scrollContentSizeHeight + bottomInset - height

        return collectionView.contentOffset.y >= (verticalOffsetForBottom - 1)
    }

    // MARK: Subscription

    fileprivate func markAsRead() {
        SubscriptionManager.markAsRead(subscription) { _ in
            // Nothing, for now
        }
    }

    internal func unsubscribe(for subscription: Subscription) {
        SocketManager.unsubscribe(eventName: subscription.rid)
        SocketManager.unsubscribe(eventName: "\(subscription.rid)/typing")
    }

    internal func updateSubscriptionInfo() {
        print("update subscription info is called")
        MainChatViewController.closeSideMenu()
        if let token = messagesToken {
            token.invalidate()
        }
        title = subscription?.displayName() 
        chatTitleView?.subscription = subscription
        textView.resignFirstResponder()

        collectionView?.performBatchUpdates({
            let indexPaths = self.dataController.clear()
            self.collectionView?.deleteItems(at: indexPaths)
        }, completion: { _ in
            CATransaction.commit()

            if self.closeSidebarAfterSubscriptionUpdate {
                MainChatViewController.closeSideMenuIfNeeded()
                self.closeSidebarAfterSubscriptionUpdate = false
            }
        })

        if self.subscription.isValid() {
            self.updateSubscriptionMessages()
        } else {
            self.subscription.fetchRoomIdentifier({ [weak self] response in
                self?.subscription = response
            })
        }

        if subscription.isJoined() {
            setTextInputbarHidden(false, animated: false)
            chatPreviewModeView?.removeFromSuperview()
        } else {
            setTextInputbarHidden(true, animated: false)
            showChatPreviewModeView()
        }
    }

    internal func updateSubscriptionMessages() {
        messagesQuery = subscription.fetchMessagesQueryResults()
        
        activityIndicator.startAnimating()

        dataController.loadedAllMessages = false
        isRequestingHistory = false
        updateMessagesQueryNotificationBlock()
        loadMoreMessagesFrom(date: nil)

        MessageManager.changes(subscription)
        registerTypingEvent()
    }

    fileprivate func registerTypingEvent() {
        typingIndicatorView?.interval = 0

        SubscriptionManager.subscribeTypingEvent(subscription) { [weak self] username, flag in
            guard let username = username else { return }

            let isAtBottom = self?.chatLogIsAtBottom()

            if flag {
                self?.typingIndicatorView?.insertUsername(username)
            } else {
                self?.typingIndicatorView?.removeUsername(username)
            }

            if let isAtBottom = isAtBottom,
                isAtBottom == true {
                self?.scrollToBottom(true)
            }
        }
    }
    
    /*
     let messagesCount = self.messagesQuery.count
     -            var indexPathModifications: [Int] = []
     
     -            for modified in modifications {
     -                if messagesCount < modified + 1 {
     -                    continue
     -                }
     -
     -                let message = Message(value: self.messagesQuery[modified])
     -                let index = self.dataController.update(message)
     -                if index >= 0 && !indexPathModifications.contains(index) {
     -                    indexPathModifications.append(index)
     -                }
     -            }
     -
     -            if indexPathModifications.count > 0 {
     +            if modifications.count > 0 {
     DispatchQueue.main.async {
     let isAtBottom = self.chatLogIsAtBottom()
     +
     UIView.performWithoutAnimation {
     self.collectionView?.performBatchUpdates({
     +                            var indexPathModifications: [Int] = []
     +
     +                            for modified in modifications {
     +                                if messagesCount < modified + 1 {
     +                                    continue
     +                                }
     +
     +                                let message = Message(value: self.messagesQuery[modified])
     +                                let index = self.dataController.update(message)
     +                                if index >= 0 && !indexPathModifications.contains(index) {
     +                                    indexPathModifications.append(index)
     +                                }
     +                            }
     +
     self.collectionView?.reloadItems(at: indexPathModifications.map { IndexPath(row: $0, section: 0) })
     }, completion: { _ in
     if isAtBottom {
 */

    fileprivate func updateMessagesQueryNotificationBlock() {
        messagesToken?.invalidate()
        messagesToken = messagesQuery.observe { [unowned self] changes in
            guard case .update(_, _, let insertions, let modifications) = changes else {
                return
            }

            if insertions.count > 0 {
                var newMessages: [Message] = []
                for insertion in insertions {
                    let newMessage = Message(value: self.messagesQuery[insertion])
                    print("new message = \(newMessage)")
                    newMessages.append(newMessage)
                }
                print("new messages : \(newMessages)")

                self.messages.append(contentsOf: newMessages)
                self.appendMessages(messages: newMessages, completion: {
                    self.markAsRead()
                })
            }

            if modifications.count == 0 {
                return
            }
            
            

            let messagesCount = self.messagesQuery.count
            var indexPathModifications: [Int] = []

            for modified in modifications {
                if messagesCount < modified + 1 {
                    continue
                }

                let message = Message(value: self.messagesQuery[modified])
                let index = self.dataController.update(message)
                if index >= 0 && !indexPathModifications.contains(index) {
                    indexPathModifications.append(index)
                }
            }
            
            
            if indexPathModifications.count > 0 {
                if modifications.count > 0 {
                   
                    DispatchQueue.main.async {
                    let isAtBottom = self.chatLogIsAtBottom()
                    UIView.performWithoutAnimation {
                        
                        self.collectionView?.performBatchUpdates({
                            var indexPathModifications: [Int] = []
                            for modified in modifications {
                                if messagesCount < modified + 1 {
                                    continue
                                }
                            
                            let message = Message(value: self.messagesQuery[modified])
                            let index = self.dataController.update(message)
                            if index >= 0 && !indexPathModifications.contains(index) {
                            indexPathModifications.append(index)
                            }
                        }
                            self.collectionView?.reloadItems(at: indexPathModifications.map { IndexPath(row: $0, section: 0) })
                        }, completion: { _ in
                            if isAtBottom {
                                self.scrollToBottom()
                            }
                        })
                    }
                }
                    }            }
        }
    }
    
    func loadHistoryFromRemote(date: Date?) {
        let tempSubscription = Subscription(value: self.subscription)

        MessageManager.getHistory(tempSubscription, lastMessageDate: date) { [weak self] messages in
            DispatchQueue.main.async {
                self?.activityIndicator.stopAnimating()
                self?.isRequestingHistory = false
                self?.loadMoreMessagesFrom(date: date, loadRemoteHistory: false)

                if messages.count == 0 {
                    self?.dataController.loadedAllMessages = true

                    self?.collectionView?.performBatchUpdates({
                        if let (indexPaths, removedIndexPaths) = self?.dataController.insert([]) {
                            self?.collectionView?.insertItems(at: indexPaths)
                            self?.collectionView?.deleteItems(at: removedIndexPaths)
                        }
                    }, completion: nil)
                } else {
                    self?.dataController.loadedAllMessages = false
                }
            }
        }
    }

    fileprivate func loadMoreMessagesFrom(date: Date?, loadRemoteHistory: Bool = true) {
        if isRequestingHistory || dataController.loadedAllMessages {
            return
        }

        isRequestingHistory = true

        let newMessages = subscription.fetchMessages(lastMessageDate: date).map({ Message(value: $0) })
        if newMessages.count > 0 {
            messages.append(contentsOf: newMessages)
            appendMessages(messages: newMessages, completion: { [weak self] in
                self?.activityIndicator.stopAnimating()

                if date == nil {
                    self?.scrollToBottom()
                }

                if SocketManager.isConnected() {
                    if !loadRemoteHistory {
                        self?.isRequestingHistory = false
                    } else {
                        self?.loadHistoryFromRemote(date: date)
                    }
                } else {
                    self?.isRequestingHistory = false
                }
            })
        } else {
            if SocketManager.isConnected() {
                if loadRemoteHistory {
                    loadHistoryFromRemote(date: date)
                } else {
                    isRequestingHistory = false
                }
            } else {
                isRequestingHistory = false
            }
        }
    }

    fileprivate func appendMessages(messages: [Message], completion: VoidCompletion?) {
        guard let collectionView = self.collectionView else { return }
        guard !isAppendingMessages else {
            Log.debug("[APPEND MESSAGES] Blocked trying to append \(messages.count) messages")

            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: { [weak self] in
                self?.appendMessages(messages: messages, completion: completion)
            })

            return
        }

        isAppendingMessages = true

        var tempMessages: [Message] = []
        for message in messages {
            tempMessages.append(Message(value: message))
        }

        DispatchQueue.global(qos: .background).async {
            var objs: [ChatData] = []
            var newMessages: [Message] = []

            // Do not add duplicated messages
            for message in tempMessages {
                var insert = true

                for obj in self.dataController.data
                    where message.identifier == obj.message?.identifier {
                        insert = false
                }

                if insert {
                    newMessages.append(message)
                }
            }

            // Normalize data into ChatData object
            for message in newMessages {
                guard let createdAt = message.createdAt else { continue }
                var obj = ChatData(type: .message, timestamp: createdAt)
                obj.message = message
                objs.append(obj)
            }

            // No new data? Don't update it then
            if objs.count == 0 {
                DispatchQueue.main.async {
                    self.isAppendingMessages = false
                    completion?()
                }

                return
            }

            DispatchQueue.main.async {
                collectionView.performBatchUpdates({
                    let (indexPaths, removedIndexPaths) = self.dataController.insert(objs)
                    collectionView.insertItems(at: indexPaths)
                    collectionView.deleteItems(at: removedIndexPaths)
                }, completion: { _ in
                    self.isAppendingMessages = false
                    completion?()
                })
            }
        }
    }

    fileprivate func showChatPreviewModeView() {
        chatPreviewModeView?.removeFromSuperview()

        if let previewView = ChatPreviewModeView.instantiateFromNib() {
            previewView.delegate = self
            previewView.subscription = subscription
            previewView.frame = CGRect(x: 0, y: view.frame.height - previewView.frame.height, width: view.frame.width, height: previewView.frame.height)
            view.addSubview(previewView)
            chatPreviewModeView = previewView
        }
    }

    fileprivate func isContentBiggerThanContainerHeight() -> Bool {
        if let contentHeight = self.collectionView?.contentSize.height {
            if let collectionViewHeight = self.collectionView?.frame.height {
                if contentHeight < collectionViewHeight {
                    return false
                }
            }
        }

        return true
    }

    // MARK: IBAction

    @objc func chatTitleViewDidPressed(_ sender: AnyObject) {
        performSegue(withIdentifier: "Channel Info", sender: sender)
    }

    @IBAction func buttonScrollToBottomPressed(_ sender: UIButton) {
        scrollToBottom(true)
    }
}

// MARK: UICollectionViewDataSource

extension ChatViewController {

    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row < 4 {
            if let message = dataController.oldestMessage() {
                loadMoreMessagesFrom(date: message.createdAt)
            }
        }
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return dataController.data.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard dataController.data.count > indexPath.row else { return UICollectionViewCell() }
        guard let obj = dataController.itemAt(indexPath) else { return UICollectionViewCell() }

        if obj.type == .message {
            return cellForMessage(obj, at: indexPath)
        }

        if obj.type == .daySeparator {
            return cellForDaySeparator(obj, at: indexPath)
        }

        if obj.type == .loader {
            return cellForLoader(obj, at: indexPath)
        }

        if obj.type == .header {
            if subscription.type == .directMessage {
                return cellForDMHeader(obj, at: indexPath)
            } else {
                return cellForChannelHeader(obj, at: indexPath)
            }
        }
        return UICollectionViewCell()
    }

    // MARK: Cells

    func cellForMessage(_ obj: ChatData, at indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView?.dequeueReusableCell(
            withReuseIdentifier: ChatMessageCell.identifier,
            for: indexPath
        ) as? ChatMessageCell else {
            return UICollectionViewCell()
        }
        

        cell.delegate = self

        if let message = obj.message {
            cell.message = message
            
        }

        cell.sequential = dataController.hasSequentialMessageAt(indexPath)

        return cell
    }

    func cellForDaySeparator(_ obj: ChatData, at indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView?.dequeueReusableCell(
            withReuseIdentifier: ChatMessageDaySeparator.identifier,
            for: indexPath
        ) as? ChatMessageDaySeparator else {
                return UICollectionViewCell()
        }
        cell.labelTitle.text = obj.timestamp.formatted("MMM dd, YYYY")
        return cell
    }

    func cellForChannelHeader(_ obj: ChatData, at indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView?.dequeueReusableCell(
            withReuseIdentifier: ChatChannelHeaderCell.identifier,
            for: indexPath
        ) as? ChatChannelHeaderCell else {
            return UICollectionViewCell()
        }
        cell.subscription = subscription
        return cell
    }

    func cellForDMHeader(_ obj: ChatData, at indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView?.dequeueReusableCell(
            withReuseIdentifier: ChatDirectMessageHeaderCell.identifier,
            for: indexPath
        ) as? ChatDirectMessageHeaderCell else {
            return UICollectionViewCell()
        }
        cell.subscription = subscription
        return cell
    }

    func cellForLoader(_ obj: ChatData, at indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView?.dequeueReusableCell(
            withReuseIdentifier: ChatLoaderCell.identifier,
            for: indexPath
        ) as? ChatLoaderCell else {
            return UICollectionViewCell()
        }

        return cell
    }

}

// MARK: UICollectionViewDelegateFlowLayout

extension ChatViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let fullWidth = collectionView.bounds.size.width

        if let obj = dataController.itemAt(indexPath) {
            if obj.type == .header {
                if subscription.type == .directMessage {
                    return CGSize(width: fullWidth, height: ChatDirectMessageHeaderCell.minimumHeight)
                } else {
                    return CGSize(width: fullWidth, height: ChatChannelHeaderCell.minimumHeight)
                }
            }

            if obj.type == .loader {
                return CGSize(width: fullWidth, height: ChatLoaderCell.minimumHeight)
            }

            if obj.type == .daySeparator {
                return CGSize(width: fullWidth, height: ChatMessageDaySeparator.minimumHeight)
            }

            if let message = obj.message {
                let sequential = dataController.hasSequentialMessageAt(indexPath)
                let height = ChatMessageCell.cellMediaHeightFor(message: message, sequential: sequential)
                return CGSize(width: fullWidth, height: height)
            }
        }

        return CGSize(width: fullWidth, height: 40)
    }
}

// MARK: UIScrollViewDelegate

extension ChatViewController {
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        super.scrollViewDidScroll(scrollView)

        if scrollView.contentOffset.y < -10 {
            if let message = dataController.oldestMessage() {
                loadMoreMessagesFrom(date: message.createdAt)
            }
        }

        showButtonScrollToBottom = !chatLogIsAtBottom()
    }
}

// MARK: ChatPreviewModeViewProtocol

extension ChatViewController: ChatPreviewModeViewProtocol {

    func userDidJoinedSubscription() {
        guard let auth = AuthManager.isAuthenticated() else { return }
        guard let subscription = self.subscription else { return }

        Realm.executeOnMainThread({ _ in
            subscription.auth = auth
        })

        self.subscription = subscription
    }

}

extension NSMutableAttributedString {
    
    public func setAsLink(textToFind:String, linkURL:String) -> Bool {
        
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(NSAttributedStringKey.link, value: linkURL, range: foundRange)
            return true
        }
        return false
    }
}

