//
//  CallSessionPJSUA.h
//  Rocket.Chat
//
//  Created by Riyaz Lakhani on 14/02/18.
//  Copyright © 2018 Rocket.Chat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <pjsua.h>
#import <pjsip.h>
#import <pjnath.h>
#import <pjlib.h>
#import <pjsip_ua.h>
#import <pjlib-util.h>

@interface CallSessionPJSUA : NSObject{
    pjsua_call_info call_info;
}

- (void)saveSession :(pjsua_call_info)info;


@property(strong,nonatomic) NSMutableArray * calllArray;
@end
