//
//  ConferenceCell.m
//  Rocket.Chat
//
//  Created by Riyaz Lakhani on 23/03/18.
//  Copyright © 2018 Rocket.Chat. All rights reserved.
//

#import "ConferenceCell.h"

@implementation ConferenceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
