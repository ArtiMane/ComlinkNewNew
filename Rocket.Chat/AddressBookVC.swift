//
//  AddressBookVC.swift
//  Rocket.Chat
//
//  Created by Riyaz Lakhani on 08/02/18.
//  Copyright © 2018 Rocket.Chat. All rights reserved.
//

import UIKit
import ContactsUI
import SwiftyJSON

// swiftlint:disable Trailing Whitespace
// swiftlint:disable trailing_whitespace
// swiftlint:disable colon
// swiftlint:disable control_statement
// swiftlint:disable vertical_whitespace
// swiftlint:disable opening_brace
// swiftlint:disable statement_position
// swiftlint:disable force_unwrapping
// swiftlint:disable legacy_constructor
// swiftlint:disable closure_parameter_position
class AddressBookVC: UIViewController,CNContactPickerDelegate , UITableViewDelegate , UITableViewDataSource  ,UITextFieldDelegate ,UISearchResultsUpdating,UISearchBarDelegate , UISearchControllerDelegate{
    

    let searchController = UISearchController(searchResultsController: nil)
    @IBOutlet weak var cancelButtons: UIButton!
    @IBOutlet weak var tapButtonn: UIButton!
    @IBOutlet weak var phoneBookView: UIView!
    @IBAction func action_call(_ sender: Any) {
      
        if(self.keytext.text == ""){
            let alertController : UIAlertController = UIAlertController(title: "Message", message: "Please enter mobile number.", preferredStyle: .alert)
            let action : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alertController.removeFromParentViewController()
            })
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        }
        else{
         let callscreen : CallScreenViewController  = (self.storyboard?.instantiateViewController(withIdentifier: "CallScreenViewController") as? CallScreenViewController)!
            print(self.keytext.text)
            var finalNumber  : String = ""
            var secndNum : NSString = (self.keytext.text as? NSString)!
            var myRange1: NSRange = NSMakeRange(0, secndNum.length)
            var secondPhoneNumber: String = secndNum.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression, range: myRange1)
            print(secondPhoneNumber)
        for contact in contacts {
            let userPhoneNumbers:[CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
            let firstPhoneNumber:CNPhoneNumber = userPhoneNumbers[0].value
            let primaryPhoneNumberStr:NSString = firstPhoneNumber.stringValue as NSString
            var myRange: NSRange = NSMakeRange(0, primaryPhoneNumberStr.length)
            var newPhoneNumber: String = primaryPhoneNumberStr.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression, range: myRange)
            print(newPhoneNumber)
            
            if(newPhoneNumber == secondPhoneNumber){
                print("yes")
                 finalNumber = contact.givenName + " " + contact.familyName
                break
            }
            else {
                print("no")
                finalNumber = "Unknown"
            }
           }
        callscreen.calleeName1 = finalNumber
        callscreen.phoneNumber = self.keytext.text
        self.navigationController?.pushViewController(callscreen, animated: true)
     //   self.openView.removeFromSuperview()
            self.phoneBookView.isHidden = true
        }
    }
    @IBAction func action_closeView(_ sender: Any) {
        self.openView.removeFromSuperview()
    }
    @IBOutlet weak var keytext: UITextField!
    @IBOutlet var openView: UIView!
    @IBOutlet weak var tableView: UITableView!
    let pjcall : PJ = PJ()
    var contacts = [CNContact]()
    var filteredContacts = [CNContact]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.keytext.delegate = self
        let dummyView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        self.keytext.inputView = dummyView
        let username : NSString = "1000577734"
        let pass : NSString  = "2e5b206-7"
        let host : NSString  = "209.15.239.82"
        let port : NSString = "5060"
   
        self.phoneBookView.isHidden = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(normalTap(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapButtonn.addGestureRecognizer(tapGesture)
        
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        tapButtonn.addGestureRecognizer(longGesture)
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(normalTap1(_:)))
        tapGesture.numberOfTapsRequired = 1
        cancelButtons.addGestureRecognizer(tapGesture1)
        
        let longGesture1 = UILongPressGestureRecognizer(target: self, action: #selector(longTap1(_:)))
        cancelButtons.addGestureRecognizer(longGesture1)
        
        let req = CNContactFetchRequest(keysToFetch: [
            CNContactFamilyNameKey as CNKeyDescriptor,
            CNContactGivenNameKey as CNKeyDescriptor ,
            CNContactPhoneNumbersKey as CNKeyDescriptor
            ])
        req.sortOrder = CNContactSortOrder.userDefault
        try? CNContactStore().enumerateContacts(with: req) {
            contact, stop in
            if (!contact.phoneNumbers.isEmpty) {
               //  print(contact)
                self.contacts.append(contact)
                self.tableView.reloadData()
            }
           
//            if (contact.givenName == nil && contact.familyName == nil){
//
//            }
//            else {
                // in real life, probably populate an array}
            
//        }
        }
        
        // Search Controller
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Contact"
//        if #available(iOS 11.0, *) {
//            navigationItem.searchController = searchController
//        } else {
//            // Fallback on earlier versions
//        }
        if (!(searchController.searchBar.superview != nil)) {
            self.tableView.tableHeaderView = self.searchController.searchBar;
        }
//        if (!searchController.isActive && (searchController.searchBar.text?.isEmpty)!) {
//            self.tableView.contentOffset = CGPoint(x: 0, y: 44)
//            //self.tableView.contentOffset = CGPointMake(0, CGRectGetHeight(self.searchController.searchBar.frame));
//        }
        definesPresentationContext = true
        
        // Setup the Scope Bar
     //   searchController.searchBar.scopeButtonTitles = ["All", "Chocolate", "Hard", "Other"]
       // searchController.searchBar.scopeButtonTitles = ["All"]
        searchController.searchBar.delegate = self
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(isFiltering()){
            return filteredContacts.count
        }
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  : AddressBookCell = (tableView.dequeueReusableCell(withIdentifier: "Cell") as? AddressBookCell)!
        
        if(isFiltering()){
            cell.name.text = filteredContacts[indexPath.row].givenName + " " + filteredContacts[indexPath.row].familyName
            let contact : CNContact = filteredContacts[indexPath.row]
            let userName:String = contact.givenName
            
            // user phone number
            if contact.isKeyAvailable(CNContactPhoneNumbersKey) {
                let userPhoneNumbers : [CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
                let firstPhoneNumber : CNPhoneNumber = userPhoneNumbers[0].value
                let primaryPhoneNumberStr : String = firstPhoneNumber.stringValue
                cell.phoneNumber.text = primaryPhoneNumberStr
            }
        }
        else {
        cell.name.text = contacts[indexPath.row].givenName + " " + contacts[indexPath.row].familyName
        let contact : CNContact = contacts[indexPath.row]
        let userName:String = contact.givenName
        
        // user phone number
        if contact.isKeyAvailable(CNContactPhoneNumbersKey) {
            let userPhoneNumbers : [CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
            let firstPhoneNumber : CNPhoneNumber = userPhoneNumbers[0].value
            let primaryPhoneNumberStr : String = firstPhoneNumber.stringValue
            cell.phoneNumber.text = primaryPhoneNumberStr
        }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(isFiltering()){
            let contact : CNContact = filteredContacts[indexPath.row]
            let userName:String = contact.givenName
            // user phone number
            let userPhoneNumbers:[CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
            let firstPhoneNumber:CNPhoneNumber = userPhoneNumbers[0].value
            // user phone number string
            let primaryPhoneNumberStr:String = firstPhoneNumber.stringValue
            print(primaryPhoneNumberStr)
            let callscreen : CallScreenViewController  = (self.storyboard?.instantiateViewController(withIdentifier: "CallScreenViewController") as? CallScreenViewController)!
            callscreen.phoneNumber = primaryPhoneNumberStr
            callscreen.calleeName1 = filteredContacts[indexPath.row].givenName + " " + filteredContacts[indexPath.row].familyName
            DispatchQueue.main.async {
                self.searchController.isActive = false
            }
            self.navigationController?.pushViewController(callscreen, animated: true)
            self.tableView.deselectRow(at: indexPath, animated: true)
           
            
           // self.navigationController?.setNavigationBarHidden(true, animated: true)
            
            
        }
        else {
            let contact : CNContact = contacts[indexPath.row]
            let userName:String = contact.givenName
            // user phone number
            let userPhoneNumbers:[CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
            let firstPhoneNumber:CNPhoneNumber = userPhoneNumbers[0].value
            // user phone number string
            let primaryPhoneNumberStr:String = firstPhoneNumber.stringValue
            print(primaryPhoneNumberStr)
            let callscreen : CallScreenViewController  = (self.storyboard?.instantiateViewController(withIdentifier: "CallScreenViewController") as? CallScreenViewController)!
            callscreen.phoneNumber = primaryPhoneNumberStr
            callscreen.calleeName1 = contacts[indexPath.row].givenName + " " + contacts[indexPath.row].familyName
            self.navigationController?.pushViewController(callscreen, animated: true)
            self.tableView.deselectRow(at: indexPath, animated: true)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.0
    }

    @IBAction func action_keys(_ sender: UIButton) {
        
        let newPosition = self.keytext.endOfDocument
        self.keytext.selectedTextRange = self.keytext.textRange(from: newPosition, to: newPosition)
        
        var textfiledText : String = self.keytext.text!
      
        if(sender.tag == 1){
          self.keytext.text = textfiledText + "1"
        }
        if(sender.tag == 2){
            self.keytext.text = textfiledText + "2"
        }
        if(sender.tag == 3){
            self.keytext.text = textfiledText + "3"
        }
        if(sender.tag == 4){
            self.keytext.text = textfiledText + "4"
        }
        if(sender.tag == 5){
            self.keytext.text = textfiledText + "5"
        }
        if(sender.tag == 6){
            self.keytext.text = textfiledText + "6"
        }
        if(sender.tag == 7){
            self.keytext.text = textfiledText + "7"
        }
        if(sender.tag == 8){
            self.keytext.text = textfiledText + "8"
        }
        if(sender.tag == 9){
            self.keytext.text = textfiledText + "9"
        }
        if(sender.tag == 10){
            self.keytext.text = textfiledText + "*"
        }
        if(sender.tag == 11){
            self.keytext.text = textfiledText + "0"
        }
        if(sender.tag == 12){
//            if(textfiledText.count > 0){
//               textfiledText = textfiledText.substring(to: textfiledText.index(before: textfiledText.endIndex))
//                self.keytext.text = textfiledText
//            }
            self.keytext.text = textfiledText + "#"
        }
        
    }
    
    @IBAction func action_openkeyview(_ sender: Any) {
        
//        if(self.openView.isDescendant(of: self.view)){
//            self.openView.removeFromSuperview()
//            self.navigationController?.setNavigationBarHidden(false, animated: true)
//        }
//        else {
//            self.navigationController?.setNavigationBarHidden(true, animated: true)
//     //   self.openView.frame = CGRect(x: 0, y: 0, width: 360, height: 500)
//        self.openView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 500)
//       // self.openView.center = self.view.center
//        self.openView.layer.masksToBounds = true
//        self.openView.layer.cornerRadius = 8
//        self.openView.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
//        self.openView.layer.shadowRadius = 5
//        self.openView.layer.shadowOpacity = 0.5
//        self.openView.layer.borderWidth = 1
//        self.openView.layer.borderColor = UIColor.init(red:222/255.0, green:225/255.0, blue:227/255.0, alpha: 1.0).cgColor
//
//        self.view.addSubview(self.openView)
//
//    }
        
        if(self.phoneBookView.isHidden == true){
            self.phoneBookView.isHidden = false
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }
        else {
            self.phoneBookView.isHidden = true
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//
//        return false
//    }
    
    @IBAction func action_closesss(_ sender: Any) {
     //   UserDefaults.standard.removeObject(forKey: "Registered")
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func normalTap(_ sender: UIGestureRecognizer){
        print("Normal tap")
        var textfiledText : String = self.keytext.text!
        self.keytext.text = textfiledText + "0"
        
    }
    
    @objc func longTap(_ sender: UIGestureRecognizer){
        print("Long tap")
        var textfiledText : String = self.keytext.text!
        
        if sender.state == .ended {
            
            print("UIGestureRecognizerStateEnded")
            //Do Whatever You want on End of Gesture
        }
        else if sender.state == .began {
            self.keytext.text = textfiledText + "+"
            print("UIGestureRecognizerStateBegan.")
            //Do Whatever You want on Began of Gesture
        }
    }
    
    
    @IBAction func button_cancel(_ sender: Any) {
         var textfiledText : String = self.keytext.text!
        if let text = characterBeforeCursor() {
            print(text)
        }
        
        if let selectedRange = self.keytext.selectedTextRange {
            
            let cursorPosition = self.keytext.offset(from: self.keytext.beginningOfDocument, to: selectedRange.start)
            
            print("\(cursorPosition)")
        }
//
//        if(textfiledText.count > 0){
//                textfiledText = textfiledText.substring(to: textfiledText.index(before: textfiledText.endIndex))
//                self.keytext.text = textfiledText
//        }
    }
    
    @objc func normalTap1(_ sender: UIGestureRecognizer){
        var textfiledText  : String  = self.keytext.text!
//        if(textfiledText.count > 0){
//            textfiledText = textfiledText.substring(to: textfiledText.index(before: textfiledText.endIndex))
//           // textfiledText = textfiledText.substring(from: textfiledText.inde)
//            self.keytext.text = textfiledText
//        }
        if(textfiledText.count > 1){
        if let text = characterBeforeCursor() {
            print(text)
        }
        var position : Int = 0
        if let selectedRange = self.keytext.selectedTextRange {
        
            print("selected range = \(selectedRange)")
            
            let cursorPosition = self.keytext.offset(from: self.keytext.beginningOfDocument, to: selectedRange.start)
            position = cursorPosition
            print("\(cursorPosition)")
            
            let start : UITextPosition = selectedRange.start
            let end  : UITextPosition = selectedRange.end
            let range : UITextRange = self.keytext.textRange(from: start, to: end)!
            let length = self.keytext.offset(from: start, to: end)
            if(length > 1){
           
                self.keytext.replace(range, withText: "")
            }
            else {
                var myText  = self.keytext.text!
                if((position == 1) || ( position == 0)){
                    
                }
                else {
                    let index = myText.index(myText.startIndex, offsetBy: position-1)
                    myText.remove(at: index)
                }
                self.keytext.text = myText
            }
           
            
            
        }
        
            
        
       
        }
    
        else if(textfiledText.count > 0  && textfiledText.count == 1){
            textfiledText = textfiledText.substring(to: textfiledText.index(before: textfiledText.endIndex))
            self.keytext.text = textfiledText
        }
        

        
    }
    
    @objc func longTap1(_ sender: UIGestureRecognizer){
        print("Long tap")
        var textfiledText : String = self.keytext.text!
        
        if sender.state == .ended {
            
            print("UIGestureRecognizerStateEnded")
            //Do Whatever You want on End of Gesture
        }
        else if sender.state == .began {
             if(textfiledText.count > 0){
            for var i in 0..<textfiledText.count{
                textfiledText = textfiledText.substring(to: textfiledText.index(before: textfiledText.endIndex))
                self.keytext.text = textfiledText
            }
            }
        }
    }
    
    func characterBeforeCursor() -> String? {
        
        // get the cursor position
        if let cursorRange = self.keytext.selectedTextRange {
            
            // get the position one character before the cursor start position
            if let newPosition = self.keytext.position(from: cursorRange.start, offset: -1) {
                
                let range = self.keytext.textRange(from: newPosition, to: cursorRange.start)
                return self.keytext.text(in: range!)
            }
        }
        return nil
    }
    
    
    // MARK: Search Bar Controller Methods
    func updateSearchResults(for searchController: UISearchController) {
        print("update search result")
        let searchBar = searchController.searchBar
      //  let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        let scope = "All"
        filterContentForSearchText(searchController.searchBar.text!, scope: scope)
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredContacts = contacts.filter({( contact : CNContact) -> Bool in
            var primaryPhoneNumberStr : String = ""
            if contact.isKeyAvailable(CNContactPhoneNumbersKey) {
                let userPhoneNumbers : [CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
                let firstPhoneNumber : CNPhoneNumber = userPhoneNumbers[0].value
                primaryPhoneNumberStr = firstPhoneNumber.stringValue
                
            }
            let doesCategoryMatch = (scope == "All") || (contact.givenName == scope) || (primaryPhoneNumberStr == scope || (contact.familyName == scope))
           
            if searchBarIsEmpty() {
                return doesCategoryMatch
            } else {
                return doesCategoryMatch && (contact.givenName.lowercased().contains(searchText.lowercased()) || contact.familyName.lowercased().contains(searchText.lowercased()) || primaryPhoneNumberStr.contains(searchText) )
            }
        })
        tableView.reloadData()
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
