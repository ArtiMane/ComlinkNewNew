//
//  CallScreenViewController.m
//  appl
//
//  Created by Reuf Kicin on 10/31/16.
//  Copyright © 2016 Kicin. All rights reserved.
//

#import "CallScreenViewController.h"


//#import "pjsip-include/pjsua.h"
//#import "pjsip-include/pjsip.h"
//#import "pjsip-include/pjnath.h"
//#import "pjsip-include/pjlib.h"
//#import "pjsip-include/pjmedia.h"
//#import "pjsip-include/pjsip_ua.h"
//#import "pjsip-include/pjlib-util.h"
#import <pjsua.h>
#import <pjsip.h>
#import <pjnath.h>
#import <pjlib.h>
#import <pjsip_ua.h>
#import <pjlib-util.h>
#import <pjmedia.h>
#import <pjmedia_audiodev.h>


#pragma mark pjsip static functions prototypes

static void on_incoming_call(pjsua_acc_id accountID, pjsua_call_id callID, pjsip_rx_data *rdata);
static void on_call_state(pjsua_call_id callID, pjsip_event *event);
static void on_reg_state2(pjsua_acc_id accountID, pjsua_reg_info *info);
static void on_call_media_state(pjsua_call_id callID);
static void error_exit1(const char *msg, pj_status_t stat);
static void conf_list(pjmedia_conf *conf, pj_bool_t detail);
static void monitor_level(pjmedia_conf *conf, int slot, int dir, int dur);
static void stopPlaying();
static void startPlaying();
static void play_sound_during_call(pj_str_t sound_file);
pj_status_t play_sound_during_call1(pj_str_t sound_file);
static void playWavFile();
static void pauseWavFile();



NSMutableArray * callList , * callIdArray;
pjsua_call_id mycallID;
pj_pool_t           *pool;
pjmedia_conf        *conf;
pjmedia_port        *cport;
pj_pool_t *pjPool;
NSInteger ringbackSlot;
pjmedia_port *ringbackPort;
NSInteger ringbackCount;
pjmedia_snd_port *snd_port;
pjsua_player_id player_id , player_id1;


#pragma PJ interface and implementation

@interface PJ ()
@end

@implementation PJ{
    pjsua_acc_id accountID;
    RegisterCallBack callBack;
    
    
    
}


//init
+ (PJ *)sharedPJ{
  //  callList = [[NSMutableArray alloc]init];
    static PJ *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[PJ alloc] init];
    });
    
    return instance;
    
}


//Hang up call
- (void)endCall{
    pjsua_call_hangup_all();
    
   
}


//Make a call
- (void)makeCall:(char*)uri
{
    
    pj_status_t status;
    pj_str_t uriX = pj_str(uri);
    pj_status_t status1 = pjsua_verify_sip_url(uri);
    NSLog(@"status = %d",status1);
    status = pjsua_call_make_call(accountID, &uriX, 0, NULL, NULL, NULL);
    NSLog(@"status = %d",status);
    if(status != PJ_SUCCESS){
        return;
    }
    for (int i = 0; i < callIdArray.count ; i++){
        pjsua_call_id callId = [callIdArray[i] intValue];
        NSLog(@"callid i = %d",callId);
        pjsua_call_set_hold(callId, NULL);
    }
    pj_status_t status2;
 //   status2 = on_call_state(<#pjsua_call_id callID#>, <#pjsip_event *event#>)
    
    
//    if (status1 != PJ_SUCCESS)
//    {
//        NSLog(@"status = %d",status1);
//    }
    if(status != PJ_SUCCESS)
    {
       
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Can't make call"
                                                            message:nil
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        
            [alert show];
        error_exit1("Can't make call", status);
        }

        
}

-(int)startPjsipAndRegisterOnServer:(char *) domain                         withUserName:(char *) username andPassword1:(char *) pass{
#define THIS_FILE    "pjsua_vid.c"
    pj_status_t status;
    pjsua_acc_config cfg;
    if (accountID != PJSUA_INVALID_ID){
        
        //        pjsua_acc_info info;
        //        pjsua_acc_get_info(_sip_acc_id, &info);
        //
        //        if (info.has_registration){
        pj_status_t statusDelete = pjsua_acc_del(accountID);
        if (statusDelete != PJ_SUCCESS)
        {
            pjsua_perror(THIS_FILE, "Error removing new account", status);
            
        }
        pjsua_destroy();
        //        }
    }
    return 0;
    
}


- (int)startPjsipAndRegisterOnServer:(char *) domain                         withUserName:(char *) username andPassword:(char *) pass{
 
    pjsua_destroy();
    callList = [[NSMutableArray alloc]init];
    callIdArray = [[NSMutableArray alloc]init];
    ringbackCount = 0;
    pj_status_t status;
    
    
    status = pjsua_create();
    if(status != PJ_SUCCESS) error_exit1("Error", status);
    
    {
        pjsua_config cf;
        pjsua_config_default(&cf);
        pjsua_media_config mediaConfig;
        pjsua_media_config_default(&mediaConfig);
    
        cf.cb.on_call_state = &on_call_state;
      //  NSLog(@"call state = %d",cf.cb.on_call_state);
        
        cf.cb.on_incoming_call = &on_incoming_call;
        cf.cb.on_call_state = &on_call_state;
        cf.cb.on_call_media_state = &on_call_media_state;
     //   cf.cb.on_stream_created = &on_stream_created;
        
     //   cf.cb.on_reg_state2 = &on_reg_state2;
        
        pjsua_logging_config log_cfg;
        pjsua_logging_config_default(&log_cfg);
        log_cfg.console_level = 4;
        
    
        cf.stun_host =  pj_str("209.15.239.82:16069");
        cf.cb.on_nat_detect = &on_nat;
        
        mediaConfig.enable_ice = 1;
        mediaConfig.snd_auto_close_time = 1;

        mediaConfig.enable_turn = 1;
        mediaConfig.turn_server = pj_str("209.15.239.82:16069");
        mediaConfig.turn_auth_cred.data.static_cred.username = pj_str("comlink");
        mediaConfig.turn_auth_cred.data.static_cred.data = pj_str("$$4915216384");
//
        
        
        
  //      status = pjsua_init(&cf, &log_cfg, NULL);
        status = pjsua_init(&cf, &log_cfg, &mediaConfig);
        
        if(status != PJ_SUCCESS) error_exit1("Error in init", status);
        
    }
    
    
    //UDP and TCP
    {
        
        pjsua_transport_config cfg;
        pjsua_transport_config_default(&cfg);
        cfg.port = [@"5060" intValue];
        
        status = pjsua_transport_create(PJSIP_TRANSPORT_UDP, &cfg, NULL);
        if(status != PJ_SUCCESS) error_exit1("Error in creating transport", status);
        
//        status = pjsua_transport_create(PJSIP_TRANSPORT_TCP, &cfg, NULL);
//        if(status != PJ_SUCCESS) error_exit1("Error in creating transport", status);
        
    }
    
    status = pjsua_start();
    if(status != PJ_SUCCESS) error_exit1("Error in startiing pjsua", status);
    
    {
        
        pjsua_acc_config cfg;
        pjsua_acc_config_default(&cfg);
        
       
        username = [@"1000577737" UTF8String];
        char sipID[50];
        sprintf(sipID, "sip:%s@%s", username, domain);
        cfg.id = pj_str(sipID);
        
        
        //register uri
        
        char regURI[50];
        sprintf(regURI, "sip:%s", domain);
        cfg.reg_uri = pj_str(regURI);
        
        cfg.cred_count = 1;

        cfg.cred_info[0].scheme = pj_str((char *)"digest");
        cfg.cred_info[0].realm = pj_str((char *)"*");
        cfg.cred_info[0].username = pj_str(username);
        cfg.cred_info[0].data_type = PJSIP_CRED_DATA_PLAIN_PASSWD;
        cfg.cred_info[0].data = pj_str("3282b5a-2");
        
        
        NSString *searchFilename = @"holdmusic.wav"; // name of the PDF you are searching for
        NSString* filePath = [[NSBundle mainBundle] pathForResource:@"holdmusic" ofType:@"wav"];
        
        pj_str_t soundFile = pj_str([filePath UTF8String]);
        play_sound_during_call(soundFile);
        pj_status_t soundStatus =  play_sound_during_call1(soundFile);
        printf("soundStaus = %d",soundStatus);
        
        
        status = pjsua_acc_add(&cfg, PJ_TRUE, &accountID);
        if(status != PJ_SUCCESS) error_exit1("Error adding account", status);
    }
    
   // callBack = callback;
    
    return 0;
}


- (void)handleRegistrationStateChangeWithRegInfo: (pjsua_reg_info *) info
{

    switch(info->cbparam->code){
        case 200:
            callBack(YES);
            break;
        case 401:
            callBack(NO);
            break;
        default: break;
    }

    
    
}

static void on_nat(const pj_stun_nat_detect_result *result) {
    if (result->status != PJ_SUCCESS) {
        pjsua_perror(THIS_FILE, "NAT detection failed", result->status);
        
        
    } else {
        
        PJ_LOG(3, (THIS_FILE, "NAT detected as %s", result->nat_type_name));
        
    }
}

@end

#pragma mark CallScreenViewController interface and implementation
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
@interface CallScreenViewController ()

@end

@implementation CallScreenViewController

PJ *pjcall;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //Loading the user data
    
     [self.navigationController setNavigationBarHidden:YES animated:YES];
    NSUserDefaults *userdata = [NSUserDefaults standardUserDefaults];
    NSString *username =@"1000577737";
    NSString *pass =@"3282b5a-2";
    NSString *host = @"209.15.239.82";
    NSString *port = @"5060";
  
    
    self.name.text = self.calleeName1;
    self.mobileNumber.text = self.phoneNumber;
    self.statusLabell.text = @"Connecting";
    self.timerlabel.text = @"00:00:00";
    [self.timerlabel setHidden:YES];
    [self.statusLabell setHidden:NO];
    [self.endCallButtton setHidden:NO];
    [self.button_speaker1 setImage:[UIImage imageNamed:@"Dail pad-26-1.png"] forState:UIControlStateNormal];
    [self.muteButton setImage:[UIImage imageNamed:@"Dail pad-16.png"] forState:UIControlStateNormal];
    [self.bluetoothButton setImage:[UIImage imageNamed:@"Dail pad-25-1.png"] forState:UIControlStateNormal];
    [self.button_keypad setImage:[UIImage imageNamed:@"Dail pad-31.png"] forState:UIControlStateNormal];
    [self.button_addCall setImage:[UIImage imageNamed:@"Dail pad-32.png"] forState:UIControlStateNormal];
    [self.button_contacts setImage:[UIImage imageNamed:@"Dail pad-30.png"] forState:UIControlStateNormal];
    
    
    // Dial Pad Settings
    [self.dialingPadView setHidden:YES];
    self.keypadText.delegate = self;
    [self.hideButton setHidden: YES];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(normalTap:)];
    tap.numberOfTapsRequired = 1;
    [self.tappedButtton addGestureRecognizer:tap];
    
    UILongPressGestureRecognizer * longGesture = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longTap:)];
    [self.tappedButtton addGestureRecognizer:longGesture];
    
   // Add Call
    self.addCallTableView.delegate = self;
    self.addCallTableView.dataSource = self;
    contactsArray = [[NSMutableArray alloc]init];
    callArray = [[NSMutableArray alloc]init];
    callRecordDic = [[NSMutableDictionary alloc]init];
    callRecordArray = [[NSMutableArray alloc]init];
    callLogArray = [[NSMutableArray alloc]init];
   // callIdArray = [[NSMutableArray alloc]init];
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    if( status == CNAuthorizationStatusDenied || status == CNAuthorizationStatusRestricted)
    {
        NSLog(@"access denied");
    }
    else
    {
        //Create repository objects contacts
        CNContactStore *contactStore = [[CNContactStore alloc] init];
        //Select the contact you want to import the key attribute  ( https://developer.apple.com/library/watchos/documentation/Contacts/Reference/CNContact_Class/index.html#//apple_ref/doc/constant_group/Metadata_Keys )
        
        NSArray *keys = [[NSArray alloc]initWithObjects:CNContactIdentifierKey, CNContactEmailAddressesKey, CNContactBirthdayKey, CNContactImageDataKey, CNContactPhoneNumbersKey, CNContactViewController.descriptorForRequiredKeys, nil];
        
        // Create a request object
        CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keys];
        request.predicate = nil;
        request.sortOrder = CNContactSortOrderUserDefault;
        
        [contactStore enumerateContactsWithFetchRequest:request
                                                  error:nil
                                             usingBlock:^(CNContact* __nonnull contact, BOOL* __nonnull stop)
         {
             // Contact one each function block is executed whenever you get
             NSString *phoneNumber = @"";
             if(contact.phoneNumbers.count != 0){
                 phoneNumber = [[[contact.phoneNumbers firstObject] value] stringValue];
             
             NSLog(@"phoneNumber = %@", phoneNumber);
                                  [contactsArray addObject:contact];
                 
             NSLog(@"givenName = %@", contact.givenName);
             NSLog(@"familyName = %@", contact.familyName);
             NSLog(@"email = %@", contact.emailAddresses);
             
             
            
             }
         }];
        
       
    }
    
    // Bluetooth
    

    
    
   
 pj_status_t registrationStatus =  [[PJ sharedPJ] startPjsipAndRegisterOnServer:[host UTF8String] withUserName:[username UTF8String] andPassword:[pass UTF8String]];
    if(registrationStatus != PJ_SUCCESS ){
        return;
    }
    else {
        NSString * newPhoneNumber =[_phoneNumber stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [_phoneNumber length])];
        NSLog(@"new phone number = %@",newPhoneNumber);
        NSString* callURI = [NSString stringWithFormat:@"sip:%@", [NSString stringWithFormat:@"%@%@",newPhoneNumber,@"@209.15.239.82"]];
        NSLog(@"call uri : %@",callURI);
        NSLog(@"acc is = %d",(int)pjsua_acc_get_count);
        [[PJ sharedPJ] makeCall:[callURI UTF8String]];
    }
//
    
 
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"TestNotification" object:nil];
    
    // Search Controller
    searchResults = [[NSMutableArray alloc]init];
    
    // Conference Call
    
    self.firstConfView.hidden = YES;
    self.secondConfView.hidden = YES;
    [[NSUserDefaults standardUserDefaults]setValue:@"Yes" forKey:@"isFirstCall"];
    [[NSUserDefaults standardUserDefaults]setValue:self.name.text forKey:@"calleename"];
    [[NSUserDefaults standardUserDefaults]setValue:self.mobileNumber.text forKey:@"calleeNumber"];
    self.conferenceTable.dataSource = self;
    self.conferenceTable.delegate = self;
    
}


- (void)disconnected{
    NSLog(@"disconnected");
     dispatch_async(dispatch_get_main_queue(), ^{
    [self.navigationController popViewControllerAnimated:YES];
     });
}

- (void)receiveTestNotification:(NSNotification *) notification {
    

    NSLog(@"notification : %@",notification.userInfo);
    NSDictionary* userInfo = notification.userInfo;
    NSLog(@"userinfo = %@",notification.userInfo);
    NSString * state = userInfo[@"total"];
    NSDictionary * dic = userInfo[@"dict"];
   
    NSLog(@"state = %@",state);
    NSString * max = userInfo[@"count"];
    NSLog(@"max = %@",max);
    int maxInt = (int)max;
    
    NSString * status = userInfo[@"dict"][@"stateText"];
    NSNumber *   port = userInfo[@"dict"][@"port"];
     NSLog(@"port  = %@",port);
    int portInt = [port intValue];
//    NSLog(@"port int = %d",portInt);
     NSInteger anIndex = 0;
    if([status isEqualToString:@"DISCONNCTD"]){
    if(portInt <= 0){
        
    }
   else{
        NSLog(@"before deleting call record array = %@",callRecordArray);
        for(int i = 0 ; i<callRecordArray.count ; i++){
           anIndex = [callRecordArray indexOfObjectPassingTest:^BOOL(NSDictionary *dict, NSUInteger idx, BOOL * _Nonnull stop) {
                return [[dict objectForKey:@"port"] isEqual:port];
            }];
        }
       NSLog(@"index = %ld",(long)anIndex);
       [callIdArray removeObjectAtIndex:anIndex];
       [callRecordArray removeObjectAtIndex:anIndex];
       [callLogArray removeObjectAtIndex:anIndex];
       NSLog(@"deleted callLogArray = %@",callLogArray);
       
       NSLog(@" after deleting call record array = %@",callRecordArray);
    }
    }
   
    dispatch_async(dispatch_get_main_queue(), ^{
        if([state isEqualToString:@"CONFIRMED"]){
            
            
            [self.timerlabel setHidden:NO];
            if([[[NSUserDefaults standardUserDefaults]valueForKey:@"isFirstCall"] isEqualToString:@"Yes"]){
            countUpTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                                          selector:@selector(countUpTimerFired:) userInfo:nil repeats:YES];
            }
            [self.statusLabell setHidden:YES];
            [callRecordArray addObject:dic];
            [callLogArray addObject:userInfo[@"dict"][@"calleeNumber"]];
            NSLog(@"added callLogArray = %@",callLogArray);
             NSLog(@"call record array = %@",callRecordArray);
        
        }
        if([state isEqualToString:@"DISCONNECTED"]){
            if([max isEqualToString:@"1"] || [max isEqualToString:@"0"]){
                [countUpTimer invalidate];
                [self.statusLabell setHidden:NO];
                self.statusLabell.text = @"Call Declined";
                [self.endCallButtton setHidden:YES];
                [self performSelector:@selector(goBack) withObject:self afterDelay:3.0];
            }
            else {
                
            }
           
        }
        
    
    });
    if([status isEqualToString:@"CALLING"] || [status isEqualToString:@"EARLY"] || [status isEqualToString:@"CONNECTING"]){
         dispatch_async(dispatch_get_main_queue(), ^{
        if(callRecordArray.count > 0 ){
            
          //pj_status_t holdstatus =  pjsua_call_set_hold(0, NULL);
         // NSLog(@"hold status = %d",holdstatus);
            
            
            
        self.firstConfView.hidden = NO;
        self.firstConfStatus.text = @"calling...";
        self.firstConfLabel.text = userInfo[@"dict"][@"calleeName"];
            self.mobileNumber.hidden = YES;
            self.timerlabel.hidden = YES;
        }
        if(callRecordArray.count == 1){
            self.secondConfView.hidden = NO;
            self.secondConfLabel.text = [[callRecordArray objectAtIndex:0]valueForKey:@"calleeName"];
            self.button_confInfoButton.hidden = YES;
            self.secondStatusLabel.hidden = NO;
            self.firstConfStatus.text = @"calling...";
//            self.firstConfView.hidden = NO;
//            self.firstConfLabel.text = @"calling...";
            
        }
        if(callRecordArray.count > 1){
            self.secondConfView.hidden = NO;
            self.secondConfLabel.text = @"Conference Call";
            self.button_confInfoButton.hidden = YES;
            self.secondStatusLabel.hidden = NO;
            self.firstConfStatus.text = @"calling...";
//            self.firstConfView.hidden = NO;
//            self.firstConfLabel.text = @"calling...";
        }
             [self.button_keypad setImage:[UIImage imageNamed:@"Dail pad-31.png"] forState:UIControlStateNormal];
             [self.button_addCall setImage:[UIImage imageNamed:@"Dail pad-32.png"] forState:UIControlStateNormal];
             [self.button_contacts setImage:[UIImage imageNamed:@"Dail pad-30.png"] forState:UIControlStateNormal];
             self.keyPadLabel.textColor = [UIColor darkGrayColor];
             self.contactsButton.textColor = [UIColor darkGrayColor];
             self.addCallButton.textColor = [UIColor darkGrayColor];
             });
        
    }
    else {
        if(callRecordArray.count > 0){
            dispatch_async(dispatch_get_main_queue(), ^{
                
//                pj_status_t REinvIstatus = pjsua_call_reinvite(0, PJ_TRUE, NULL);
//                NSLog(@"reinvite status = %d",REinvIstatus);
                
                
                [self.button_keypad setImage:[UIImage imageNamed:@"Dail pad-17.png"] forState:UIControlStateNormal];
                [self.button_addCall setImage:[UIImage imageNamed:@"Dail pad-19.png"] forState:UIControlStateNormal];
                [self.button_contacts setImage:[UIImage imageNamed:@"Dail pad-21.png"] forState:UIControlStateNormal];
                self.keyPadLabel.textColor = [UIColor blackColor];
                self.contactsButton.textColor = [UIColor blackColor];
                self.addCallButton.textColor = [UIColor blackColor];
                
                self.firstConfView.hidden = YES;
                self.secondConfView.hidden = YES;
                self.mobileNumber.hidden = NO;
                self.timerlabel.hidden = NO;
                
                if([status isEqualToString:@"DISCONNCTD"]){
                    self.firstConfStatus.text = @"declined";
                    self.firstConfView.hidden = YES;
                }
                if(callRecordArray.count == 1){
                    self.mobileNumber.hidden = NO;
                    self.timerlabel.hidden = NO;
                    self.name.text = [[callRecordArray objectAtIndex:0]valueForKey:@"calleeName"];
                    self.mobileNumber.text = [[callRecordArray objectAtIndex:0]valueForKey:@"calleeNumber"];
                }
                if(callRecordArray.count > 1){
                  //  self.name.text = @"Conference Call";
                    self.mobileNumber.hidden = YES;
                    self.timerlabel.hidden = NO;
                    self.secondConfView.hidden = NO;
                    self.secondConfLabel.text = @"Conference Call";
                   // self.secondStatusLabel
                    self.button_confInfoButton.hidden = NO;
                    self.secondStatusLabel.hidden = YES;
                }
            });
           
        }
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
    [self.conferenceTable reloadData];
        
    });
        
}

- (void)countUpTimerFired:(id)sender {
    [[NSUserDefaults standardUserDefaults]setValue:@"NO" forKey:@"isFirstCall"];
    timeInSeconds++;
    int hours = timeInSeconds/3600;
    int minutes = (timeInSeconds%3600)/60;
    int seconds = timeInSeconds%60;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.timerlabel setText:[NSString stringWithFormat:@"%d:%02d:%02d", hours, minutes, seconds]];
    });
}

-(void)goBack{
    
       // pjsua_destroy();
        [self setAudioOutputSpeaker:false];
        dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
        });
}

- (void)viewWillAppear:(BOOL)animated
{
   
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
    
    
}

-(void)viewDidAppear:(BOOL)animated{
     [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    
    [super viewWillDisappear:animated];
//    pjmedia_port_destroy(ringbackPort);
  //  pj_pool_release(pjPool);
   
}


- (void)loginComplete:(BOOL)success{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (IBAction)action_back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)action_endCall:(id)sender {
    
   // pjsua_call_hangup(mycallID, 0, NULL, NULL);
     [self setAudioOutputSpeaker:false];
     pjsua_call_hangup_all();
    
    
//    pj_status_t status = pjsua_call_hangup(mycallID, 0, NULL, NULL);
//    NSLog(@"call end status = %d",status);

}

- (IBAction)action_loadSpeaker:(id)sender {
    

    if([[self.button_speaker1 currentImage] isEqual:[UIImage imageNamed:@"Dail pad-26-1.png"]] ){
        NSLog(@"self.button_speaker");
         [self.button_speaker1 setImage:[UIImage imageNamed:@"Dail pad-18-1.png"] forState:UIControlStateNormal];
         [self setAudioOutputSpeaker:true];
    }
   else if([[self.button_speaker1 currentImage] isEqual:[UIImage imageNamed:@"Dail pad-18-1.png"]] ){
        NSLog(@"self.button_speaker");
        [self.button_speaker1 setImage:[UIImage imageNamed:@"Dail pad-26-1.png"] forState:UIControlStateNormal];
        [self setAudioOutputSpeaker:false];
    }
//
}

- (void)setAudioOutputSpeaker:(BOOL)enabled
{
    AVAudioSession *session =   [AVAudioSession sharedInstance];
    NSError *error;
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    [session setMode:AVAudioSessionModeVoiceChat error:&error];
    if (enabled) // Enable speaker
    {
        [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
    }
    else // Disable speaker
    {
        [session overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:&error];
    }
    [session setActive:YES error:&error];
}
- (IBAction)action_muteMyAudio:(id)sender {
    if([[self.muteButton currentImage] isEqual:[UIImage imageNamed:@"Dail pad-16.png"]] ){
        NSLog(@"self.button_mute");
        [self.muteButton setImage:[UIImage imageNamed:@"Dail pad-24.png"] forState:UIControlStateNormal];
       int mute =  [self mutethecall];
        
       // [self setInputGain:0.0];
        
    
    }
    else if([[self.muteButton currentImage] isEqual:[UIImage imageNamed:@"Dail pad-24.png"]] ){
        NSLog(@"self.button_mute");
        [self.muteButton setImage:[UIImage imageNamed:@"Dail pad-16.png"] forState:UIControlStateNormal];
        int mute =  [self unmutethecall];
       //  [self setInputGain:1.0];
    }
}

-(int) mutethecall
{
    pj_status_t status =   pjsua_conf_adjust_rx_level (0,0);
    status = pjsua_conf_adjust_tx_level (0,1);
    return (PJ_SUCCESS == status);
}
-(int) unmutethecall
{
    pj_status_t status =   pjsua_conf_adjust_rx_level (0,1);
    status = pjsua_conf_adjust_tx_level (0,1);
    return (PJ_SUCCESS == status);
}

+ (void)setInputGain:(CGFloat)gain
{
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *error = nil;
    BOOL success = [audioSession setInputGain:gain error:&error];
    if (!success) {
        NSLog(@" error ::::: %@", error);
    }

}

#pragma mark Dialing Pad Actions
- (IBAction)action_keyPad:(UIButton *)sender {
    if([[self.button_keypad currentImage] isEqual:[UIImage imageNamed:@"Dail pad-17.png"]]){
    NSString * mytext = self.keypadText.text;
    if(sender.tag == 1){
        self.keypadText.text = [mytext stringByAppendingString:@"1"];
        [self sendDTMF:@"1"];
    }
    if(sender.tag == 2){
        self.keypadText.text = [mytext stringByAppendingString:@"2"];
        [self sendDTMF:@"2"];
    }
    if(sender.tag == 3){
        self.keypadText.text = [mytext stringByAppendingString:@"3"];
        [self sendDTMF:@"3"];
    }
    if(sender.tag == 4){
        self.keypadText.text = [mytext stringByAppendingString:@"4"];
        [self sendDTMF:@"4"];
    }
    if(sender.tag == 5){
        self.keypadText.text = [mytext stringByAppendingString:@"5"];
        [self sendDTMF:@"5"];
    }
    if(sender.tag == 6){
        self.keypadText.text = [mytext stringByAppendingString:@"6"];
        [self sendDTMF:@"6"];
    }
    if(sender.tag == 7){
        self.keypadText.text = [mytext stringByAppendingString:@"7"];
        [self sendDTMF:@"7"];
    }
    if(sender.tag == 8){
        self.keypadText.text = [mytext stringByAppendingString:@"8"];
        [self sendDTMF:@"8"];
    }
    if(sender.tag == 9){
        self.keypadText.text = [mytext stringByAppendingString:@"9"];
        [self sendDTMF:@"9"];
    }
    if(sender.tag == 10){
        self.keypadText.text = [mytext stringByAppendingString:@"*"];
        [self sendDTMF:@"*"];
    }
    if(sender.tag == 12){
        self.keypadText.text = [mytext stringByAppendingString:@"#"];
        [self sendDTMF:@"#"];
    }
    }
    else {}
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return NO;
}

-(void)normalTap : (UITapGestureRecognizer*)tap {
    NSString * keyText = self.keypadText.text;
    self.keypadText.text = [keyText stringByAppendingString:@"0"];
}

-(void)longTap : (UILongPressGestureRecognizer*)longTapped {
    NSString * keyText = self.keypadText.text;
    if(longTapped.state == UIGestureRecognizerStateBegan){
        self.keypadText.text = [keyText stringByAppendingString:@"+"];
    }
}
- (IBAction)action_hideKeypad:(id)sender {
    [self.dialingPadView setHidden:YES];
    [self.hideButton setHidden: YES];
    
}

- (IBAction)action_showKeypad:(id)sender {
    if(callRecordArray.count > 0){
    [self.dialingPadView setHidden:NO];
    [self.hideButton setHidden: NO];
    }
}

-(void)sendDTMF:(NSString*)number{
    pj_status_t status;
    pj_str_t pjKeyPressed  = pj_str([number UTF8String]);
   // if(mycallID != nil){
    status = pjsua_call_dial_dtmf(mycallID, &pjKeyPressed);
    if(status != PJ_SUCCESS) error_exit1("Error in init", status);
   // }
}

#pragma mark Add Call Actions

- (IBAction)doneButton:(id)sender {
    if (self.searchController.isActive) {
        dispatch_async(dispatch_get_main_queue(), ^(void){
            self.searchController.active = NO;
             _searchController.dimsBackgroundDuringPresentation = NO;
        });
    }
    
    [self.addCallView removeFromSuperview];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == self.addCallTableView){
  //  if (self.searchController.isActive && self.searchController.searchBar.text.length > 0) {
     if (self.searchController.isActive && (![self.searchController.searchBar.text isEqualToString:@""])) {
        NSLog(@"count = %lu",(unsigned long)searchResults.count);
        return searchResults.count;
    }
//    if([self isFiltering]){
//        return searchResults.count;
//    }
    else {
        return contactsArray.count;
    }
    }
    else if(tableView == self.conferenceTable){
        return callRecordArray.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    if(tableView ==  self.addCallTableView){
    AddCallCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (self.searchController.isActive && (![self.searchController.searchBar.text isEqualToString:@""])) {
  //  if([self isFiltering]){
        NSString * first =  [searchResults[indexPath.row] givenName];
        NSLog(@"first = %@",first);
        NSString * second =  [searchResults[indexPath.row] familyName];
        NSLog(@"second = %@",second);
        cell.name.text = [[first stringByAppendingString:@" "]stringByAppendingString:second];
        CNContact * contact = searchResults[indexPath.row];
        if([contact isKeyAvailable:CNContactPhoneNumbersKey]){
            cell.phoneNumber.text = [[[contact.phoneNumbers firstObject] value] stringValue];
        }
    }
    else {
    NSString * first =  [contactsArray[indexPath.row] givenName];
    NSString * second =  [contactsArray[indexPath.row] familyName];
    cell.name.text = [[first stringByAppendingString:@" "]stringByAppendingString:second];
    CNContact * contact = contactsArray[indexPath.row];
    if([contact isKeyAvailable:CNContactPhoneNumbersKey]){
        cell.phoneNumber.text = [[[contact.phoneNumbers firstObject] value] stringValue];
    }
    }
        return cell;
    }
    else {
        ConferenceCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.fdefde.text = [[callRecordArray objectAtIndex:indexPath.row]valueForKey:@"calleeName"];
        cell.calleeNumber.text = [[callRecordArray objectAtIndex:indexPath.row]valueForKey:@"calleeNumber"];
        [cell.buttton_removeConf addTarget:self action:@selector(removeFromConfCall:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(callRecordArray.count == 4){
        
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Maximum limit of conference call has been exceeded."  delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    else {
    if(tableView == self.addCallTableView){
    if (self.searchController.isActive && (![self.searchController.searchBar.text isEqualToString:@""])) {
        CNContact * contact = searchResults[indexPath.row];
        if([contact isKeyAvailable:CNContactPhoneNumbersKey]){
            
            NSString * phoen = [[[contact.phoneNumbers firstObject] value] stringValue];
            if([callLogArray containsObject: phoen]){
                NSString * first =  [searchResults[indexPath.row] givenName];
                NSString * second =  [searchResults[indexPath.row] familyName];
                calleeName = [[first stringByAppendingString:@" "]stringByAppendingString:second];

                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@ already in call",calleeName]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
            else {
            NSString * first =  [searchResults[indexPath.row] givenName];
            NSString * second =  [searchResults[indexPath.row] familyName];
            calleeName = [[first stringByAppendingString:@" "]stringByAppendingString:second];
             [[NSUserDefaults standardUserDefaults]setValue:calleeName forKey:@"calleename"];
            NSString * newPhoneNumber =[phoen stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [phoen length])];
            [[NSUserDefaults standardUserDefaults]setValue:newPhoneNumber forKey:@"calleeNumber"];
            NSLog(@"new phone number = %@",newPhoneNumber);
            NSString* callURI = [NSString stringWithFormat:@"sip:%@", [NSString stringWithFormat:@"%@%@",newPhoneNumber,@"@209.15.239.82"]];
            NSLog(@"call uri : %@",callURI);
            NSLog(@"acc is = %d",(int)pjsua_acc_get_count);
           [[PJ sharedPJ] makeCall:[callURI UTF8String]];
          //  if (self.searchController.isActive) {
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    self.searchController.active = NO;
                });
          //  }
            [searchResults removeAllObjects];
            [searchResults addObjectsFromArray:contactsArray];
            [self.addCallView removeFromSuperview];
            }
    }
    }
    else{
    CNContact * contact = contactsArray[indexPath.row];
    if([contact isKeyAvailable:CNContactPhoneNumbersKey]){
      NSString * phoen = [[[contact.phoneNumbers firstObject] value] stringValue];
        if([callLogArray containsObject: phoen]){
            NSString * first =  [contactsArray[indexPath.row] givenName];
            NSString * second =  [contactsArray[indexPath.row] familyName];
            calleeName = [[first stringByAppendingString:@" "]stringByAppendingString:second];

            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@ already in call",calleeName]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        else {
        NSString * first =  [contactsArray[indexPath.row] givenName];
        NSString * second =  [contactsArray[indexPath.row] familyName];
        calleeName = [[first stringByAppendingString:@" "]stringByAppendingString:second];
        [[NSUserDefaults standardUserDefaults]setValue:calleeName forKey:@"calleename"];
    NSString * newPhoneNumber =[phoen stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [phoen length])];
        [[NSUserDefaults standardUserDefaults]setValue:newPhoneNumber forKey:@"calleeNumber"];
    NSLog(@"new phone number = %@",newPhoneNumber);
    NSString* callURI = [NSString stringWithFormat:@"sip:%@", [NSString stringWithFormat:@"%@%@",newPhoneNumber,@"@209.15.239.82"]];
    NSLog(@"call uri : %@",callURI);
    NSLog(@"acc is = %d",(int)pjsua_acc_get_count);
    [[PJ sharedPJ] makeCall:[callURI UTF8String]];
    self.searchController.active = NO;
    [self.addCallView removeFromSuperview];
        }
    }
    }
        [searchResults removeAllObjects];
        [searchResults addObjectsFromArray:contactsArray];
        _searchController.dimsBackgroundDuringPresentation = NO;
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.addCallTableView){
    return 69.0;
    }
    else return 60;
}



-(void)removeFromConfCall :(id)sender {
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.conferenceTable];
    NSIndexPath *indexPath = [self.conferenceTable indexPathForRowAtPoint:buttonPosition];
    NSDictionary  * dic = [callRecordArray objectAtIndex:indexPath.row];
    NSNumber *   port = [dic valueForKey:@"port"];
    NSLog(@"port  = %@",port);
    int portInt = [port intValue];
   // pjsua_conf_port_id slotOne = pjsua_call_get_conf_port(portInt);
    
    pjsua_conf_disconnect(0, portInt);
    pjsua_conf_disconnect(portInt, 0);
    for(int i = 0; i < callList.count ; i++){
         pjsua_conf_port_id slotTwo = pjsua_call_get_conf_port(i);
        if(slotTwo < 0){
            
        }
        else {
            if(portInt == slotTwo){
                
            }
            else {
       
        pjsua_conf_disconnect(slotTwo, portInt);
        pjsua_conf_disconnect(portInt, slotTwo);
      
}
        }
    }
        pjsua_call_id callId = [callIdArray[indexPath.row] intValue];
    
        pjsua_call_hangup(callId, 0, NULL, NULL);
            NSLog(@"before callIdArray = %@",callIdArray);
    
            NSLog(@"after callIdArray = %@",callIdArray);
   
}

# pragma mark Add Contacts

- (IBAction)action_addCall:(id)sender {
    if([[self.button_addCall currentImage] isEqual:[UIImage imageNamed:@"Dail pad-19.png"]]){
    self.addCallView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
 
    _searchController = [[UISearchController alloc]initWithSearchResultsController:nil];
    _searchController.searchResultsUpdater = self;
    _searchController.dimsBackgroundDuringPresentation = NO;
    _searchController.searchBar.delegate = self;
    [_searchController.searchBar sizeToFit];
 
    if (!_searchController.searchBar.superview) {
        self.addCallTableView.tableHeaderView = self.searchController.searchBar;
    }
//    if (!self.searchController.active && self.searchController.searchBar.text.length == 0) {
//        self.addCallTableView.contentOffset = CGPointMake(0, CGRectGetHeight(self.searchController.searchBar.frame));
//    }
     [self.addCallTableView reloadData];
     [self.view addSubview:self.addCallView];
    }
    else {}
  
}

#pragma mark Contacts
- (IBAction)action_openContacts:(id)sender {
    if([[self.button_contacts currentImage] isEqual:[UIImage imageNamed:@"Dail pad-21.png"]]){
    self.addCallView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    _searchController = [[UISearchController alloc]initWithSearchResultsController:nil];
    _searchController.searchResultsUpdater = self;
    _searchController.dimsBackgroundDuringPresentation = NO;
    _searchController.searchBar.delegate = self;
    [_searchController.searchBar sizeToFit];
    
    if (!_searchController.searchBar.superview) {
        self.addCallTableView.tableHeaderView = self.searchController.searchBar;
    }
//    if (!self.searchController.active && self.searchController.searchBar.text.length == 0) {
//        self.addCallTableView.contentOffset = CGPointMake(0, CGRectGetHeight(self.searchController.searchBar.frame));
//    }
    [self.addCallTableView reloadData];
    [self.view addSubview:self.addCallView];
    }
    else {}
}



#pragma mark - UISearchControllerDelegate

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
 //   [self filterContentForSearchText:searchController.searchBar.text scope:@"All"];
  //  [self.addCallTableView reloadData];
}

-(void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope{
   // [self filterContentForSearchText:searchBar.text scope:@"All"];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSLog(@"searchBar");
    [searchResults removeAllObjects];
    if (searchText.length > 0) {
        for (int i = 0; i < contactsArray.count ; i++)
        {
            NSString * phoneNumber = @"";
            CNContact * contact = [contactsArray objectAtIndex:i];
            NSString * name = contact.givenName;
            NSString * familyName = contact.familyName;
            if([contact isKeyAvailable:CNContactPhoneNumbersKey]){
                phoneNumber = [[[contact.phoneNumbers firstObject] value] stringValue];
            }
            NSRange titleResultsRange = [name rangeOfString:searchText options:NSCaseInsensitiveSearch];
            NSRange titleResultsRange1 = [familyName rangeOfString:searchText options:NSCaseInsensitiveSearch];
            NSRange titleResultsRange2 = [phoneNumber rangeOfString:searchText options:NSCaseInsensitiveSearch];
     //       NSLog(@"self.searchResults = %@",searchResults);
            if (titleResultsRange.length > 0 || titleResultsRange1.length > 0  || titleResultsRange2.length > 0)
            {
                [searchResults addObject:[contactsArray objectAtIndex:i]];
            }
            // [self.addCallTableView reloadData];
        }
        
    }
    else {
        [searchResults addObjectsFromArray:contactsArray];
    }
    [self.addCallTableView reloadData];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchResults removeAllObjects];
    [searchResults addObjectsFromArray:contactsArray]; 
    [self.addCallTableView reloadData];
}

-(void)filterContentForSearchText : (NSString*)searchText scope:(NSString*)scope{
   // NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@",searchText];

  //  filteredArray = [contactsArray filteredArrayUsingPredicate:resultPredicate];
    [searchResults removeAllObjects];
    if (searchText.length > 0) {
        for (int i = 0; i < contactsArray.count ; i++)
        {
            NSString * phoneNumber = @"";
            CNContact * contact = [contactsArray objectAtIndex:i];
            NSString * name = contact.givenName;
            NSString * familyName = contact.familyName;
            if([contact isKeyAvailable:CNContactPhoneNumbersKey]){
                phoneNumber = [[[contact.phoneNumbers firstObject] value] stringValue];
            }
            
//            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[c] %@", searchText];
//            NSLog(@"predicate = %@",predicate);
            NSRange titleResultsRange = [name rangeOfString:searchText options:NSCaseInsensitiveSearch];
            NSRange titleResultsRange1 = [familyName rangeOfString:searchText options:NSCaseInsensitiveSearch];
            NSRange titleResultsRange2 = [phoneNumber rangeOfString:searchText options:NSCaseInsensitiveSearch];
        
         //   NSLog(@"self.searchResults = %@",searchResults);
            if (titleResultsRange.length > 0 || titleResultsRange1.length > 0  || titleResultsRange2.length > 0)
            {
                [searchResults addObject:[contactsArray objectAtIndex:i]];
            }
          // [self.addCallTableView reloadData];
        }
       [self.addCallTableView reloadData];
    }
}

-(BOOL)isFiltering{

    int searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0;
    return searchController.isActive && (![self searchBarIsEmpty] || searchBarScopeIsFiltering);
}

-(BOOL)searchBarIsEmpty{
    return searchController.searchBar.text.length > 0 ;
}




#pragma mark Bluetooth
- (IBAction)action_setBluetooth:(id)sender {
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionAllowBluetooth error:nil];
    if([[self.bluetoothButton currentImage] isEqual:[UIImage imageNamed:@"Dail pad-25-1.png"]] ){
        [self.bluetoothButton setImage:[UIImage imageNamed:@"Dail pad-20-1.png"] forState:UIControlStateNormal];
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        
    }
    else if([[self.bluetoothButton
              currentImage] isEqual:[UIImage imageNamed:@"Dail pad-20-1.png"]] ){
       [self.bluetoothButton setImage:[UIImage imageNamed:@"Dail pad-25-1.png"] forState:UIControlStateNormal];
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:NO error:nil];
        
    }
}
- (IBAction)button_confInfo:(id)sender {
    self.conferenceView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:self.conferenceView];
    [self.conferenceTable reloadData];
}
- (IBAction)action_deleteConferenceUI:(id)sender {
    [self.conferenceView removeFromSuperview];
}





#pragma mark - play ringtone

- (void)playStartRingTone{
  
}
@end





#pragma mark static functions implementation

static void on_incoming_call(pjsua_acc_id accountID, pjsua_call_id callID, pjsip_rx_data *rdata)
{
    pjsua_call_info inf;
    
    PJ_UNUSED_ARG(accountID);
    PJ_UNUSED_ARG(rdata);
    
    pjsua_call_get_info(callID, &inf);
    PJ_LOG(3, ("pj.c", "Incoming call from %.*s", (int)inf.remote_info.slen, inf.remote_info.ptr));
    
    pjsua_call_answer(callID, 200, NULL, NULL);
    
}


#define THIS_FILE    "pjsua_vid.c"
static void on_call_state(pjsua_call_id callID, pjsip_event *event)
{
    mycallID = callID;
    NSLog(@"mycallid = %d",mycallID);
    pjsua_call_info ci;
    pj_status_t status = pjsua_call_get_info(callID, &ci);
    NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
    if (status == PJ_SUCCESS) {
        pj_str_t localinfo = ci.local_info;
        pj_str_t laststatustext = ci.last_status_text;
        pj_str_t remoteStatus = ci.remote_info;
        pj_str_t stateText = ci.state_text;
        pjsip_status_code laststatus = ci.last_status;
        pjsua_conf_port_id port = ci.conf_slot;
        pj_time_val totalDuration = ci.total_duration;
        pj_time_val connectDuration = ci.connect_duration;
        printf("port =  %d \n",port);
        printf("local info = %s \n",localinfo);
        printf("laststatus = %u \n",laststatus);
        printf("remote status = %s \n",remoteStatus);
        printf("last status text = %s \n",laststatustext);
        printf("status code = %s \n",stateText);
        NSString * myString = [pjstring stringWithPJString:localinfo];
        NSString * myStringlaststatustext = [pjstring stringWithPJString:laststatustext];
        NSString * myStringstateText = [pjstring stringWithPJString:stateText];
    
        [dic setValue:myString forKey:@"localinfo"];
        [dic setObject:[NSNumber numberWithInt:port] forKey:@"port"];
        [dic setObject:myStringlaststatustext forKey:@"laststatustext"];
        [dic setObject:myStringstateText forKey:@"stateText"];
        [dic setValue:[[NSUserDefaults standardUserDefaults]valueForKey:@"calleename"] forKey:@"calleeName"];
        [dic setValue:[[NSUserDefaults standardUserDefaults]valueForKey:@"calleeNumber"] forKey:@"calleeNumber"];

        if([myStringstateText isEqualToString:@"DISCONNCTD"]){
            if(port <= 0 ){
                
            }
            else {
                [callList removeObject:[NSNumber numberWithInt:port]];
                NSLog(@"calling this state");
            }
         
            NSLog(@"call List array = %@",callList);
        }
        
    }
   // PJ_UNUSED_ARG(e);
    int max=pjsua_call_get_count();
    NSLog(@"max = %d",max);
    printf("max  =  %d", max);
    pjsua_call_get_info(callID, &ci);
    PJ_LOG(3,(THIS_FILE, "******* ***** Call %d state=%.*s", callID,
              (int)ci.state_text.slen,
              ci.state_text.ptr));
    NSString * state;
    if(ci.state == PJSIP_INV_STATE_DISCONNECTED){
        NSLog(@"callId array = %@",callIdArray);
        for (int i = 0; i < callIdArray.count ; i++){
            pjsua_call_id callId = [callIdArray[i] intValue];
            NSLog(@"callid d = %d",callId);
           // pjsua_conf_disconnect(pjsua_player_get_conf_port(player_id), callId);
            pjsua_call_reinvite(callId, PJ_TRUE, NULL);

        }
        
        for(int i = 0 ;i<callList.count;i++){
            pjsua_call_id callId = [callList[i] intValue];
            pjsua_conf_disconnect(pjsua_player_get_conf_port(player_id), callId);
        }
        state = @"DISCONNECTED";
        stopPlaying();
        ringbackCount = ringbackCount + 1;
        NSDictionary *dict = @{@"total": state , @"count" : [NSString stringWithFormat:@"%d",max] , @"dict" : dic};
        [[NSNotificationCenter defaultCenter]postNotificationName:@"TestNotification" object:nil userInfo:dict];

        }
    if(ci.state == PJSIP_INV_STATE_CALLING){
       state = @"CALLING";
        NSLog(@"callId array = %@",callIdArray);
//        for(int i = 0 ;i<callList.count;i++){
//            pjsua_call_id callId = [callList[i] intValue];
//            NSLog(@"wav callid i = %d",callId);
//            pj_status_t status =   pjsua_conf_connect(pjsua_player_get_conf_port(player_id), callId);
//            NSLog(@"wav playing = %d",status);
//        }

        
//        for (int i = 0; i < callIdArray.count ; i++){
//            pjsua_call_id callId = [callIdArray[i] intValue];
//            NSLog(@"callid i = %d",callId);
//            pjsua_call_set_hold(callId, NULL);
//        }
//
        
        

        NSDictionary *dict = @{@"total": state , @"count" : [NSString stringWithFormat:@"%d",max], @"dict" : dic};
        [[NSNotificationCenter defaultCenter]postNotificationName:@"TestNotification" object:nil userInfo:dict];
    }
    if(ci.state == PJSIP_INV_STATE_CONNECTING){
        state = @"CONNECTING";
        
        stopPlaying();
        ringbackCount = ringbackCount + 1;
         NSDictionary *dict = @{@"total": state , @"count" : [NSString stringWithFormat:@"%d",max], @"dict" : dic};
        [[NSNotificationCenter defaultCenter]postNotificationName:@"TestNotification" object:nil userInfo:dict];
    }
    if(ci.state == PJSIP_INV_STATE_NULL){
       state = @"NULL";
       
         NSDictionary *dict = @{@"total": state , @"count" : [NSString stringWithFormat:@"%d",max], @"dict" : dic};
        [[NSNotificationCenter defaultCenter]postNotificationName:@"TestNotification" object:nil userInfo:dict];
    }
    if(ci.state == PJSIP_INV_STATE_EARLY){
        state = @"EARLY";
       
        
        pj_str_t reason;
        pjsip_msg *msg;
        int code;
        
        pj_assert(event->type == PJSIP_EVENT_TSX_STATE);
        
        if (event->body.tsx_state.type == PJSIP_EVENT_RX_MSG) {
            msg = event->body.tsx_state.src.rdata->msg_info.msg;
        } else {
            msg = event->body.tsx_state.src.tdata->msg;
        }
        
        code = msg->line.status.code;
        reason = msg->line.status.reason;

        if (ci.role == PJSIP_ROLE_UAC && ci.media_status == PJSUA_CALL_MEDIA_NONE && code == 180 &&
            msg->body == NULL ) {
       
           startPlaying();
                    for(int i = 0 ;i<callList.count;i++){
                        pjsua_call_id callId = [callList[i] intValue];
                        NSLog(@"wav callid i = %d",callId);
                        pj_status_t status =   pjsua_conf_connect(pjsua_player_get_conf_port(player_id), callId);
                        NSLog(@"wav playing = %d",status);
                        }
    }
        
         NSDictionary *dict = @{@"total": state , @"count" : [NSString stringWithFormat:@"%d",max], @"dict" : dic};
        [[NSNotificationCenter defaultCenter]postNotificationName:@"TestNotification" object:nil userInfo:dict];
    }
    if(ci.state == PJSIP_INV_STATE_CONFIRMED){
        NSLog(@"callId array = %@",callIdArray);
        for (int i = 0; i < callIdArray.count ; i++){
            pjsua_call_id callId = [callIdArray[i] intValue];
            NSLog(@"callid d = %d",callId);
           // pjsua_conf_disconnect(pjsua_player_get_conf_port(player_id), callId);
            pjsua_call_reinvite(callId, PJ_TRUE, NULL);


        }
        
    
        for(int i = 0 ;i<callList.count;i++){
            pjsua_call_id callId = [callList[i] intValue];
            pjsua_conf_disconnect(pjsua_player_get_conf_port(player_id), callId);
        }
       state = @"CONFIRMED";
        [callIdArray addObject:[NSNumber numberWithInt:callID]];
        NSLog(@"callId = %d",callID);
        pj_status_t status = pjsua_call_get_info(callID, &ci);
        if (status == PJ_SUCCESS) {
        [callList addObject:[NSNumber numberWithInt:ci.conf_slot]];
        NSLog(@"call List array = %@",callList);
        }
         NSDictionary *dict = @{@"total": state , @"count" : [NSString stringWithFormat:@"%d",max], @"dict" : dic};
         [[NSNotificationCenter defaultCenter]postNotificationName:@"TestNotification" object:nil userInfo:dict];
    }
 
    NSLog(@"max = %d",max);
}



static void on_reg_state2(pjsua_acc_id accountID, pjsua_reg_info *info){
  [[PJ sharedPJ] handleRegistrationStateChangeWithRegInfo: info];
    
}

struct call_data
{
    pj_pool_t           *pool;
    pjmedia_conf        *conf;
    pjmedia_port        *cport;
    pjmedia_port        *null;
    pjmedia_master_port *m;
    int                  call_slot;
};


static void on_call_media_state(pjsua_call_id callID){
    
   // [callIdArray addObject:[NSNumber numberWithInt:callID]];
    NSLog(@"callId = %d",callID);
    NSLog(@"call List array = %@",callList);
    NSLog(@"call list count = %lu",(unsigned long)callList.count);
    
    BOOL mergeCalls = false;
    pjsua_call_info info;
    printf("port1 info = %d",info.conf_slot);
    pjmedia_conf_port_info conf_info;
    printf("port info = %d",conf_info.slot);
    mycallID = callID;
    //pjsua_call_get_info(callID, &info);
    
    pj_status_t status = pjsua_call_get_info(callID, &info);
    if (status == PJ_SUCCESS) {
         printf("port11 info = %d",info.conf_slot);
        pj_str_t localinfo = info.local_info;
        pj_str_t laststatus = info.last_status_text;
        pj_str_t remoteStatus = info.remote_info;
        printf("local info = %s",localinfo);
        printf("laststatus = %s",laststatus);
        printf("remote status = %s",remoteStatus);
    }
    if(info.media_status == PJSUA_CALL_MEDIA_ACTIVE){
         printf("port12 info = %d",info.conf_slot);
        pjsua_conf_connect(info.conf_slot, 0);
        pjsua_conf_connect(0, info.conf_slot);
        
    
    }
    
    
  //  callList = [[NSMutableArray alloc]init];
//    [callList addObject:[NSNumber numberWithInt:info.conf_slot]];
//    NSLog(@"call List array = %@",callList);
    
    if (info.media_status == PJSUA_CALL_MEDIA_ACTIVE) { //    When media is active, connect call to sound device.
        pjsua_conf_port_id slotOne = info.conf_slot;
      
        
        int max=pjsua_call_get_count();
        NSLog(@"max = %d",max);
        if (max >= 2) {
            mergeCalls=true;
        }
        
        if (mergeCalls == true) {
           //  [callList addObject:[NSNumber numberWithInt:info.conf_slot]];
            NSLog(@"call List array = %@",callList);
            NSLog(@"call list count = %lu",(unsigned long)callList.count);
            for(int i = 0; i< callList.count ; i++){
              pjsua_conf_port_id slotTwo = pjsua_call_get_conf_port(i);
              //  int r = callList[i];
              //  pjsua_conf_port_id slotTwo = [callList[i] intValue];
                if(slotTwo < 0){}
                else {
                if(slotOne == slotTwo){
                    
                }
                else {
            pj_status_t status = pjsua_conf_connect(slotOne, slotTwo);
            pj_status_t status1 = pjsua_conf_connect(slotTwo, slotOne);
                    NSLog(@"status = %d",status);
                    NSLog(@"status = %d",status1);
                    NSLog(@"call List array = %@",callList);
                    NSLog(@"call list count = %lu",(unsigned long)callList.count);
                }
            
                }
            }
            
            // since the "activeCallID" is already  talking, its conf_port is already connected to "0" (and vice versa) ...
            
        } else {
          //  accountID = call_id;
        }
    } else if (info.media_status == PJSUA_CALL_MEDIA_LOCAL_HOLD) {
        // … callSuspended(callID);
    }
    
    
    
}
static void error_exit1(const char *msg, pj_status_t stat){

   
    NSLog([NSString stringWithUTF8String:msg]);
    pjsua_perror("pj.c", msg, stat);
    
    pjsua_destroy();
    exit(1);
}

static pj_bool_t input(const char *title, char *buf, pj_size_t len)
 {
        char *p;
   
      printf("%s (empty to cancel): ", title); fflush(stdout);
     if (fgets(buf, len, stdin) == NULL)
                return PJ_FALSE;
   
        /* Remove trailing newlines. */
       for (p=buf; ; ++p) {
           if (*p=='\r' || *p=='\n') *p='\0';
             else if (!*p) break;
}
    
         if (!*buf)
              return PJ_FALSE;
   
     return PJ_TRUE;
    }

struct pjsua_player_eof_data
{
    pj_pool_t          *pool;
    pjsua_player_id player_id;
};

static PJ_DEF(pj_status_t) on_pjsua_wav_file_end_callback(pjmedia_port* media_port, void* args)
{
    pj_status_t status;
    
    struct pjsua_player_eof_data *eof_data = (struct pjsua_player_eof_data *)args;
    
    status = pjsua_player_destroy(eof_data->player_id);
    
    PJ_LOG(3,(THIS_FILE, "End of Wav File, media_port: %d", media_port));
    
    if (status == PJ_SUCCESS)
    {
        return -1;// Here it is important to return a value other than PJ_SUCCESS
        //Check link below
    }
    
    return PJ_SUCCESS;
}

static void play_sound_during_call(pj_str_t sound_file)
{
   // pjsua_player_id player_id;
    pj_status_t status;
    status = pjsua_player_create(&sound_file, 0, &player_id);
    status = pjsua_player_create(&sound_file, 0, &player_id1);
  
    if (status != PJ_SUCCESS){
       //  return status;
    }
    
    
    pjmedia_port *player_media_port , * player_media_port1;
    pjsua_conf_port_id  wavPort , wavPort1;
    
    status = pjsua_player_get_port(player_id, &player_media_port);
    status = pjsua_player_get_port(player_id1, &player_media_port1);
    if (status != PJ_SUCCESS)
    {
      //  return status;
    }
    
    pj_pool_t *pool = pjsua_pool_create("my_eof_data1", 512, 512);
    struct pjsua_player_eof_data *eof_data = PJ_POOL_ZALLOC_T(pool, struct pjsua_player_eof_data);
    eof_data->pool = pool;
    eof_data->player_id = player_id;
    pjsua_conf_add_port(pool, player_media_port, &wavPort);
//     pj_pool_t *pool1 = pjsua_pool_create("my_eof_data2", 512, 512);
//    pjsua_conf_add_port(pool1, player_media_port1, &wavPort1);
    
  //  pjmedia_wav_player_set_eof_cb(player_media_port, eof_data, &on_pjsua_wav_file_end_callback);
    
 //   status = pjsua_conf_connect(pjsua_player_get_conf_port(player_id), 0);
    
//    if (status != PJ_SUCCESS)
//    {
//        return status;
//    }
//
//    return status;
}


enum {
    kAKRingbackFrequency1  = 440,
    kAKRingbackFrequency2  = 480,
    kAKRingbackOnDuration  = 2000,
    kAKRingbackOffDuration = 4000,
    kAKRingbackCount       = 1,
    kAKRingbackInterval    = 2000
};


pj_status_t play_sound_during_call1(pj_str_t sound_file){
    unsigned i, samplesPerFrame;
    pjmedia_tone_desc tone[kAKRingbackCount];
    pj_str_t name;
    pjsua_media_config mediaConfig;
    pj_status_t status;
    samplesPerFrame = mediaConfig.audio_frame_ptime *
    mediaConfig.clock_rate *
    mediaConfig.channel_count / 1000;
    
 
    name = pj_str("ringback");
    pjmedia_port *aRingbackPort;
    pjmedia_port *soundPort ;
  
     pjPool = pjsua_pool_create("my_eof_data", 512, 512);

    status = pjmedia_tonegen_create2(pjPool, &name,
                                     8000,
                                    1,
                                     160, 16, PJMEDIA_TONEGEN_LOOP,
                                     &aRingbackPort);
    if (status != PJ_SUCCESS) {
        NSLog(@"Error creating ringback tones");
        
        return 0;
    }
    

    ringbackPort = aRingbackPort;
    
    pj_bzero(&tone, sizeof(tone));
    for (i = 0; i < kAKRingbackCount; ++i) {
        tone[i].freq1 = kAKRingbackFrequency1;
        tone[i].freq2 = kAKRingbackFrequency2;
        tone[i].on_msec = kAKRingbackOnDuration;
        tone[i].off_msec = kAKRingbackOffDuration;
    }
    tone[kAKRingbackCount - 1].off_msec = kAKRingbackInterval;
    
   pj_status_t ringStatus1 =  pjmedia_tonegen_play(ringbackPort, kAKRingbackCount, tone, PJMEDIA_TONEGEN_LOOP);
   NSLog(@"ringStatus1 = %d",ringStatus1);
   
    
    
    pjsua_conf_port_id aRingbackSlot;
    status = pjsua_conf_add_port(pjPool, ringbackPort, &aRingbackSlot);
    NSLog(@"status add port = %d",status);
    ringbackSlot = (NSInteger)aRingbackSlot;
    
    ringbackCount = ringbackCount + 1;
  
    NSLog(@"status conf connect = %d",status);
    if (status != PJ_SUCCESS) {
        NSLog(@"Error adding media port for ringback tones");
        
        return 0 ;
    }
    
    
    
   
    return status;
}

static void startPlaying(){
    NSLog(@"start playing is called");
    NSLog(@"ringback count = %ld",(long)ringbackCount);
    if(ringbackCount == 1){
       pj_status_t status = pjsua_conf_connect(ringbackSlot, 0);
        NSLog(@"status conf connect = %d",status);
    }
}

static void stopPlaying(){
    
    NSLog(@"stop playing is called");
    ringbackCount = ringbackCount - 1 ;
    NSLog(@"ringback count = %ld",(long)ringbackCount);
    if(ringbackCount == 0){
    pjsua_conf_disconnect(ringbackSlot, 0);
   // pjsua_conf_remove_port(ringbackSlot);
    pjmedia_tonegen_rewind(ringbackPort);
    }

}

static void playWavFile(){
    
    int dev_count;
    pjmedia_aud_dev_index dev_idx;
    pj_status_t status;
    dev_count = pjmedia_aud_dev_count();
    printf("Got %d audio devices\n", dev_count);
    for (dev_idx=0; dev_idx<dev_count; ++dev_idx) {
        pjmedia_aud_dev_info info;
        status = pjmedia_aud_dev_get_info(dev_idx, &info);
        printf("%d. %s (in=%d, out=%d)\n",
               dev_idx, info.name,
               info.input_count, info.output_count);
    }
    pjmedia_port *soundPort ;
    pool = pjsua_pool_create("wav", 512, 512);
    
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"iphone_6_original" ofType:@"wav"];
  //  pj_str_t soundFile = pj_str([filePath UTF8String]);
    pj_status_t waveFileStatus = pjmedia_wav_player_port_create(pool, [filePath UTF8String], 20, 0, 0, &soundPort);
    
    printf("wav file status = %d",waveFileStatus);
    NSLog(@"wave file status = %d",waveFileStatus);
    if (waveFileStatus != PJ_SUCCESS) {
        NSLog(@"Unable to use WAV file");
       
    }
    pj_status_t portstatus = pjmedia_snd_port_create_player(pool,-1,PJMEDIA_PIA_SRATE(&soundPort->info), PJMEDIA_PIA_CCNT(&soundPort->info), PJMEDIA_PIA_SPF(&soundPort->info),PJMEDIA_PIA_BITS(&soundPort->info),0,&snd_port);
    NSLog(@"portstatus = %d",portstatus);
    if (portstatus != PJ_SUCCESS) {
        NSLog(@"Unable to open sound device");
       
    }
    
    pj_status_t soundStatus = pjmedia_snd_port_connect(snd_port, soundPort);
    NSLog(@"soundStatus = %d",soundStatus);
    if (soundStatus != PJ_SUCCESS) {
        NSLog(@"Unable to open sound device");
       
    }
}

static void pauseWavFile(){
    
}
/*
static void playWavFile(){
    pj_caching_pool cp;
    pjmedia_endpt *med_endpt;
    pj_pool_t *pool;
    pjmedia_port *file_port;
    pjmedia_snd_port *snd_port;
    char tmp[10];
    pj_status_t status;

    if (argc != 2) {
    puts("Error: filename required");
         puts(desc);
         return 1;
}

      /* Must init PJLIB first:
      status = pj_init();
      PJ_ASSERT_RETURN(status == PJ_SUCCESS, 1);

      /* Must create a pool factory before we can allocate any memory.
      pj_caching_pool_init(&cp, &pj_pool_factory_default_policy, 0);
    
      /*
          111  * Initialize media endpoint.
          112  * This will implicitly initialize PJMEDIA too.
          113
      status = pjmedia_endpt_create(&cp.factory, NULL, 1, &med_endpt);
      PJ_ASSERT_RETURN(status == PJ_SUCCESS, 1);
    
      /* Create memory pool for our file player
      pool = pj_pool_create( &cp.factory, /* pool factory
                                 "wav", /* pool name.
                                 4000, /* init size
                                 4000, /* increment size
                                 NULL /* callback on error
                                 );
    
      /* Create file media port from the WAV file
      status = pjmedia_wav_player_port_create( pool, /* memory pool
                                                 127  argv[1], /* file to play
                                                 128  20, /* ptime.
                                                 129  0, /* flags
                                                 130  0, /* default buffer
                                                 131  &file_port/* returned port
                                                 132  );
      if (status != PJ_SUCCESS) {
          app_perror(THIS_FILE, "Unable to use WAV file", status);
          return 1;
          }
    
      /* Create sound player port.
      status = pjmedia_snd_port_create_player(
                                                   pool, /* pool
                                                   -1, /* use default dev.
                                                   PJMEDIA_PIA_SRATE(&file_port->info),/* clock rate.
                                                   PJMEDIA_PIA_CCNT(&file_port->info),/* # of channels.
                                                   PJMEDIA_PIA_SPF(&file_port->info), /* samples per frame.
                                                   PJMEDIA_PIA_BITS(&file_port->info),/* bits per sample.
                                                   0, /* options
                                                   &snd_port /* returned port
                                                   );
      if (status != PJ_SUCCESS) {
          app_perror(THIS_FILE, "Unable to open sound device", status);
          return 1;
          }
    
      /* Connect file port to the sound player.
          155  * Stream playing will commence immediately.
          156
      status = pjmedia_snd_port_connect( snd_port, file_port);
      PJ_ASSERT_RETURN(status == PJ_SUCCESS, 1);
    
      /*
          163  * File should be playing and looping now, using sound device's thread.
          164  */
    
    
      /* Sleep to allow log messages to flush
      pj_thread_sleep(100);
    
      printf("Playing %s..\n", argv[1]);
      puts("");
      puts("Press <ENTER> to stop playing and quit");
    
      if (fgets(tmp, sizeof(tmp), stdin) == NULL) {
        puts("EOF while reading stdin, will quit now..");
          }
    
      /* Start deinitialization: */
    
      /* Disconnect sound port from file port
      status = pjmedia_snd_port_disconnect(snd_port);
      PJ_ASSERT_RETURN(status == PJ_SUCCESS, 1);
    
      /* Without this sleep, Windows/DirectSound will repeteadly
          187  * play the last frame during destroy.
          188
      pj_thread_sleep(100);
    
      /* Destroy sound device
      status = pjmedia_snd_port_destroy( snd_port );
      PJ_ASSERT_RETURN(status == PJ_SUCCESS, 1);
    
    
      /* Destroy file port
      status = pjmedia_port_destroy( file_port );
      PJ_ASSERT_RETURN(status == PJ_SUCCESS, 1);
    
    
      /* Release application pool
      pj_pool_release( pool );
    
      /* Destroy media endpoint.
      pjmedia_endpt_destroy( med_endpt );
    
      /* Destroy pool factory
      pj_caching_pool_destroy( &cp );
    
      /* Shutdown PJLIB
      pj_shutdown();
    
    
      /* Done.
      //return 0;
}

*/


