//
//  CallScreenViewController.h
//  appl
//
//  Created by Reuf Kicin on 10/31/16.
//  Copyright © 2016 Kicin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "pjstring.h"
#import "AddCallCell.h"
#import "pjstring.h"
#import "ConferenceCell.h"

@interface PJ : NSObject{
    NSMutableArray * callArray;
}

typedef void (^RegisterCallBack)(BOOL success);

+ (PJ *)sharedPJ;

//- (int)startPjsipAndRegisterOnServer:(char *) domain
//                        withUserName:(char *) username andPassword:(char *) pass callback:(RegisterCallBack) callback;
- (int)startPjsipAndRegisterOnServer:(char *) domain
                        withUserName:(char *) username andPassword:(char *) pass;
-(int)startPjsipAndRegisterOnServer:(char *) domain                         withUserName:(char *) username andPassword1:(char *) pass;

-(void)makeCall:(char*)uri;

-(void)endCall;
-(void)disconnected;
@end


@interface CallScreenViewController : UIViewController<UITextFieldDelegate , UITableViewDelegate , UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate , UISearchDisplayDelegate, UISearchControllerDelegate>
{
    NSTimer *countUpTimer;
    int timeInSeconds;
    NSMutableArray * contactsArray , * callArray, *searchResults , * callRecordArray  , * callLogArray;
    UISearchController *  searchController;
    NSArray * filteredArray ;
    NSMutableDictionary * callRecordDic ;
    NSString * calleeName;
    
}
-(void)disconnected;
@property (weak, nonatomic) IBOutlet UILabel *CalleeNumber;
@property (weak, nonatomic) IBOutlet UILabel *CalleeName;
@property (weak, nonatomic) IBOutlet UILabel *Secs;
@property (weak, nonatomic) IBOutlet UILabel *Mins;
@property(strong ,nonatomic) NSString * phoneNumber;
@property(strong ,nonatomic) NSString * calleeName1;
@property (strong, nonatomic) UINavigationController *navController;
- (IBAction)action_back:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *mobileNumber;
@property (weak, nonatomic) IBOutlet UILabel *timerlabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabell;
@property (weak, nonatomic) IBOutlet UIButton *endCallButtton;
- (IBAction)action_endCall:(id)sender;
- (IBAction)action_loadSpeaker:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *button_speaker;
@property (weak, nonatomic) IBOutlet UIButton *button_speaker1;
@property (weak, nonatomic) IBOutlet UIButton *muteButton;
- (IBAction)action_muteMyAudio:(id)sender;
+ (void)setInputGain:(CGFloat)gain;
// Dialing Pad

@property (weak, nonatomic) IBOutlet UIView *dialingPadView;
@property (weak, nonatomic) IBOutlet UITextField *keypadText;
- (IBAction)action_keyPad:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *tappedButtton;
@property (weak, nonatomic) IBOutlet UIButton *hideButton;
- (IBAction)action_hideKeypad:(id)sender;
- (IBAction)action_showKeypad:(id)sender;

// Add Call
@property (strong, nonatomic) IBOutlet UIView *addCallView;
- (IBAction)doneButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *addCallTableView;
- (IBAction)action_addCall:(id)sender;

// Contacts
- (IBAction)action_openContacts:(id)sender;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) UISearchController *searchController;
@property (weak, nonatomic) IBOutlet UIView *searchView;
//@property (nonatomic, strong) NSArray *searchResults;
// Bluetooth
@property (weak, nonatomic) IBOutlet UIButton *bluetoothButton;
- (IBAction)action_setBluetooth:(id)sender;

// Conference Call
@property (weak, nonatomic) IBOutlet UIView *firstConfView;
@property (weak, nonatomic) IBOutlet UIView *secondConfView;
@property (weak, nonatomic) IBOutlet UILabel *secondConfLabel;
@property (weak, nonatomic) IBOutlet UILabel *firstConfLabel;
@property (weak, nonatomic) IBOutlet UILabel *firstConfStatus;
@property (weak, nonatomic) IBOutlet UIButton *button_keypad;
@property (weak, nonatomic) IBOutlet UILabel *keyPadLabel;
@property (weak, nonatomic) IBOutlet UIButton *button_contacts;
@property (weak, nonatomic) IBOutlet UILabel *contactsButton;
@property (weak, nonatomic) IBOutlet UIButton *button_addCall;
@property (weak, nonatomic) IBOutlet UILabel *addCallButton;
@property (weak, nonatomic) IBOutlet UILabel *secondStatusLabel;
- (IBAction)button_confInfo:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *button_confInfoButton;

// Conference UI
@property (strong, nonatomic) IBOutlet UIView *conferenceView;
- (IBAction)action_deleteConferenceUI:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *conferenceTable;


// Play ringtone




@end
