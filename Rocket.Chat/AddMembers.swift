//
//  AddMembers.swift
//  Rocket.Chat
//
//  Created by arpit makhija on 22/01/18.
//  Copyright © 2018 Rocket.Chat. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift
import SwiftyJSON

// swiftlint:disable Trailing Whitespace
// swiftlint:disable trailing_whitespace
// swiftlint:disable colon
// swiftlint:disable control_statement
// swiftlint:disable vertical_whitespace
// swiftlint:disable opening_brace
// swiftlint:disable statement_position
// swiftlint:disable force_unwrapping


class AddMembers: UIViewController , UITableViewDelegate , UITableViewDataSource ,UITextFieldDelegate{

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textsearch1: UITextField!
     var subscription: Subscription?
    @IBOutlet weak var textSearch: UITextField!
    var searchText: String = ""
    var isSearchingLocally = false
    var isSearchingRemotely = false
    var searchResult: [Subscription]?
    var subscriptions: Results<Subscription>?
    var groupInfomation: [[String: String]]?
    var groupSubscriptions: [[Subscription]]?
    var nameArray : NSMutableArray = []
    var activityView : UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      //  text_search.delegate = self
        textSearch.delegate = self
        
        activityView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityView.center = self.view.center
       
        
        self.view.addSubview(activityView)

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        searchText = (currentText as NSString).replacingCharacters(in: range, with: string)
        
        if string == "\n" {
            if currentText.characters.count > 0 {
                searchOnSpotlight(currentText)
            }
            
            return false
        }
        
        searchBy(searchText)
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.nameArray.removeAllObjects()
        searchBy()
        return true
    }
    
    func searchOnSpotlight(_ text: String = "") {
        
        self.nameArray.removeAllObjects()
        SubscriptionManager.spotlight(text) { [weak self] result in
            let currentText = self?.textSearch.text ?? ""
            
            if currentText.characters.count == 0 {
                return
            }
            
            
            self?.isSearchingRemotely = true
            self?.searchResult = result
            print("serachResult = \(self?.searchResult)")
            self?.groupSubscription()
            if((self?.groupSubscriptions?.count)! > 0)
            {
                for var i in 0..<(self?.groupSubscriptions?.count)!
                {
                    let directSubNSDictionary : [Subscription] = self!.groupSubscriptions![i]
                    //  print("directSubNSDictionary count = \(directSubNSDictionary.count)")
                    
                    
                    for var i in 0..<directSubNSDictionary.count
                    {
                        let name : String = ((directSubNSDictionary[i] as Subscription).value(forKey: "name") as? String)!
                        let privateType : String = ((directSubNSDictionary[i] as Subscription).value(forKey: "privateType") as? String)!
                        if(privateType == "d")
                        {
                            self?.nameArray.add(name)
                        }
                        print(self?.nameArray)
                        
                    }
                }
            }
              self?.tableView.reloadData()
            
        }
    }
    
    func searchBy(_ text: String = "") {
        self.nameArray.removeAllObjects()
        guard let auth = AuthManager.isAuthenticated() else { return }
        subscriptions = auth.subscriptions.filterBy(name: text)
        
        if text.characters.count == 0 {
            isSearchingLocally = false
            isSearchingRemotely = false
            searchResult = []
            
            groupSubscription()
            if((groupSubscriptions?.count)! > 0)
            {
                for var i in 0..<(groupSubscriptions?.count)!
                {
                    let directSubNSDictionary : [Subscription] = groupSubscriptions![i]
                    //  print("directSubNSDictionary count = \(directSubNSDictionary.count)")
                    
                    
                    for var i in 0..<directSubNSDictionary.count
                    {
                        let name : String = ((directSubNSDictionary[i] as Subscription).value(forKey: "name") as? String)!
                        let privateType : String = ((directSubNSDictionary[i] as Subscription).value(forKey: "privateType") as? String)!
                        if(privateType == "d")
                        {
                            self.nameArray.add(name)
                        }
                        print(self.nameArray)
                        
                    }
                }
            }
               tableView.reloadData()
            //            tableView.tableFooterView = nil
            
            //   activityViewSearching.stopAnimating()
            
            return
        }
        
        if subscriptions?.count == 0 {
            searchOnSpotlight(text)
            return
        }
        
        isSearchingLocally = true
        isSearchingRemotely = false
        self.nameArray.removeAllObjects()
        groupSubscription()
        // print(groupSubscriptions)
        // print("groupSubscriptions?.count\(groupSubscriptions?.count)")
        if((groupSubscriptions?.count)! > 0)
        {
            for var i in 0..<(groupSubscriptions?.count)!
            {
                let directSubNSDictionary : [Subscription] = groupSubscriptions![i]
                //  print("directSubNSDictionary count = \(directSubNSDictionary.count)")
                
                
                for var i in 0..<directSubNSDictionary.count
                {
                    let name : String = ((directSubNSDictionary[i] as Subscription).value(forKey: "name") as? String)!
                    let privateType : String = ((directSubNSDictionary[i] as Subscription).value(forKey: "privateType") as? String)!
                    if(privateType == "d")
                    {
                        self.nameArray.add(name)
                    }
                    print(self.nameArray)
                    
                }
            }
        }
          tableView.reloadData()
        //        
        //        if let footerView = SubscriptionSearchMoreView.instantiateFromNib() {
        //            footerView.delegate = self
        //            tableView.tableFooterView = footerView
        //        }
    }
    
    func groupSubscription() {
        var unreadGroup: [Subscription] = []
        var favoriteGroup: [Subscription] = []
        var channelGroup: [Subscription] = []
        var directMessageGroup: [Subscription] = []
        var searchResultsGroup: [Subscription] = []
        
        guard let subscriptions = subscriptions else { return }
        let orderSubscriptions = isSearchingRemotely ? searchResult : Array(subscriptions.sorted(byKeyPath: "name", ascending: true))
        
        for subscription in orderSubscriptions ?? [] {
            if isSearchingRemotely {
                searchResultsGroup.append(subscription)
            }
            
            if !isSearchingLocally && !subscription.open {
                continue
            }
            
            if subscription.alert {
                unreadGroup.append(subscription)
                continue
            }
            
            if subscription.favorite {
                favoriteGroup.append(subscription)
                continue
            }
            
            switch subscription.type {
            case .channel, .group:
                channelGroup.append(subscription)
            case .directMessage:
                directMessageGroup.append(subscription)
            }
        }
        
        groupInfomation = [[String: String]]()
        groupSubscriptions = [[Subscription]]()
        
        if searchResultsGroup.count > 0 {
            groupInfomation?.append([
                "name": String(format: "%@ (%d)", localized("subscriptions.search_results"), searchResultsGroup.count)
                ])
            
            searchResultsGroup = searchResultsGroup.sorted {
                return $0.displayName() < $1.displayName()
            }
            
            groupSubscriptions?.append(searchResultsGroup)
        } else {
            if unreadGroup.count > 0 {
                groupInfomation?.append([
                    "name": String(format: "%@ (%d)", localized("subscriptions.unreads"), unreadGroup.count)
                    ])
                
                unreadGroup = unreadGroup.sorted {
                    return $0.type.rawValue < $1.type.rawValue
                }
                
                groupSubscriptions?.append(unreadGroup)
            }
            
            if favoriteGroup.count > 0 {
                groupInfomation?.append([
                    "icon": "Star",
                    "name": String(format: "%@ (%d)", localized("subscriptions.favorites"), favoriteGroup.count)
                    ])
                
                favoriteGroup = favoriteGroup.sorted {
                    return $0.type.rawValue < $1.type.rawValue
                }
                
                groupSubscriptions?.append(favoriteGroup)
            }
            
            if channelGroup.count > 0 {
                groupInfomation?.append([
                    "name": String(format: "%@ (%d)", localized("subscriptions.channels"), channelGroup.count)
                    ])
                
                groupSubscriptions?.append(channelGroup)
            }
            
            if directMessageGroup.count > 0 {
                groupInfomation?.append([
                    "name": String(format: "%@ (%d)", localized("subscriptions.direct_messages"), directMessageGroup.count)
                    ])
                
                groupSubscriptions?.append(directMessageGroup)
            }
        }
    }
    
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.nameArray.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell : AddMembersCell = (tableView.dequeueReusableCell(withIdentifier: "Cell") as? AddMembersCell)!
            cell.name.text = self.nameArray.object(at: indexPath.row) as? String
          //  cell.selectionStyle = .none
            return cell
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let name : String = (self.nameArray.object(at: indexPath.row) as? String)!
        print(name)
        self.view.endEditing(true)
         activityView.startAnimating()
        addMember(name: name)
    }
    
    func addMember(name : String)
    {
        
        let headers: HTTPHeaders = [
            "X-Auth-Token": API.shared.authToken!,
            "X-User-Id": API.shared.userId!
        ]
        let type : String = (self.subscription?.type.rawValue)!
        var finalURL : String = ""
        let urlGroup = "https://ctsichat.mvoipctsi.com/api/v1/groups.invite"
        let urlChannel = "https://ctsichat.mvoipctsi.com/api/v1/channels.invite"
        print(self.subscription?.name)
        if(type == "p"){
            finalURL = urlGroup
        }
        else if(type == "c"){
            finalURL = urlChannel
        }
        print(self.subscription?.displayName())
        print(name)
        
        Alamofire.request(finalURL ,method: .post, parameters: ["roomName" : (self.subscription?.name)! as AnyObject , "username" : name], encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            print("log")
            print(response.request as Any)  // original URL request
            print(response.response as Any) // URL response
            print(response.result.value as Any)
            
            if(response.result.isSuccess)
            {
                self.activityView.stopAnimating()
                let resultDic : NSDictionary = (response.result.value as? NSDictionary)!
                let success : Bool = (resultDic.value(forKey: "success") as? Bool)!
                if(success == false){
                    let alertcontroller = UIAlertController(title: "Error", message: (resultDic.value(forKey: "error") as? String)!, preferredStyle: .actionSheet)
                    let alertAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alertcontroller.removeFromParentViewController()
                    })
                    alertcontroller.addAction(alertAction)
                    self.present(alertcontroller, animated: true, completion: nil)
                    
                }
                else 
                {
//                    print("added successfully")
//                    let alertcontroller = UIAlertController(title: "", message: "Added", preferredStyle: .actionSheet)
//                    let alertAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
//                        alertcontroller.removeFromParentViewController()
//                    })
//                    alertcontroller.addAction(alertAction)
//                    self.present(alertcontroller, animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
                    
                }
                
            }
            if(response.result.isFailure)
            {
                self.activityView.stopAnimating()
                print("error")
            }
            
        }
    }

    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
