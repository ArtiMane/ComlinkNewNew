//
//  CreateGroupVC.swift
//  Rocket.Chat
//
//  Created by arpit makhija on 17/01/18.
//  Copyright © 2018 Rocket.Chat. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

// swiftlint:disable Trailing Whitespace
// swiftlint:disable trailing_whitespace
// swiftlint:disable colon
// swiftlint:disable control_statement
// swiftlint:disable vertical_whitespace
// swiftlint:disable opening_brace
// swiftlint:disable statement_position
// swiftlint:disable force_unwrapping


class CreateGroupVC: UIViewController , UITextFieldDelegate  {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textSerach: UITextField!
    var searchText: String = ""
    var isSearchingLocally = false
    var isSearchingRemotely = false
    var searchResult: [Subscription]?
    var subscriptions: Results<Subscription>?
    var groupInfomation: [[String: String]]?
    var groupSubscriptions: [[Subscription]]?
    var nameArray : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //self.textSerach.delegate = self
        
        
        
        CheckTableViewCell.registerCell(for: tableView)
        TextFieldTableViewCell.registerCell(for: tableView)
        MentionsTextFieldTableViewCell.registerCell(for: tableView)
        
        tableView.keyboardDismissMode = .interactive
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame(_:)), name: .UIKeyboardWillHide, object: nil)
        
        let user = CreateGroupVC.user
        let createPrivate = true
        let createPublic = true
        
        if !createPrivate && !createPublic {
//            alert(title: localized("alert.authorization_error.title"),
//                  message: localized("alert.authorization_error.create_channel.description")) { _ in
//                    self.dismiss(animated: true, completion: nil)
//            }
        }
 
       // self.createGroup()
    }
    
    
    @IBAction func button_closePressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func button_createPressed(_ sender: Any) {
        guard
            var roomName = setValues["room name"] as? String,
            let publicRoom = setValues["public room"] as? Bool,
            let membersRoom = setValues["users list"] as? [String],
            let readOnlyRoom = setValues["read only room"] as? Bool
            else {
                return
        }
        
        print(roomName)
        print(publicRoom)
        print(membersRoom)
        print(readOnlyRoom)
        
        let headers: HTTPHeaders = [
            "X-Auth-Token": API.shared.authToken!,
            "X-User-Id": API.shared.userId!,
            "Content-type" : "application/json"
        ]
        print("API.shared.authToken = \(API.shared.authToken)")
        print("API.shared.userId = \(API.shared.userId)")
        let myArray : [String] = ["rajesh","venjat","tushar","admin"]
        let parameters1 : [NSString : AnyObject] = ["name" : "QuicSovNew" as AnyObject , "members" : myArray as AnyObject]
        //        Alamofire.request("https://ctsichat.mvoipctsi.com/api/v1/groups.create", method: .post, parameters: parameters1, encoding: JSONEncoding.default, headers:headers)
        //            .responseJSON { response in
        
        let urlGroup = "https://ctsichat.mvoipctsi.com/api/v1/groups.create"
        let urlChannel = "https://ctsichat.mvoipctsi.com/api/v1/channels.create"
        
        let notificationGroup = "https://ctsichat.mvoipctsi.com/api/v1/groups.notifications"
        let notificationChannel = "https://ctsichat.mvoipctsi.com/api/v1/channels.notifications"
        var finalURL = ""
        var finalNotification = ""
        if(publicRoom == true)
        {
            finalURL = urlChannel
            finalNotification = notificationChannel
        }
        else 
        {
            finalURL = urlGroup
            finalNotification = notificationGroup
        }
        
        if(roomName.contains(" ")){
            roomName = roomName.replacingOccurrences(of: " ", with: "")
            print("roomname = \(roomName)")
        }
        /*
        Alamofire.request(notificationChannel ,method: .get, parameters: ["roomName" : "mytestchanne2" as String], encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            //            print(response.request as Any)
            //            print(response.error.debugDescription)
            //            print(response.response as Any) // URL response
            print(response.result.value as Any)
            if(response.result.isSuccess)
            {
                //                print("Response String: \(String(describing: response.result.value))")
                //                let responseString : String = response.result.value!
                //                print(responseString)
//                let myDic : [String:AnyObject] = self.convertStringToDictionary(text: (response.result.value)!)!
//                print("my Dic = \(myDic)")
            }
            if(response.result.isFailure)
            {
                print("error while creatig notification settings")
                print(response.result.error.debugDescription)
            }
        }
 */
        /*
        Alamofire.request(notificationChannel ,method: .post, parameters: ["roomName" : "mytestchanne2" as AnyObject , "notifications" : ["mobilePushNotifications": "all" as AnyObject] as AnyObject], encoding: JSONEncoding.default, headers: headers).responseString { response in
//            print(response.request as Any)
//            print(response.error.debugDescription)
//            print(response.response as Any) // URL response
          //  print(response.result.value as Any)
            if(response.result.isSuccess)
            {
//                print("Response String: \(String(describing: response.result.value))")
//                let responseString : String = response.result.value!
//                print(responseString)
                let myDic : [String:AnyObject] = self.convertStringToDictionary(text: (response.result.value)!)!
                print("my Dic = \(myDic)")
            }
            if(response.result.isFailure)
            {
                print("error while creatig notification settings")
                print(response.result.error.debugDescription)
            }
        }
        
        */
        
        
        Alamofire.request(finalURL ,method: .post, parameters: ["name" : roomName as AnyObject , "members" : membersRoom as AnyObject , "readOnly" : readOnlyRoom as AnyObject], encoding: JSONEncoding.default, headers: headers).responseJSON { response in
//            print("log")
//            print(response.request as Any)  // original URL request
//            print(response.response as Any) // URL response
//            print(response.result.value as Any)
            
            if(response.result.isSuccess)
            {
//                let dic : NSDictionary = (response.result.value as? NSDictionary)
//                print("dic  = \(dic)")
                
//                if(!(dic == nil)){
                
//                }
                
                self.dismiss(animated: true, completion: nil)
                print("grup created")
                guard let auth = AuthManager.isAuthenticated() else { return }
                
                SubscriptionManager.updateSubscriptions(auth) { _ in
                    if let newRoom = Realm.shared?.objects(Subscription.self).filter("name == '\(roomName)' && privateType != 'd'").first {
                        
                        let controller = ChatViewController.shared
                        controller?.subscription = newRoom
                        
                    } else {
                        
                    }
                }
                
            }
            if(response.result.isFailure)
            {
                print("error")
            }
            
        }
        
        
        
        
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do
            {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            }
            catch
            {
                print("error")
            }
        }
        return nil
    }
    
    func createGroup()
    {
        
        let headers: HTTPHeaders = [
            "X-Auth-Token": API.shared.authToken!,
            "X-User-Id": API.shared.userId!
        ]
        let myArray : [String] = ["rajesh","venjat","tushar","admin"]
        let parameters1 : [NSString : AnyObject] = ["name" : "QuicSovNew" as AnyObject , "members" : myArray as AnyObject]
//        Alamofire.request("https://ctsichat.mvoipctsi.com/api/v1/groups.create", method: .post, parameters: parameters1, encoding: JSONEncoding.default, headers:headers)
//            .responseJSON { response in
        
        Alamofire.request("https://ctsichat.mvoipctsi.com/api/v1/groups.create",method: .post, parameters: ["name" : "QuicSovNew" as AnyObject , "members" : myArray as AnyObject], encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            print("log")
            print(response.request as Any)  // original URL request
            print(response.response as Any) // URL response
            print(response.result.value as Any)
            
        }
            
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
         searchText = (currentText as NSString).replacingCharacters(in: range, with: string)
        
        if string == "\n" {
            if currentText.characters.count > 0 {
                searchOnSpotlight(currentText)
            }
            
            return false
        }
        
        searchBy(searchText)
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.nameArray.removeAllObjects()
        searchBy()
        return true
    }
    
    func searchOnSpotlight(_ text: String = "") {
        
        self.nameArray.removeAllObjects()
        SubscriptionManager.spotlight(text) { [weak self] result in
            let currentText = self?.textSerach.text ?? ""
            
            if currentText.characters.count == 0 {
                return
            }
            
            
            self?.isSearchingRemotely = true
            self?.searchResult = result
            print("serachResult = \(self?.searchResult)")
            self?.groupSubscription()
            if((self?.groupSubscriptions?.count)! > 0)
            {
                for var i in 0..<(self?.groupSubscriptions?.count)!
                {
                    let directSubNSDictionary : [Subscription] = self!.groupSubscriptions![i]
                    //  print("directSubNSDictionary count = \(directSubNSDictionary.count)")
                    
                    
                    for var i in 0..<directSubNSDictionary.count
                    {
                        let name : String = ((directSubNSDictionary[i] as Subscription).value(forKey: "name") as? String)!
                        let privateType : String = ((directSubNSDictionary[i] as Subscription).value(forKey: "privateType") as? String)!
                        if(privateType == "d")
                        {
                            self?.nameArray.add(name)
                        }
                        print(self?.nameArray)
                        
                    }
                }
            }
          //  self?.tableView.reloadData()
           
        }
    }
    
    func searchBy(_ text: String = "") {
        self.nameArray.removeAllObjects()
        guard let auth = AuthManager.isAuthenticated() else { return }
        subscriptions = auth.subscriptions.filterBy(name: text)
        
        if text.characters.count == 0 {
            isSearchingLocally = false
            isSearchingRemotely = false
            searchResult = []
            
            groupSubscription()
            if((groupSubscriptions?.count)! > 0)
            {
                for var i in 0..<(groupSubscriptions?.count)!
                {
                    let directSubNSDictionary : [Subscription] = groupSubscriptions![i]
                    //  print("directSubNSDictionary count = \(directSubNSDictionary.count)")
                    
                    
                    for var i in 0..<directSubNSDictionary.count
                    {
                        let name : String = ((directSubNSDictionary[i] as Subscription).value(forKey: "name") as? String)!
                        let privateType : String = ((directSubNSDictionary[i] as Subscription).value(forKey: "privateType") as? String)!
                        if(privateType == "d")
                        {
                            self.nameArray.add(name)
                        }
                        print(self.nameArray)
                        
                    }
                }
            }
         //   tableView.reloadData()
//            tableView.tableFooterView = nil
            
         //   activityViewSearching.stopAnimating()
            
            return
        }
        
        if subscriptions?.count == 0 {
            searchOnSpotlight(text)
            return
        }
        
        isSearchingLocally = true
        isSearchingRemotely = false
        self.nameArray.removeAllObjects()
        groupSubscription()
       // print(groupSubscriptions)
       // print("groupSubscriptions?.count\(groupSubscriptions?.count)")
        if((groupSubscriptions?.count)! > 0)
        {
            for var i in 0..<(groupSubscriptions?.count)!
            {
        let directSubNSDictionary : [Subscription] = groupSubscriptions![i]
      //  print("directSubNSDictionary count = \(directSubNSDictionary.count)")
        
            
        for var i in 0..<directSubNSDictionary.count
        {
            let name : String = ((directSubNSDictionary[i] as Subscription).value(forKey: "name") as? String)!
            let privateType : String = ((directSubNSDictionary[i] as Subscription).value(forKey: "privateType") as? String)!
            if(privateType == "d")
            {
            self.nameArray.add(name)
            }
            print(self.nameArray)
        
        }
            }
        }
     //   tableView.reloadData()
//        
//        if let footerView = SubscriptionSearchMoreView.instantiateFromNib() {
//            footerView.delegate = self
//            tableView.tableFooterView = footerView
//        }
    }
    
    func groupSubscription() {
        var unreadGroup: [Subscription] = []
        var favoriteGroup: [Subscription] = []
        var channelGroup: [Subscription] = []
        var directMessageGroup: [Subscription] = []
        var searchResultsGroup: [Subscription] = []
        
        guard let subscriptions = subscriptions else { return }
        let orderSubscriptions = isSearchingRemotely ? searchResult : Array(subscriptions.sorted(byKeyPath: "name", ascending: true))
        
        for subscription in orderSubscriptions ?? [] {
            if isSearchingRemotely {
                searchResultsGroup.append(subscription)
            }
            
            if !isSearchingLocally && !subscription.open {
                continue
            }
            
            if subscription.alert {
                unreadGroup.append(subscription)
                continue
            }
            
            if subscription.favorite {
                favoriteGroup.append(subscription)
                continue
            }
            
            switch subscription.type {
            case .channel, .group:
                channelGroup.append(subscription)
            case .directMessage:
                directMessageGroup.append(subscription)
            }
        }
        
        groupInfomation = [[String: String]]()
        groupSubscriptions = [[Subscription]]()
        
        if searchResultsGroup.count > 0 {
            groupInfomation?.append([
                "name": String(format: "%@ (%d)", localized("subscriptions.search_results"), searchResultsGroup.count)
                ])
            
            searchResultsGroup = searchResultsGroup.sorted {
                return $0.displayName() < $1.displayName()
            }
            
            groupSubscriptions?.append(searchResultsGroup)
        } else {
            if unreadGroup.count > 0 {
                groupInfomation?.append([
                    "name": String(format: "%@ (%d)", localized("subscriptions.unreads"), unreadGroup.count)
                    ])
                
                unreadGroup = unreadGroup.sorted {
                    return $0.type.rawValue < $1.type.rawValue
                }
                
                groupSubscriptions?.append(unreadGroup)
            }
            
            if favoriteGroup.count > 0 {
                groupInfomation?.append([
                    "icon": "Star",
                    "name": String(format: "%@ (%d)", localized("subscriptions.favorites"), favoriteGroup.count)
                    ])
                
                favoriteGroup = favoriteGroup.sorted {
                    return $0.type.rawValue < $1.type.rawValue
                }
                
                groupSubscriptions?.append(favoriteGroup)
            }
            
            if channelGroup.count > 0 {
                groupInfomation?.append([
                    "name": String(format: "%@ (%d)", localized("subscriptions.channels"), channelGroup.count)
                    ])
                
                groupSubscriptions?.append(channelGroup)
            }
            
            if directMessageGroup.count > 0 {
                groupInfomation?.append([
                    "name": String(format: "%@ (%d)", localized("subscriptions.direct_messages"), directMessageGroup.count)
                    ])
                
                groupSubscriptions?.append(directMessageGroup)
            }
        }
    }

//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.nameArray.count
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell : SearchCell = (tableView.dequeueReusableCell(withIdentifier: "Cell") as? SearchCell)!
//        cell.name2.text = self.nameArray.object(at: indexPath.row) as? String
//        return cell
//    }

    
    let tableViewData: [SectionForm] = [
        SectionForm(
            name: nil,
            footer: nil,
            cells: [
                createPublicChannelSwitch(
                    allowPublic: true,
                    allowPrivate: true
                ),
                FormCell(
                    cell: .check(title: localized("new_room.cell.read_only.title"), description: localized("new_room.cell.read_only.description")),
                    key: "read only room",
                    defaultValue: false,
                    enabled: true
                )
            ]
        ),
        SectionForm(
            name: localized("new_room.group.channel.name"),
            footer: localized("new_room.group.channel.footer"),
            cells: [
                FormCell(
                    cell: .textField(placeholder: localized("new_room.cell.channel_name.title"), icon: #imageLiteral(resourceName: "Hashtag")),
                    key: "room name",
                    defaultValue: [],
                    enabled: true
                )
            ]
        ),
        SectionForm(
            name: localized("new_room.group.invite_users"),
            footer: nil,
            cells: [
                FormCell(
                    cell: .mentionsTextField(placeholder: localized("new_room.cell.invite_users.placeholder"), icon: #imageLiteral(resourceName: "Mention")),
                    key: "users list",
                    defaultValue: [],
                    enabled: true
                )
            ]
        )
    ]

    static func createPublicChannelSwitch(allowPublic: Bool, allowPrivate: Bool) -> FormCell {
        var description: String = ""
        if allowPublic && allowPrivate {
            description = localized("new_room.cell.public_channel.description")
        } else if allowPublic {
            description = localized("new_room.cell.public_channel.description.public_only")
        } else if allowPrivate {
            description = localized("new_room.cell.public_channel.description.private_only")
        }
        
        return FormCell(
            cell: .check(title: localized("new_room.cell.public_channel.title"), description: description),
            key: "public room",
            defaultValue: allowPublic,
            enabled: allowPublic && allowPrivate
        )
    }
    
    var referenceOfCells: [String: FormTableViewCellProtocol] = [:]
    lazy var setValues: [String: Any] = {
        return tableViewData.reduce([String: Any]()) { (dict, entry) in
            var ndict = dict
            entry.cells.forEach { ndict[$0.key] = $0.defaultValue }
            return ndict
        }
    }()
    
    static var user: User? {
        return AuthManager.currentUser()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    fileprivate func showErrorAlert(_ errorMessage: String?) {
        let errorMessage = errorMessage ?? localized("error.socket.default_error_message")
        
        let alert = UIAlertController(
            title: localized("error.socket.default_error_title"),
            message: errorMessage,
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: localized("global.ok"), style: .default, handler: nil))
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: FormTableViewDelegate

extension CreateGroupVC: FormTableViewDelegate {
    func updateDictValue(key: String, value: Any) {
        setValues[key] = value
        
        if key == "public room",
            let value = value as? Bool,
            let cellRoomName = referenceOfCells["room name"] as? TextFieldTableViewCell {
            
            if value {
                cellRoomName.imgLeftIcon.image = #imageLiteral(resourceName: "Hashtag")
            } else {
                cellRoomName.imgLeftIcon.image = #imageLiteral(resourceName: "Lock")
            }
        }
    }
    
    func getPreviousValue(key: String) -> Any? {
        return setValues[key]
    }
    
    func updateTable(key: String) {
        tableView.beginUpdates()
        tableView.endUpdates()
        
        var section = 0
        for sectionForm in tableViewData {
            var row = 0
            for cell in sectionForm.cells {
                if cell.key == key {
                    tableView.scrollToRow(at: IndexPath(row: row, section: section), at: .none, animated: true)
                    return
                }
                row += 1
            }
            section += 1
        }
    }
}

// MARK: UITableViewDelegate

extension CreateGroupVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = tableViewData[indexPath.section].cells[indexPath.row]
        
        if let newChannelCell = data.cell.createCell(table: tableView, delegate: self, key: data.key, enabled: data.enabled),
            let newCell = newChannelCell as? UITableViewCell {
            newCell.textLabel?.font = UIFont.systemFont(ofSize: 20)
            referenceOfCells[data.key] = newChannelCell
            return newCell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = CGFloat(tableViewData[indexPath.section].cells[indexPath.row].cell.getClass().defaultHeight)
        
        if height < 0,
            let cell = tableView.cellForRow(at: indexPath) as? MentionsTextFieldTableViewCell {
            return cell.height()
        }
        
        return height
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return tableViewData[section].name
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return tableViewData[section].footer
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let data = tableViewData[indexPath.section].cells[indexPath.row]
        referenceOfCells.removeValue(forKey: data.key)
    }
}

// MARK: UITableViewDataSource

extension CreateGroupVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData[section].cells.count
    }
}

// MARK: Update cell position when the keyboard will show/hide

extension CreateGroupVC {
    func getTableViewInsets(keyboardHeight: CGFloat) -> UIEdgeInsets? {
        guard let window = UIApplication.shared.delegate?.window as? UIWindow else { return nil }
        guard let tableViewFrame = tableView.superview?.convert(tableView.frame, to: window) else { return nil }
        
        let bottomInset = keyboardHeight - (window.frame.height - tableViewFrame.height - tableViewFrame.origin.y)
        
        return UIEdgeInsets(
            top: tableView.contentInset.top,
            left: tableView.contentInset.left,
            bottom: bottomInset,
            right: tableView.contentInset.right
        )
    }
    
    @objc func keyboardWillChangeFrame(_ notification: Notification) {
        guard let info = notification.userInfo else { return }
        guard let animationDuration = info[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval else { return }
        guard let keyboardFrame = info[UIKeyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        let keyboardHeight = keyboardFrame.cgRectValue.height
        guard let contentInsets = getTableViewInsets(keyboardHeight: keyboardHeight) else { return }
        
        UIView.animate(
            withDuration: animationDuration,
            animations: {
                self.tableView.contentInset = contentInsets
                self.tableView.scrollIndicatorInsets = contentInsets
        }, completion: { _ -> Void in
            guard let contentInsets = self.getTableViewInsets(keyboardHeight: keyboardHeight) else { return }
            
            self.tableView.contentInset = contentInsets
            self.tableView.scrollIndicatorInsets = contentInsets
        }
        )
    }
}

