//
//  MentionsTextFieldTableViewCell.swift
//  Rocket.Chat
//
//  Created by Artur Rymarz on 07.11.2017.
//  Copyright © 2017 Rocket.Chat. All rights reserved.
//

import Foundation
import RealmSwift
import TagListView
import SearchTextField

// swiftlint:disable Trailing Whitespace
// swiftlint:disable trailing_whitespace
// swiftlint:disable colon
// swiftlint:disable control_statement
// swiftlint:disable vertical_whitespace
// swiftlint:disable opening_brace

// swiftlint:disable force_unwrapping



class MentionsTextFieldTableViewCell: UITableViewCell, FormTableViewCellProtocol, TagListViewDelegate , UITextFieldDelegate{
    static let identifier = "kMentionsTextFieldTableViewCell"
    static let xibFileName = "MentionsTextFieldTableViewCell"
    static let defaultHeight: Float = -1
    weak var delegate: FormTableViewDelegate?
    var key: String?
    var searchText: String = ""
    var isSearchingLocally = false
    var isSearchingRemotely = false
    var searchResult: [Subscription]?
    var subscriptions: Results<Subscription>?
    var groupInfomation: [[String: String]]?
    var groupSubscriptions: [[Subscription]]?
    var nameArray : NSMutableArray = []

    @IBOutlet private weak var tagListView: TagListView!
    @IBOutlet weak var imgLeftIcon: UIImageView!
    @IBOutlet weak var textFieldInput: SearchTextField!
    @IBOutlet weak var tagViewTopConstraint: NSLayoutConstraint!

    private var users: [String: TagView] = [:]

    override func awakeFromNib() {
        super.awakeFromNib()

        textFieldInput.clearButtonMode = .whileEditing
        tagListView.textFont = UIFont.systemFont(ofSize: 20)
        tagListView.delegate = self
        tagViewTopConstraint.constant = 0

        textFieldInput.itemSelectionHandler = { filteredResults, itemPosition in
            let item = filteredResults[itemPosition]
            self.addUserToInviteList(name: item.title)
            self.textFieldInput.text = nil
            self.textFieldInput.delegate = self
            self.delegate?.updateDictValue(key: self.key ?? "", value: self.invitedUsers())
        }
    }

    private func invitedUsers() -> [String] {
        return users.map { $0.key }
    }

    func height() -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
   

    func setPreviousValue(previous: Any) {
        if let previous = previous as? String {
            textFieldInput.text = previous
        }
    }

    @IBAction func textFieldInputEditingChanged(_ sender: Any) {
        fetchUsers()
    }

    private func fetchUsers() {
        searchText = textFieldInput.text!
        guard
            let realm = Realm.shared,
            let name = textFieldInput.text,
            name.count > 0
        else {
            textFieldInput.filterStrings([])
            return
        }
        
        
//            if name.characters.count > 0 {
//              let usernames =  searchOnSpotlight(name)
//                print("username1 = \(usernames)")
//        }

        
        let usernames = searchBy(searchText)
        print("usernames = \(usernames)")

//        let users = realm.objects(User.self).filter(
//            NSPredicate(format: "name CONTAINS %@ OR username CONTAINS %@", name, name)
//        )

//        let usernames = Array(users).flatMap { $0.username }.filter {
//            AuthManager.currentUser()?.username != $0
//        }
        
        print("usernames = \(usernames)")
        
      //  textFieldInput.filterStrings(usernames)
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        users.removeAll()
        tagListView.removeAllTags()
    }

    // MARK: Tag
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        for tag in users where tag.value === tagView {
            users.removeValue(forKey: tag.key)
        }

        tagListView.removeTagView(tagView)
        updateTagViewConstraint()
        delegate?.updateDictValue(key: key ?? "", value: self.invitedUsers())
        delegate?.updateTable(key: key ?? "")
    }

    private func updateTagViewConstraint() {
        tagViewTopConstraint.constant = tagListView.tagViews.count > 0 ? 12 : 0
    }

    private func addUserToInviteList(name: String) {
        if users[name] == nil {
            users[name] = tagListView.addTag(name)
            updateTagViewConstraint()
            delegate?.updateTable(key: key ?? "")
        }
    }
    
    func searchOnSpotlight(_ text: String = "") -> [String] {
        
        self.nameArray.removeAllObjects()
        SubscriptionManager.spotlight(text) { [weak self] result in
            let currentText = self?.textFieldInput.text ?? ""
            
            if currentText.characters.count == 0 {
                return
            }
            
            
            self?.isSearchingRemotely = true
            self?.searchResult = result
            print("serachResult = \(self?.searchResult)")
            self?.groupSubscription()
            if((self?.groupSubscriptions?.count)! > 0)
            {
                for var i in 0..<(self?.groupSubscriptions?.count)!
                {
                    let directSubNSDictionary : [Subscription] = self!.groupSubscriptions![i]
                    //  print("directSubNSDictionary count = \(directSubNSDictionary.count)")
                    
                    
                    for var i in 0..<directSubNSDictionary.count
                    {
                        let name : String = ((directSubNSDictionary[i] as Subscription).value(forKey: "name") as? String)!
                        let privateType : String = ((directSubNSDictionary[i] as Subscription).value(forKey: "privateType") as? String)!
                        if(privateType == "d")
                        {
                            self?.nameArray.add(name)
                        }
                        print(self?.nameArray)
                        
                    }
                }
            }
         //   self?.tableView.reloadData()
            self?.textFieldInput.filterStrings((self?.nameArray as? [String])!)
        }
        
        return (self.nameArray as? [String])!
    }
    
    func searchBy(_ text: String = "") -> [String] {
        self.nameArray.removeAllObjects()
         let auth = AuthManager.isAuthenticated() 
        subscriptions = auth?.subscriptions.filterBy(name: text)
        
        if text.characters.count == 0 {
            isSearchingLocally = false
            isSearchingRemotely = false
            searchResult = []
            
            groupSubscription()
            if((groupSubscriptions?.count)! > 0)
            {
                for var i in 0..<(groupSubscriptions?.count)!
                {
                    let directSubNSDictionary : [Subscription] = groupSubscriptions![i]
                    //  print("directSubNSDictionary count = \(directSubNSDictionary.count)")
                    
                    
                    for var i in 0..<directSubNSDictionary.count
                    {
                        let name : String = ((directSubNSDictionary[i] as Subscription).value(forKey: "name") as? String)!
                        let privateType : String = ((directSubNSDictionary[i] as Subscription).value(forKey: "privateType") as? String)!
                        if(privateType == "d")
                        {
                            self.nameArray.add(name)
                        }
                        print(self.nameArray)
                        
                    }
                }
            }
           
        }
        
        if subscriptions?.count == 0 {
            searchOnSpotlight(text)
            
        }
        
        isSearchingLocally = true
        isSearchingRemotely = false
        self.nameArray.removeAllObjects()
        groupSubscription()
        // print(groupSubscriptions)
        // print("groupSubscriptions?.count\(groupSubscriptions?.count)")
        if((groupSubscriptions?.count)! > 0)
        {
            for var i in 0..<(groupSubscriptions?.count)!
            {
                let directSubNSDictionary : [Subscription] = groupSubscriptions![i]
                //  print("directSubNSDictionary count = \(directSubNSDictionary.count)")
                
                
                for var i in 0..<directSubNSDictionary.count
                {
                    let name : String = ((directSubNSDictionary[i] as Subscription).value(forKey: "name") as? String)!
                    let privateType : String = ((directSubNSDictionary[i] as Subscription).value(forKey: "privateType") as? String)!
                    if(privateType == "d")
                    {
                        self.nameArray.add(name)
                    }
                    print("self.nameArray\(self.nameArray)")
                    
                }
            }
        }
      //  tableView.reloadData()
        textFieldInput.filterStrings((self.nameArray as? [String])!)
        return (self.nameArray as? [String])!
    }
    
    func groupSubscription() {
        var unreadGroup: [Subscription] = []
        var favoriteGroup: [Subscription] = []
        var channelGroup: [Subscription] = []
        var directMessageGroup: [Subscription] = []
        var searchResultsGroup: [Subscription] = []
        
        guard let subscriptions = subscriptions else { return }
        let orderSubscriptions = isSearchingRemotely ? searchResult : Array(subscriptions.sorted(byKeyPath: "name", ascending: true))
        
        for subscription in orderSubscriptions ?? [] {
            if isSearchingRemotely  {
                searchResultsGroup.append(subscription)
            }
            
            if !isSearchingLocally && !subscription.open {
                continue
            }
            
            if subscription.alert {
                unreadGroup.append(subscription)
                continue
            }
            
            if subscription.favorite {
                favoriteGroup.append(subscription)
                continue
            }
            
            switch subscription.type {
            case .channel, .group:
                channelGroup.append(subscription)
            case .directMessage:
                directMessageGroup.append(subscription)
            }
        }
        
        groupInfomation = [[String: String]]()
        groupSubscriptions = [[Subscription]]()
        
        if searchResultsGroup.count > 0 {
            groupInfomation?.append([
                "name": String(format: "%@ (%d)", localized("subscriptions.search_results"), searchResultsGroup.count)
                ])
            
            searchResultsGroup = searchResultsGroup.sorted {
                return $0.displayName() < $1.displayName()
            }
            
            groupSubscriptions?.append(searchResultsGroup)
        } else {
            if unreadGroup.count > 0 {
                groupInfomation?.append([
                    "name": String(format: "%@ (%d)", localized("subscriptions.unreads"), unreadGroup.count)
                    ])
                
                unreadGroup = unreadGroup.sorted {
                    return $0.type.rawValue < $1.type.rawValue
                }
                
                groupSubscriptions?.append(unreadGroup)
            }
            
            if favoriteGroup.count > 0 {
                groupInfomation?.append([
                    "icon": "Star",
                    "name": String(format: "%@ (%d)", localized("subscriptions.favorites"), favoriteGroup.count)
                    ])
                
                favoriteGroup = favoriteGroup.sorted {
                    return $0.type.rawValue < $1.type.rawValue
                }
                
                groupSubscriptions?.append(favoriteGroup)
            }
            
            if channelGroup.count > 0 {
                groupInfomation?.append([
                    "name": String(format: "%@ (%d)", localized("subscriptions.channels"), channelGroup.count)
                    ])
                
                groupSubscriptions?.append(channelGroup)
            }
            
            if directMessageGroup.count > 0 {
                groupInfomation?.append([
                    "name": String(format: "%@ (%d)", localized("subscriptions.direct_messages"), directMessageGroup.count)
                    ])
                
                groupSubscriptions?.append(directMessageGroup)
            }
        }
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        let currentText = textField.text ?? ""
//        searchText = (currentText as NSString).replacingCharacters(in: range, with: string)
//        
//        if string == "\n" {
//            if currentText.characters.count > 0 {
//                let usernames = searchOnSpotlight(currentText)
//                textFieldInput.filterStrings(usernames)
//            }
//            
//            return false
//        }
//        
//        let usernames = searchBy(searchText)
//        textFieldInput.filterStrings(usernames)
//        return true
//    }
//    
//    func textFieldShouldClear(_ textField: UITextField) -> Bool {
//        self.nameArray.removeAllObjects()
//        let usernames = searchBy()
//        textFieldInput.filterStrings(usernames)
//        return true
//    }

}


