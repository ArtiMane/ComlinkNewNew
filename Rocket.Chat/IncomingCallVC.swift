//
//  IncomingCallVC.swift
//  Rocket.Chat
//
//  Created by Riyaz Lakhani on 01/03/18.
//  Copyright © 2018 Rocket.Chat. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

class IncomingCallVC: UIViewController {

    var player: AVPlayer?
    @IBOutlet weak var namelabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let defaults = UserDefaults.standard
      self.namelabel.text =  (defaults.object(forKey:"bodyTitle") as? String)!
    }

    @IBAction func action_declineCall(_ sender: Any) {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "serverurl1")
        defaults.removeObject(forKey: "isTapped")
        defaults.removeObject(forKey: "bodyTitle")
        defaults.set("0", forKey: "isVideoGoing")
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func action_receiveCall(_ sender: Any) {
        let storyboardChat = UIStoryboard(name: "Chat", bundle: Bundle.main)
        let controller = storyboardChat.instantiateViewController(withIdentifier: "JitsiViewController")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.playSound()
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func playSound() {
        guard let url = Bundle.main.url(forResource: "tring_tring_tring", withExtension: "mp3") else { return }
        
        do {
            // AVAudioSessionCategoryPlayback
          //  try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)
             try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord, with: AVAudioSessionCategoryOptions.defaultToSpeaker)
            try AVAudioSession.sharedInstance().setActive(true)
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
           
            
            
//            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)
//            try AVAudioSession.sharedInstance().setActive(true)
//            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
            
          //  player = try AVAudioPlayer(contentsOf: url)
            let item = AVPlayerItem(url: url)
            player = AVPlayer(playerItem: item)
            guard let player = player else { return } 
            NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: item)
            (MPVolumeView().subviews.filter{NSStringFromClass($0.classForCoder) == "MPVolumeSlider"}.first as? UISlider)?.setValue(1.0, animated: false)
            player.play()
        }
        catch let error {
            print(error.localizedDescription)
        }
        
            
            //self.playSound()
            
        }

    
    @objc func playerDidFinishPlaying(note: NSNotification) {
        // Your code here
        self.playSound()
        
    }
    
   
    override func viewDidDisappear(_ animated: Bool) {
        player?.pause()
        NotificationCenter.default      .removeObserver(self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
