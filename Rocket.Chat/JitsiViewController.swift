//
//  JitsiViewController.swift
//  Rocket.Chat
//
//  Created by Riyaz Lakhani on 14/11/17.
//  Copyright © 2017 Rocket.Chat. All rights reserved.
//

import UIKit
import JitsiMeet

// swiftlint:disable Trailing Whitespace
// swiftlint:disable trailing_whitespace
// swiftlint:disable colon
// swiftlint:disable control_statement
// swiftlint:disable vertical_whitespace
// swiftlint:disable opening_brace
// swiftlint:disable statement_position
// swiftlint:disable force_unwrapping


class JitsiViewController: UIViewController , JitsiMeetViewDelegate {
    @IBOutlet weak var myJitsiMeetView: JitsiMeetView!
     var subscription: Subscription!
    @IBOutlet weak var myName1: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
       
        let defaults = UserDefaults.standard
        defaults.setValue("1", forKey: "isVideoGoing")
//        let serverUrlVideoVC = defaults.object(forKey:"serverurl")
//        print("ServerUrlVideoVC is : \(String(describing: serverUrlVideoVC))")
//        let objJitsiMeetView: JitsiMeetView  = (self.view as? JitsiMeetView)!
//        objJitsiMeetView.delegate = self as? JitsiMeetViewDelegate
//        objJitsiMeetView.welcomePageEnabled = false
//        objJitsiMeetView.loadURLString(serverUrlVideoVC as? String)
//        objJitsiMeetView.accessibilityActivate()
        //defaults.removeObject(forKey: "serverurl")
        if(UserDefaults.standard.value(forKey: "serverurl1") != nil)
        {
//            self.navigationItem.title = (UserDefaults.standard.value(forKey: "nameOfCaller") as? String)!
            if(UserDefaults.standard.value(forKey: "isTapped") != nil){
                
            let serverUrlVideoVC : String = (defaults.object(forKey:"serverurl1") as? String)!
                let calll : String = "org.jitsi.meet:" + serverUrlVideoVC
           // let objJitsiMeetView: JitsiMeetView  = (self.view as? JitsiMeetView)!
                    let objJitsiMeetView: JitsiMeetView  = self.myJitsiMeetView
            objJitsiMeetView.delegate = self as JitsiMeetViewDelegate
           // objJitsiMeetView.welcomePageEnabled = false
                self.myName1.text = (defaults.value(forKey: "bodyTitle") as? String)!
            DispatchQueue.main.async {
                objJitsiMeetView.loadURLString(calll as? String)
//                objJitsiMeetView.loadURLObject(["config": ["startWithAudioMuted": false, "startWithVideoMuted": true], "url": serverUrlVideoVC])
            }
       //    let title1 = (UserDefaults.standard.value(forKey: "nameOfCaller") as? String)!
//                let backButton = UIBarButtonItem(title: title1, style: .plain, target: nil, action: nil)
//                navigationItem.backBarButtonItem = backButton
            objJitsiMeetView.accessibilityActivate()
              
            defaults.removeObject(forKey: "serverurl1")
            defaults.removeObject(forKey: "isTapped")
            defaults.removeObject(forKey: "bodyTitle")
       //     defaults.removeObject(forKey: "nameOfCaller")  
                
            }
            
        }
        else 
        {
            let serverUrlVideoVC : String = (defaults.object(forKey:"serverurl") as? String)!
            print("ServerUrlVideoVC is : \(String(describing: serverUrlVideoVC))")
         //   let objJitsiMeetView: JitsiMeetView  = (self.view as? JitsiMeetView)!
            let objJitsiMeetView: JitsiMeetView  = self.myJitsiMeetView
            objJitsiMeetView.delegate = self as JitsiMeetViewDelegate
        
            self.myName1.text = (defaults.value(forKey: "title") as? String)!
           // objJitsiMeetView.welcomePageEnabled = false
            DispatchQueue.main.async {
              
                objJitsiMeetView.loadURLString(serverUrlVideoVC as? String)
//                objJitsiMeetView.loadURLObject(["config": ["startWithAudioMuted": false, "startWithVideoMuted": true], "url": serverUrlVideoVC])
            }
            
            objJitsiMeetView.accessibilityActivate()
        }
    }
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK: JitsiMeetViewDelegate Methods
    func onJitsiMeetViewDelegateEvent(name: String, data: [AnyHashable: Any]) {
        print("[\(#file):\(#line)] JitsiMeetViewDelegate \(name) \(data)")
        print("event")
    }
    func conferenceFailed(_ data: [AnyHashable: Any]) {
        onJitsiMeetViewDelegateEvent(name: "CONFERENCE_FAILED", data: data)
        print("conference Failed log is : \(data)")
    }
    func conferenceJoined(_ data: [AnyHashable: Any]) {
        onJitsiMeetViewDelegateEvent(name: "CONFERENCE_JOINED", data: data)
         print("conference Joined log is : \(data)")
    }
    func conferenceLeft(_ data: [AnyHashable: Any]) {
        onJitsiMeetViewDelegateEvent(name: "CONFERENCE_LEFT", data: data)
        print("conference Left log is : \(data)")
        print("call navigation")
        DispatchQueue.main.async {
        print("call navigation")
       // self.navigationController?.popViewController(animated: true)
//            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
//            for aViewController in viewControllers {
//                if aViewController is ChatViewController {
//                    self.navigationController!.popToViewController(aViewController, animated: true)
//                }
//            }
        print("navigation called")
        
        }
    }
    func conferenceWillJoin(_ data: [AnyHashable: Any]) {
        onJitsiMeetViewDelegateEvent(name: "CONFERENCE_WILL_JOIN", data: data)
        print("conference Join log is : \(data)")
    }
    func conferenceWillLeave(_ data: [AnyHashable: Any]) {
        onJitsiMeetViewDelegateEvent(name: "CONFERENCE_WILL_LEAVE", data: data)
         print("conference Leave log is : \(data)")
        print("call navigation")
        DispatchQueue.main.async {
            print("call navigation")
        //    self.navigationController?.popViewController(animated: true)
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is ChatViewController {
                    self.navigationController!.popToViewController(aViewController, animated: true)
                }
            }
            print("navigation called")
        }
    }
    func loadConfigError(_ data: [AnyHashable: Any]) {
        onJitsiMeetViewDelegateEvent(name: "LOAD_CONFIG_ERROR", data: data)
        print("conference Error log is : \(data)")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        // Show the navigation bar on other view controllers
        let defaults = UserDefaults.standard
        defaults.setValue("0", forKey: "isVideoGoing")
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    //MARK:
    @IBAction func action_back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
