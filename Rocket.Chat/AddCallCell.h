//
//  AddCallCell.h
//  Rocket.Chat
//
//  Created by Riyaz Lakhani on 14/02/18.
//  Copyright © 2018 Rocket.Chat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddCallCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *free;
@property (weak, nonatomic) IBOutlet UILabel *name;

@property (weak, nonatomic) IBOutlet UILabel *phoneNumber;

@end
