//
//  pjstring.m
//  Rocket.Chat
//
//  Created by Riyaz Lakhani on 21/03/18.
//  Copyright © 2018 Rocket.Chat. All rights reserved.
//

#import "pjstring.h"

@implementation pjstring

+ (NSString *)stringWithPJString:(pj_str_t)pjString {
    return [[NSString alloc] initWithBytes:pjString.ptr
                                     length:(NSUInteger)pjString.slen
                                   encoding:NSUTF8StringEncoding];
}

- (pj_str_t)pjString {
   // return pj_str((char *)[self cStringUsingEncoding:NSUTF8StringEncoding]);
    return pj_str((char *)[NSString defaultCStringEncoding]);
}



@end
