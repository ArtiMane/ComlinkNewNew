//
//  AddressBookCell.swift
//  Rocket.Chat
//
//  Created by Riyaz Lakhani on 08/02/18.
//  Copyright © 2018 Rocket.Chat. All rights reserved.
//

import UIKit

class AddressBookCell: UITableViewCell {

    @IBOutlet weak var call: UIButton!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var name: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
